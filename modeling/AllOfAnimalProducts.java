import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfAnimalProducts {

    public Product wool() {
        return new AnimalProduct(new HashMap<String, Double>() {{
        }}, 1, "Wool", new Probability() {{
        }},
                200, new ArrayList<Tool>() {{
        }}, new ArrayList<Task>() {{
        }}, false, true, new Animal("Sheep", new ArrayList<Feature>(), new ArrayList<Task>() {{
        }}, new ArrayList<Product>(), new ArrayList<Item>(), 1, 200), new Image("pics/products/wool.png"));
    }

    public Product sheepMeat() {
        return new AnimalProduct(new HashMap<String, Double>() {{
        }}, 1, "Sheep Meat", new Probability() {{
        }},
                150, new ArrayList<Tool>() {{
        }}, new ArrayList<Task>() {{
        }}, false, false, new Animal("Sheep", new ArrayList<Feature>(), new ArrayList<Task>() {{
        }}, new ArrayList<Product>(), new ArrayList<Item>(), 1, 200), new Image("pics/products/sheepMeat.png"));
    }

    public Product milk() {
        return new AnimalProduct(new HashMap<String, Double>() {{
            put("Health", 20.0);
            put("Energy", 20.0);
        }}, 1,
                "Milk", new Probability() {{
        }}, 60, new ArrayList<Tool>() {{
        }}, new ArrayList<Task>() {{
        }}, true, false, new Animal("Cow", new ArrayList<Feature>(), new ArrayList<Task>() {{
        }}, new ArrayList<Product>(), new ArrayList<Item>(), 1, 200), new Image("pics/products/milk.png"));
    }

    public Product cowMeat() {
        return new AnimalProduct(new HashMap<String, Double>() {{
        }}, 1, "Cow Meat", new Probability() {{
        }},
                100, new ArrayList<Tool>() {{
        }}, new ArrayList<Task>() {{
        }}, false, false, new Animal("Cow", new ArrayList<Feature>(), new ArrayList<Task>() {{
        }}, new ArrayList<Product>(), new ArrayList<Item>(), 1, 200), new Image("pics/products/cowMeat.png"));
    }

    public Product fishMeat() {
        return new AnimalProduct(new HashMap<String, Double>() {{
        }}, 1, "Fish Meat", new Probability() {{
        }},
                200, new ArrayList<Tool>() {{
        }}, new ArrayList<Task>() {{
        }}, false, false, new Animal("Fish", new ArrayList<Feature>(), new ArrayList<Task>() {{
        }}, new ArrayList<Product>(), new ArrayList<Item>(), 1, 200), new Image("pics/products/fishMeat.png"));
    }

    public Product chickenMeat() {
        return new AnimalProduct(new HashMap<String, Double>() {{
        }}, 1, "Chicken Meat", new Probability() {{
        }}
                , 70, new ArrayList<Tool>() {{
        }}, new ArrayList<Task>() {{
        }}, false, false, new Animal("Chicken", new ArrayList<Feature>(), new ArrayList<Task>() {{
        }}, new ArrayList<Product>(), new ArrayList<Item>(), 1, 200), new Image("pics/products/chickenMeat.png"));
    }

    public Product egg() {
        return new AnimalProduct(new HashMap<String, Double>() {{
        }}, 1, "Egg", new Probability() {{
        }}
                , 30, new ArrayList<Tool>() {{
        }}, new ArrayList<Task>() {{
        }}, false, false, new Animal("Chicken", new ArrayList<Feature>(), new ArrayList<Task>() {{
        }}, new ArrayList<Product>(), new ArrayList<Item>(), 1, 200), new Image("pics/products/egg.png"));
    }
}

// FIXME: 5/6/2017 - capacity ba tavajoh be poshte shavandegi
// FIXME: 5/6/2017  - Tool ha
// FIXME: 5/6/2017 - Task ha
