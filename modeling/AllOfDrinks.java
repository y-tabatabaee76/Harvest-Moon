import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfDrinks {

    private AllOfFruits allOfFruits = new AllOfFruits();


    public Drink water() {
        Drink water = new Drink(new HashMap<String, Double>(), 1, "Water", new Probability() {{
        }}, 0, new ArrayList<Task>() {{}}, new Dissassemblity(0, HashMultiset.<Item>create(), false),
                null, new Image("pics/drinks/water.png"));
        return water;
    }


    public Drink peachJuice() {
        Drink peachJuice = new Drink(new HashMap<String, Double>() {{
            put("Health", 18.75);
        }},
                1, "Peach Juice", new Probability() {{
        }}, 18, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.peach(), new Image("pics/drinks/peachJuice.png"));
        peachJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.peach());
        return peachJuice;
    }


    public Drink pearJuice() {
        Drink pearJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 25.0);
        }},
                1, "Pear Juice", new Probability() {{
        }}, 18, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.pear(), new Image("pics/drinks/pearJuice" +
                ".png"));
        pearJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.pear());
        return pearJuice;
    }


    public Drink garlicJuice() {
        Drink garlicJuice = new Drink(new HashMap<String, Double>() {{
            put("MaxHealth", 2.5);
        }},
                1, "Garlic Juice", new Probability() {{
        }}, 18, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.garlic(), new Image("pics/drinks/water.png"));
        garlicJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.garlic());
        return garlicJuice;
    }


    public Drink peaJuice() {
        Drink peaJuice = new Drink(new HashMap<String, Double>() {{
            put("MaxEnergy", 2.5);
        }},
                1, "Pea Juice", new Probability() {{
        }}, 12, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.pea(), new Image("pics/drinks/water.png"));
        peaJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.pea());
        return peaJuice;
    }


    public Drink lettuceJuice() {
        Drink lettuceJuice = new Drink(new HashMap<String, Double>() {{
            put("Health", 12.5);
        }},
                1, "Lettuce Juice", new Probability() {{
        }}, 12, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.lettuce(), new Image("pics/drinks/water.png"));
        lettuceJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.lettuce());
        return lettuceJuice;
    }


    public Drink eggplantJuice() {
        Drink eggplantJuice = new Drink(new HashMap<String, Double>() {{
            put("Health", 12.5);
            put("Energy", -6.25);
        }},
                1, "Eggplant Juice", new Probability() {{
        }}, 24, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.eggplant(), new Image("pics/drinks/water.png"));
        eggplantJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.eggplant());
        return eggplantJuice;
    }


    public Drink lemonJuice() {
        Drink lemonJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 25.0);
        }},
                1, "Lemon Juice", new Probability() {{
        }}, 18, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.lemon(), new Image("pics/drinks/water.png"));
        lemonJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.lemon());
        return lemonJuice;
    }


    public Drink pomegranateJuice() {
        Drink pomegranateJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 18.75);
            put("Health", 18.75);
            put("MaxEnergy", 6.25);
        }},
                1, "Pomegranate Juice", new Probability() {{
        }}, 30, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.pomegranate(), new Image("pics/drinks/water.png"));
        pomegranateJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.pomegranate());
        return pomegranateJuice;
    }


    public Drink cucumberJuice() {
        Drink cucumberJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 12.5);
        }},
                1, "Cucumber Juice", new Probability() {{
        }}, 12, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.cucumber(), new Image("pics/drinks/water.png"));
        cucumberJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.cucumber());
        return cucumberJuice;
    }


    public Drink watermelonJuice() {
        Drink watermelonJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 62.5);
            put("Health", 12.5);
            put("MaxEnergy", 12.5);
        }},
                1, "Watermelon Juice", new Probability() {{
        }}, 96, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.watermelon(), new Image("pics/drinks/watermelonJuice.png"));
        watermelonJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.watermelon());
        return watermelonJuice;
    }


    public Drink onionJuice() {
        Drink onionJuice = new Drink(new HashMap<String, Double>() {{
            put("MaxEnergy", 6.25);
        }},
                1, "Onion Juice", new Probability() {{
        }}, 18, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.onion(), new Image("pics/drinks/water.png"));
        onionJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.onion());
        return onionJuice;
    }


    public Drink turnipJuice() {
        Drink turnipJuice = new Drink(new HashMap<String, Double>() {{
            put("MaxEnergy", 3.75);
            put("Health", 3.75);
        }},
                1, "Turnip Juice", new Probability() {{
        }}, 18, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.turnip(), new Image("pics/drinks/water.png"));
        turnipJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.turnip());
        return turnipJuice;
    }


    public Drink appleJuice() {
        Drink appleJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 12.5);
            put("MaxHealth", 6.25);
        }},
                1, "Apple Juice", new Probability() {{
        }}, 24, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.apple(), new Image("pics/drinks/water.png"));
        appleJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.apple());
        return appleJuice;
    }


    public Drink orangeJuice() {
        Drink orangeJuice = new Drink(new HashMap<String, Double>() {{
            put("Health", 12.5);
            put("MaxEnergy", 6.25);
        }},
                1, "Orange Juice", new Probability() {{
        }}, 24, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.orange(), new Image("pics/drinks/water.png"));
        orangeJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.orange());
        return orangeJuice;
    }


    public Drink potatoJuice() {
        Drink potatoJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 12.5);
            put("Health", -6.25);
        }},
                1, "Orange Juice", new Probability() {{
        }}, 30, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.potato(), new Image("pics/drinks/water.png"));

        potatoJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.potato());
        return potatoJuice;
    }


    public Drink carrotJuice() {
        Drink carrotJuice = new Drink(new HashMap<String, Double>() {{
            put("MaxHealth", 12.5);
        }},
                1, "Carrot Juice", new Probability() {{
        }}, 30, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.carrot(), new Image("pics/drinks/carrotJuice.png"));
        carrotJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.carrot());
        return carrotJuice;
    }


    public Drink tomatoJuice() {
        Drink tomatoJuice = new Drink(new HashMap<String, Double>() {{
            put("Health", 6.25);
            put("Energy", 6.25);
        }},
                1, "Tomato Juice", new Probability() {{
        }}, 12, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.tomato(), new Image("pics/drinks/tomatoJuice.png"));
        tomatoJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.tomato());
        return tomatoJuice;
    }


    public Drink melonJuice() {
        Drink melonJuice = new Drink(new HashMap<String, Double>() {{
            put("Health", 12.5);
            put("Energy", 50.0);
            put("MaxEnergy", 6.25);
        }},
                1, "Melon Juice", new Probability() {{
        }}, 72, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.melon(), new Image("pics/drinks/melonJuice.png"));
        melonJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.melon());
        return melonJuice;
    }


    public Drink pineappleJuice() {
        Drink pineappleJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 18.75);
            put("Health", 18.75);
            put("MaxEnergy", 18.75);
        }},
                1, "Pineapple Juice", new Probability() {{
        }}, 180, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.pineapple(), new Image("pics/drinks/pineappleJuice.png"));
        pineappleJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.pineapple());
        return pineappleJuice;
    }


    public Drink strawberryJuice() {
        Drink strawberryJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 12.5);
            put("Health", 12.5);
            put("MaxEnergy", 6.25);
            put("MaxHealth", 6.25);
        }},
                1, "Strawberry Juice", new Probability() {{
        }}, 60, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.strawberry(), new Image("pics/drinks/strawberryJuice.png"));
        strawberryJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.strawberry());
        return strawberryJuice;
    }


    public Drink capsicumJuice() {
        Drink capsicumJuice = new Drink(new HashMap<String, Double>() {{
            put("Energy", 12.5);
            put("MaxHealth", 6.25);
        }},
                1, "Capsicum Juice", new Probability() {{
        }}, 30, new ArrayList<Task>() {{
        }},
                new Dissassemblity(10, HashMultiset.<Item>create(), true), allOfFruits.capsicum(), new Image("pics/drinks/water.png"));
        capsicumJuice.getDissassemblity().getInitialIngredients().add(allOfFruits.capsicum());
        return capsicumJuice;
    }


}
// TODO: 5/15/2017 mese baghie beshe
