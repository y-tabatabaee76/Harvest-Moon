import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfFruits {

    AllOFWeathers allOFWeathers = new AllOFWeathers();


    public Fruit junk() {
        Fruit junk = new Fruit(new HashMap<String, Double>() {{
        }}, 1, "Junk",
                new Probability() {{
                }}, 0, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false, null, new Image("pics/fruits/junk.png"));

        return junk;
    }

    public Fruit peach() {
        Fruit peach = new Fruit(new HashMap<String, Double>() {{
            put("Health", 15.0);
        }}, 1, "Peach",
                new Probability() {{
                }}, 15, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.spring(), new Image("pics/fruits/peach.png"));
        return peach;
    }


    public Fruit pear() {
        Fruit pear = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 20.0);
        }}, 1, "Pear",
                new Probability() {{
                }}, 15, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.spring(), new Image("pics/fruits/pear.png"));
        return pear;
    }


    public Fruit garlic() {
        Fruit garlic = new Fruit(new HashMap<String, Double>() {{
            put("MaxHealth", 2.0);
        }}, 1, "Garlic",
                new Probability() {{
                }}, 15, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.spring(), new Image("pics/fruits/garlic.png"));
        return garlic;
    }


    public Fruit pea() {
        Fruit pea = new Fruit(new HashMap<String, Double>() {{
            put("MaxEnergy", 2.0);
        }}, 1, "Pea",
                new Probability() {{
                }}, 10, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.spring(), new Image("pics/fruits/pea.png"));
        return pea;
    }


    public Fruit lettuce() {
        Fruit lettuce = new Fruit(new HashMap<String, Double>() {{
            put("Health", 10.0);
        }}, 1, "Lettuce",
                new Probability() {{
                }}, 10, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.spring(), new Image("pics/fruits/lettuce.png"));
        return lettuce;
    }


    public Fruit eggplant() {
        Fruit eggplant = new Fruit(new HashMap<String, Double>() {{
            put("Health", 10.0);
            put("Energy", -5.0);
        }}, 1, "Eggplant",
                new Probability() {{
                }}, 20, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.spring(), new Image("pics/fruits/eggplant.png"));
        return eggplant;
    }


    public Fruit lemon() {
        Fruit lemon = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 20.0);
        }}, 1, "Lemon",
                new Probability() {{
                }}, 15, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.summer(), new Image("pics/fruits/lemon.png"));
        return lemon;
    }


    public Fruit pomegranate() {
        Fruit pomegranate = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 15.0);
            put("Health", 15.0);
            put("MaxEnergy", 5.0);
        }}
                , 1, "Pomegranate", new Probability() {{
        }}, 25, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.summer(), new Image("pics/fruits/pomegranate.png"));
        return pomegranate;
    }


    public Fruit cucumber() {
        Fruit cucumber = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 10.0);
        }}, 1, "Cucumber",
                new Probability() {{
                }}, 10, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.summer(), new Image("pics/fruits/cucumber.png"));
        return cucumber;
    }


    public Fruit watermelon() {
        Fruit watermelon = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 50.0);
            put("Health", 10.0);
            put("MaxEnergy", 10.0);
        }}, 1, "Watermelon",
                new Probability() {{
                }}, 80, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.summer(), new Image("pics/fruits/watermelon.png"));
        return watermelon;
    }


    public Fruit onion() {
        Fruit onion = new Fruit(new HashMap<String, Double>() {{
            put("MaxEnergy", 5.0);
        }}, 1, "Onion",
                new Probability() {{
                }}, 15, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.summer(), new Image("pics/fruits/onion.png"));
        return onion;
    }


    public Fruit turnip() {
        Fruit turnip = new Fruit(new HashMap<String, Double>() {{
            put("MaxEnergy", 3.0);
            put("Health", 3.0);
        }}, 1, "Turnip",
                new Probability() {{
                }}, 15, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.summer(), new Image("pics/fruits/turnip.png"));
        return turnip;
    }


    public Fruit apple() {
        Fruit apple = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 10.0);
            put("MaxHealth", 5.0);
        }}, 1, "Apple",
                new Probability() {{
                }}, 20, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.fall(), new Image("pics/fruits/apple.png"));
        return apple;
    }


    public Fruit orange() {
        Fruit orange = new Fruit(new HashMap<String, Double>() {{
            put("Health", 10.0);
            put("MaxEnergy", 5.0);
        }}, 1, "Orange",
                new Probability() {{
                }}, 20, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.fall(), new Image("pics/fruits/orange.png"));
        return orange;
    }


    public Fruit potato() {
        Fruit potato = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 10.0);
            put("Health", -5.0);
        }}, 1, "Potato",
                new Probability() {{
                }}, 25, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.fall(), new Image("pics/fruits/potato.png"));
        return potato;
    }


    public Fruit carrot() {
        Fruit carrot = new Fruit(new HashMap<String, Double>() {{
            put("MaxHealth", 10.0);
        }}, 1, "Carrot",
                new Probability() {{
                }}, 25, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.fall(), new Image("pics/fruits/carrot.png"));
        return carrot;
    }


    public Fruit tomato() {
        Fruit tomato = new Fruit(new HashMap<String, Double>() {{
            put("Health", 5.0);
            put("Energy", 5.0);
        }}, 1, "Tomato",
                new Probability() {{
                }}, 10, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.fall(), new Image("pics/fruits/tomato.png"));
        return tomato;
    }


    public Fruit melon() {
        Fruit melon = new Fruit(new HashMap<String, Double>() {{
            put("Health", 10.0);
            put("Energy", 40.0);
            put("MaxEnergy", 5.0);
        }}
                , 1, "Melon", new Probability() {{
        }}, 60, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.fall(), new Image("pics/fruits/melon.png"));
        return melon;
    }


    public Fruit pineapple() {
        Fruit pineapple = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 15.0);
            put("Health", 15.0);
            put("MaxEnergy", 15.0);
        }}
                , 1, "Pineapple", new Probability() {{
        }}, 150, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.tropical(), new Image("pics/fruits/pineapple.png"));
        return pineapple;
    }


    public Fruit strawberry() {
        Fruit strawberry = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 10.0);
            put("Health", 10.0);
            put("MaxEnergy", 5.0);
            put("MaxHealth", 5.0);
        }}, 1, "Strawberry", new Probability() {{
        }}, 50, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.tropical(), new Image("pics/fruits/strawberry.png"));
        return strawberry;
    }


    public Fruit capsicum() {
        Fruit capsicum = new Fruit(new HashMap<String, Double>() {{
            put("Energy", 10.0);
            put("MaxHealth", 5.0);
        }}, 1, "Capsicum",
                new Probability() {{
                }}, 25, new ArrayList<Tool>(), new ArrayList<Task>() {{
        }}, false,
                allOFWeathers.tropical(), new Image("pics/fruits/capsicum.png"));
        return capsicum;
    }

}
// TODO: 5/15/2017 age halesh bod mese baghie she
