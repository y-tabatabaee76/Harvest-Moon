import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfMinerals {

    AllOfTrees allOfTrees = new AllOfTrees();

    public Mineral stone() {
        return new Mineral("Metal", new HashMap<String, Double>(), 1, "Stone",
                new Probability(){{setRate(0);setProbability(0);}}, 20, 1,
                "Hand", new ArrayList<Task>(){{}}, null, new Image("pics/minerals/stone.png"));
    }

    public Mineral ironOre() {
        return new Mineral("Metal", new HashMap<String, Double>(), 1, "IronOre",
                new Probability(){{setRate(0);setProbability(0);}}, 80, 2,
                "Stone Pickaxe", new ArrayList<Task>(){{}}, null, new Image("pics/minerals/ironOre.png"));
    }

    public Mineral silverOre() {
        return new Mineral("Metal", new HashMap<String, Double>(), 1, "SilverOre",
                new Probability(){{setRate(0);setProbability(0);}}, 250, 3,
                "Iron Pickaxe", new ArrayList<Task>(){{}}, null, new Image("pics/minerals/silverOre.png"));
    }

    public Mineral adamantiumOre() {
        return new Mineral("Metal", new HashMap<String, Double>(), 1,
                "AdamantiumOre", new Probability(){{setRate(0);setProbability(0);}}, 1000, 4,
                "Silver Pickaxe", new ArrayList<Task>(){{}}, null, new Image("pics/minerals/adamantiumOre.png"));
    }

    public Mineral branch() {
        return new Mineral("Wood", new HashMap<String, Double>(), 1, "Branch",
                new Probability(){{setRate(0);setProbability(0);}}, 20, 1,
                "Hand", new ArrayList<Task>(){{}}, null, new Image("pics/minerals/branch.png"));
    }

    public Mineral oldLumber() {
        return new Mineral("Wood", new HashMap<String, Double>(), 1, "OldLumber",
                new Probability(){{setRate(0);setProbability(0);}}, 80, 2,
                "Stone Axe", new ArrayList<Task>(){{}}, allOfTrees.oldTree(), new Image("pics/minerals/oldLumber.png"));
    }

    public Mineral pineLumber() {
        return new Mineral("Wood", new HashMap<String, Double>(), 1, "PineLumber",
                new Probability(){{setRate(0);setProbability(0);}}, 250, 3,
                "Iron Axe", new ArrayList<Task>(){{}}, allOfTrees.pine(), new Image("pics/minerals/pineLumber.png"));
    }

    public Mineral oakLumber() {
        return new Mineral("Wood", new HashMap<String, Double>(), 1, "OakLumber",
                new Probability(){{setRate(0);setProbability(0);}}, 1000, 4,
                "Silver Axe", new ArrayList<Task>(){{}}, allOfTrees.oak(), new Image("pics/minerals/oakLumber.png"));
    }


    // FIXME: 5/6/2017 - ArrayList Toolesh ro por konim(Stone va baghiye)
    // FIXME: 5/6/2017 - ArrayList Taskesh ro por konim(Stone va baghiye)
}
