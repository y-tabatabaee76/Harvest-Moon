import javafx.scene.image.Image;

public class AllOfPlayerImages {
    public LocatedImage up1() {
        return new LocatedImage("pics/player/up1.png");
    }
    public LocatedImage up2() {
        return new LocatedImage("pics/player/up2.png");
    }
    public LocatedImage up3() {
        return new LocatedImage("pics/player/up3.png");
    }
    public LocatedImage up4() {
        return new LocatedImage("pics/player/up4.png");
    }
    public LocatedImage up5() {
        return new LocatedImage("pics/player/up5.png");
    }
    public LocatedImage up6() {
        return new LocatedImage("pics/player/up6.png");
    }
    public LocatedImage up7() {
        return new LocatedImage("pics/player/up7.png");
    }
    public LocatedImage up8() {
        return new LocatedImage("pics/player/up8.png");
    }
    public LocatedImage up9() {
        return new LocatedImage("pics/player/up9.png");
    }
    public LocatedImage down1() {
        return new LocatedImage("pics/player/down1.png");
    }
    public LocatedImage down2() {
        return new LocatedImage("pics/player/down2.png");
    }
    public LocatedImage down3() {
        return new LocatedImage("pics/player/down3.png");
    }
    public LocatedImage down4() {
        return new LocatedImage("pics/player/down4.png");
    }
    public LocatedImage down5() {
        return new LocatedImage("pics/player/down5.png");
    }
    public LocatedImage down6() {
        return new LocatedImage("pics/player/down6.png");
    }
    public LocatedImage down7() {
        return new LocatedImage("pics/player/down7.png");
    }
    public LocatedImage down8() {
        return new LocatedImage("pics/player/down8.png");
    }
    public LocatedImage down9() {
        return new LocatedImage("pics/player/down9.png");
    }
    public LocatedImage right1() {
        return new LocatedImage("pics/player/right1.png");
    }
    public LocatedImage right2() {
        return new LocatedImage("pics/player/right2.png");
    }
    public LocatedImage right3() {
        return new LocatedImage("pics/player/right3.png");
    }
    public LocatedImage right4() {
        return new LocatedImage("pics/player/right4.png");
    }
    public LocatedImage right5() {
        return new LocatedImage("pics/player/right5.png");
    }
    public LocatedImage right6() {
        return new LocatedImage("pics/player/right6.png");
    }
    public LocatedImage right7() {
        return new LocatedImage("pics/player/right7.png");
    }
    public LocatedImage right8() {
        return new LocatedImage("pics/player/right8.png");
    }
    public LocatedImage right9() {
        return new LocatedImage("pics/player/right9.png");
    }
    public LocatedImage left1() {
        return new LocatedImage("pics/player/left1.png");
    }
    public LocatedImage left2() {
        return new LocatedImage("pics/player/left2.png");
    }
    public LocatedImage left3() {
        return new LocatedImage("pics/player/left3.png");
    }
    public LocatedImage left4() {
        return new LocatedImage("pics/player/left4.png");
    }
    public LocatedImage left5() {
        return new LocatedImage("pics/player/left5.png");
    }
    public LocatedImage left6() {
        return new LocatedImage("pics/player/left6.png");
    }
    public LocatedImage left7() {
        return new LocatedImage("pics/player/left7.png");
    }
    public LocatedImage left8() {
        return new LocatedImage("pics/player/left8.png");
    }
    public LocatedImage left9() {
        return new LocatedImage("pics/player/left9.png");
    }

}
