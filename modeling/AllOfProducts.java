import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfProducts {

    AllOfAnimalProducts allOfAnimalProducts = new AllOfAnimalProducts();

    public Product thread() {
        HashMultiset initialIngredients = HashMultiset.<Item>create();
        initialIngredients.add(allOfAnimalProducts.wool());
        return new Product("Product", new HashMap<String, Double>(), 1, "Thread",
                new Probability(){{}}, 300, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, false,
                true, new Dissassemblity(0, initialIngredients, true), null, new Image("pics/products/thread.png"));
    }

    public Product alfalfa() {
        return new Product("AnimalFood", new HashMap<String, Double>(), 1, "Alfalfa",
                new Probability(){{}}, 20, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, false,
                true, new Dissassemblity(0, HashMultiset.<Item>create(), false), null, new Image("pics/products/alfalfa.png"));
    }

    public Product seed() {
        return new Product("AnimalFood", new HashMap<String, Double>(), 1, "Seed",
                new Probability(){{}}, 10, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, false,
                true, new Dissassemblity(0, HashMultiset.<Item>create(), false), null, new Image("pics/products/seed.png"));
    }

    public Product oil() {
        return new Product("Product", new HashMap<String, Double>(), 1, "Oil",
                new Probability(){{}}, 30, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, false,
                false, new Dissassemblity(0, HashMultiset.<Item>create(), false), null, new Image("pics/products/oil.png"));
    }

    public Product flour() {
        return new Product("Product", new HashMap<String, Double>(), 1, "Flour",
                new Probability(){{}}, 20, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, false,
                true, new Dissassemblity(0, HashMultiset.<Item>create(), false), null, new Image("pics/products/flour.png"));
    }

    public Product sugar() {
        return new Product("Product", new HashMap<String, Double>(), 1, "Sugar",
                new Probability(){{}}, 10, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, false,
                true, new Dissassemblity(0, HashMultiset.<Item>create(), false), null, new Image("pics/products/sugar.png"));
    }

    public Product salt() {
        return new Product("Product", new HashMap<String, Double>(), 1, "Salt",
                new Probability(){{}}, 10, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, false,
                true, new Dissassemblity(0, HashMultiset.<Item>create(), false), null, new Image("pics/products/salt.png"));
    }

    public Product bread() {
        return new Product("Product", new HashMap<String, Double>(){{put("Energy", 10.0);put("Health", 5.0);}}, 1,
                "Bread", new Probability(){{}}, 20, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, true,
                false, new Dissassemblity(0, HashMultiset.<Item>create(), false), null, new Image("pics/products/bread.png"));
    }

    public Product cheese() {
        return new Product("Product", new HashMap<String, Double>(){{put("Energy", 30.0);put("Health", 30.0);}}, 1,
                "Cheese", new Probability(){{}}, 150, new ArrayList<Tool>(){{}}, new ArrayList<Task>(){{}}, true,
                false, new Dissassemblity(0, HashMultiset.<Item>create(), false), allOfAnimalProducts.milk(), new Image("pics/products/cheese.png"));
    }
}
