import javafx.scene.image.Image;

import java.util.ArrayList;

public class AllOfSeeds {

    AllOfCrops allOfCrops = new AllOfCrops();

    public Seed tomato(){
        return new Seed("Tomato Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.tomato(), new Image("pics/seed.png"));
    }

    public Seed garlic(){
        return new Seed("Garlic Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.garlic(), new Image("pics/seed.png"));
    }

    public Seed pea(){
        return new Seed("Pea Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.pea(), new Image("pics/seed.png"));
    }

    public Seed lettuce(){
        return new Seed("Lettuce Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.lettuce(), new Image("pics/seed.png"));
    }

    public Seed eggplant(){
        return new Seed("Eggplant Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.eggplant(), new Image("pics/seed.png"));
    }

    public Seed potato(){
        return new Seed("Potato Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.potato(), new Image("pics/seed.png"));
    }

    public Seed carrot(){
        return new Seed("Carrot Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.carrot(), new Image("pics/seed.png"));
    }

    public Seed melon(){
        return new Seed("Melon Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.melon(), new Image("pics/seed.png"));
    }

    public Seed cucumber(){
        return new Seed("Cucumber Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.cucumber(), new Image("pics/seed.png"));
    }

    public Seed watermelon(){
        return new Seed("Watermelon Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.watermelon(), new Image("pics/seed.png"));
    }

    public Seed onion(){
        return new Seed("Onion Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.onion(), new Image("pics/seed.png"));
    }

    public Seed turnip(){
        return new Seed("Turnip Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.turnip(), new Image("pics/seed.png"));
    }

    public Seed pineapple(){
        return new Seed("Pineapple Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.pineapple(), new Image("pics/seed.png"));
    }

    public Seed strawberry(){
        return new Seed("Strawberry Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.strawberry(), new Image("pics/seed.png"));
    }

    public Seed capsicum(){
        return new Seed("Capsicum Seed", 3, new ArrayList<Task>(){{}}, allOfCrops.capsicum(), new Image("pics/seed.png"));
    }

}
