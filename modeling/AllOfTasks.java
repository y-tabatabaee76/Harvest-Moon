import java.util.ArrayList;
import java.util.Map;

public class AllOfTasks {

    //Watering watering = new Watering();
    Take take = new Take();
    Drop drop = new Drop();
    //private Task drop;
    private Task stats;


    private Task buy;
    private Task sell;
    private Task fill;
    private Task sleep;
    private Task nextDay;
    private Task save;
    private Task cook;
    private Task replace;
    private Task update;
    private Task repair;
    private Task raining;
    private Task snowing;
    private Task thundering;
    private Task hailing;
    private Task light;
    private Task eat;
    private Task heal;
    private Task getSomething;
    private Task earn;
    private Task pay;
    private Task make;
    private Task disassemble;
    private Task missionGenerator;
    private Task training;
    private Task cut;
    private Task fishing;
    private Task breakdown;
    private Task faint;
    private Task finishedFeature;
    private Task produce;
    private Task decrease;
    private Task increase;
    private Task decreaseMax;
    private Task increaseMax;
}