
import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AllOfTools {

    AllOfMinerals allOfMinerals = new AllOfMinerals();
    AllOfProducts allOfProducts = new AllOfProducts();

    public Shovel stoneShovel() {

        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.branch(), 5);
        initialIngredients.add(allOfMinerals.stone(), 5);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.branch(), 2);
        repairItems.add(allOfMinerals.stone(), 2);
        return new Shovel("Metal", new HashMap<String, Double>() {{
            put("Energy", -150.0);
        }}, 1, "Stone Shovel",
                0, new ArrayList<Task>() {{
        }}, 1, new Dissassemblity(0, initialIngredients, true), 100, 10, repairItems,
                new Range(1, 1), new Image("pics/tools/stoneShovel.png"));
    }

    public Shovel ironShovel() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oldLumber(), 4);
        initialIngredients.add(allOfMinerals.ironOre(), 4);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oldLumber(), 2);
        repairItems.add(allOfMinerals.ironOre(), 2);
        return new Shovel("Metal", new HashMap<String, Double>() {{
            put("Energy", -80.0);
        }}, 1, "Iron Shovel",
                0, new ArrayList<Task>() {{
        }}, 2, new Dissassemblity(0, initialIngredients, true), 500, 50, repairItems,
                new Range(1, 2), new Image("pics/tools/ironShovel.png"));
    }

    public Shovel silverShovel() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.pineLumber(), 3);
        initialIngredients.add(allOfMinerals.silverOre(), 3);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.pineLumber(), 1);
        repairItems.add(allOfMinerals.silverOre(), 1);
        return new Shovel("Metal", new HashMap<String, Double>() {{
            put("Energy", -40.0);
        }}, 1, "Silver Shovel",
                0, new ArrayList<Task>() {{
        }}, 3, new Dissassemblity(0, initialIngredients, true), 1000, 100, repairItems,
                new Range(1, 3), new Image("pics/tools/silverShovel.png"));
    }

    public Shovel adamantiumShovel() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oakLumber(), 2);
        initialIngredients.add(allOfMinerals.adamantiumOre(), 2);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oakLumber(), 1);
        repairItems.add(allOfMinerals.adamantiumOre(), 1);
        return new Shovel("Metal", new HashMap<String, Double>() {{
            put("Energy", -20.0);
        }}, 1, "Adamantium Shovel",
                0, new ArrayList<Task>() {{
        }}, 4, new Dissassemblity(0, initialIngredients, true), 4000, 400, repairItems,
                new Range(3, 3), new Image("pics/tools/adamantiumShovel.png"));
    }

    public Pickaxe stonePickaxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.branch(), 5);
        initialIngredients.add(allOfMinerals.stone(), 10);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.branch(), 2);
        repairItems.add(allOfMinerals.stone(), 5);
        return new Pickaxe("Metal", new HashMap<String, Double>(), 1, "Stone Pickaxe", 0, new ArrayList<Task>() {{
        }},
                1, new Dissassemblity(0, initialIngredients, true), 200, 20, repairItems, new HashMap<String, Integer>() {{
            put("Metal", 2);
        }}, new Image("pics/tools/stonePickaxe.png"));
    }

    public Pickaxe ironPickaxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oldLumber(), 4);
        initialIngredients.add(allOfMinerals.ironOre(), 8);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oldLumber(), 2);
        repairItems.add(allOfMinerals.ironOre(), 4);
        return new Pickaxe("Metal", new HashMap<String, Double>(), 1, "Iron Pickaxe", 0, new ArrayList<Task>() {{
        }},
                2, new Dissassemblity(0, initialIngredients, true), 800, 80, repairItems, new HashMap<String, Integer>() {{
            put("Metal", 3);
        }}, new Image("pics/tools/ironPickaxe.png"));
    }

    public Pickaxe silverPickaxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.pineLumber(), 3);
        initialIngredients.add(allOfMinerals.silverOre(), 6);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.pineLumber(), 1);
        repairItems.add(allOfMinerals.silverOre(), 3);
        return new Pickaxe("Metal", new HashMap<String, Double>(), 1, "Silver Pickaxe", 0, new ArrayList<Task>() {{
        }},
                3, new Dissassemblity(0, initialIngredients, true), 2000, 200, repairItems, new HashMap<String, Integer>() {{
            put("Metal", 4);
        }}, new Image("pics/tools/silverPickaxe.png"));
    }

    public Pickaxe adamantiumPickaxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oakLumber(), 2);
        initialIngredients.add(allOfMinerals.adamantiumOre(), 4);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oakLumber(), 1);
        repairItems.add(allOfMinerals.adamantiumOre(), 2);
        return new Pickaxe("Metal", new HashMap<String, Double>(), 1, "Adamantium Pickaxe", 0, new ArrayList<Task>() {{
        }},
                4, new Dissassemblity(0, initialIngredients, true), 7000, 700, repairItems, new HashMap<String, Integer>() {{
            put("Metal", 4);
        }}, new Image("pics/tools/adamantiumPickaxe.png"));
    }

    public Axe stoneAxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.branch(), 10);
        initialIngredients.add(allOfMinerals.stone(), 5);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.branch(), 5);
        repairItems.add(allOfMinerals.stone(), 2);
        return new Axe("Metal", new HashMap<String, Double>(), 1, "Stone Axe", 0, new ArrayList<Task>() {{
        }},
                1, new Dissassemblity(0, initialIngredients, true), 200, 20, repairItems, new HashMap<String, Integer>() {{
            put("Wood", 2);
        }}, new Image("pics/tools/stoneAxe.png"));
    }

    public Axe ironAxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oldLumber(), 8);
        initialIngredients.add(allOfMinerals.ironOre(), 4);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oldLumber(), 4);
        repairItems.add(allOfMinerals.ironOre(), 2);
        return new Axe("Metal", new HashMap<String, Double>(), 1, "Iron Axe", 0, new ArrayList<Task>() {{
        }},
                2, new Dissassemblity(0, initialIngredients, true), 800, 80, repairItems, new HashMap<String, Integer>() {{
            put("Wood", 3);
        }}, new Image("pics/tools/ironAxe.png"));
    }

    public Axe silverAxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.pineLumber(), 6);
        initialIngredients.add(allOfMinerals.silverOre(), 3);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.pineLumber(), 3);
        repairItems.add(allOfMinerals.silverOre(), 1);
        return new Axe("Metal", new HashMap<String, Double>(), 1, "Silver Axe", 0, new ArrayList<Task>() {{
        }},
                3, new Dissassemblity(0, initialIngredients, true), 2000, 200, repairItems, new HashMap<String, Integer>() {{
            put("Wood", 4);
        }}, new Image("pics/tools/silverAxe.png"));
    }

    public Axe adamantiumAxe() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oakLumber(), 4);
        initialIngredients.add(allOfMinerals.adamantiumOre(), 2);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oakLumber(), 2);
        repairItems.add(allOfMinerals.adamantiumOre(), 1);
        return new Axe("Metal", new HashMap<String, Double>(), 1, "Adamantium Axe", 0, new ArrayList<Task>() {{
        }},
                4, new Dissassemblity(0, initialIngredients, true), 7000, 700, repairItems, new HashMap<String, Integer>() {{
            put("Wood", 4);
        }}, new Image("pics/tools/adamantiumAxe.png"));
    }

    public WateringCan smallWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.branch(), 5);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.branch(), 2);
        return new WateringCan("Wood", new HashMap<String, Double>() {{
            put("Energy", -40.0);
        }}, 1, "Small Watering Can",
                0, new ArrayList<Task>() {{
        }}, 1, new Dissassemblity(0, initialIngredients, true), 50, 5, repairItems,
                new Range(1, 1), 6, 1, new Image("pics/tools/wateringCan.png"));
    }

    public WateringCan oldWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oldLumber(), 4);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oakLumber(), 2);
        return new WateringCan("Wood", new HashMap<String, Double>() {{
            put("Energy", -30.0);
        }}, 1, "Old Watering Can",
                0, new ArrayList<Task>() {{
        }}, 2, new Dissassemblity(0, initialIngredients, true), 200, 20, repairItems,
                new Range(1, 2), 6, 2, new Image("pics/tools/wateringCan.png"));
    }

    public WateringCan pineWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.pineLumber(), 3);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.pineLumber(), 1);
        return new WateringCan("Wood", new HashMap<String, Double>() {{
            put("Energy", -20.0);
        }}, 1, "Pine Watering Can",
                0, new ArrayList<Task>() {{
        }}, 3, new Dissassemblity(0, initialIngredients, true), 500, 50, repairItems,
                new Range(1, 3), 6, 4, new Image("pics/tools/wateringCan.png"));
    }

    public WateringCan oakWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oakLumber(), 2);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oakLumber(), 1);
        return new WateringCan("Wood", new HashMap<String, Double>() {{
            put("Energy", -10.0);
        }}, 1, "Oak Watering Can",
                0, new ArrayList<Task>() {{
        }}, 4, new Dissassemblity(0, initialIngredients, true), 2000, 200, repairItems,
                new Range(3, 3), 6, 8, new Image("pics/tools/wateringCan.png"));
    }

    public WateringCan stoneWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.stone(), 5);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.stone(), 2);
        return new WateringCan("Metal", new HashMap<String, Double>() {{
            put("Energy", -80.0);
        }}, 1, "Stone Watering Can",
                0, new ArrayList<Task>() {{
        }}, 1, new Dissassemblity(0, initialIngredients, true), 50, 5, repairItems,
                new Range(1, 1), 6, 2, new Image("pics/tools/wateringCan.png"));
    }

    public WateringCan ironWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.ironOre(), 4);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.ironOre(), 2);
        return new WateringCan("Metal", new HashMap<String, Double>() {{
            put("Energy", -60.0);
        }}, 1, "Iron Watering Can",
                0, new ArrayList<Task>() {{
        }}, 2, new Dissassemblity(0, initialIngredients, true), 200, 20, repairItems,
                new Range(1, 2), 6, 4, new Image("pics/tools/wateringCan.png"));
    }

    public WateringCan silverWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.silverOre(), 3);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.silverOre(), 1);
        return new WateringCan("Metal", new HashMap<String, Double>() {{
            put("Energy", -40.0);
        }}, 1, "Silver Watering Can",
                0, new ArrayList<Task>() {{
        }}, 3, new Dissassemblity(0, initialIngredients, true), 500, 50, repairItems,
                new Range(1, 3), 6, 8, new Image("pics/tools/wateringCan.png"));
    }

    public WateringCan adamantiumWateringCan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.adamantiumOre(), 2);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.adamantiumOre(), 1);
        return new WateringCan("Metal", new HashMap<String, Double>() {{
            put("Energy", -20.0);
        }}, 1, "Adamantium Watering Can",
                0, new ArrayList<Task>() {{
        }}, 4, new Dissassemblity(0, initialIngredients, true), 2000, 200, repairItems,
                new Range(3, 3), 6, 16, new Image("pics/tools/wateringCan.png"));
    }

    public Tool smallFishingRod() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.branch(), 15);
        initialIngredients.add(allOfProducts.thread(), 2);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.branch(), 5);
        repairItems.add(allOfProducts.thread(), 1);
        return new FishingRod("Wood", new HashMap<String, Double>() {{
        }}, 1, "Small Fishing Rod", 0,
                new ArrayList<Task>() {{
                }}, 1, new Dissassemblity(0, initialIngredients, true), 100, 10, repairItems, new Probability() {{
            setProbability(0.4);
        }}, new Image("pics/tools/smallFishingRod.png"));
    }

    public FishingRod oldFishingRod() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oldLumber(), 10);
        initialIngredients.add(allOfProducts.thread(), 3);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oldLumber(), 4);
        repairItems.add(allOfProducts.thread(), 2);
        return new FishingRod("Wood", new HashMap<String, Double>() {{
        }}, 1, "Old Fishing Rod", 0,
                new ArrayList<Task>() {{
                }}, 2, new Dissassemblity(0, initialIngredients, true), 300, 30, repairItems, new Probability() {{
            setProbability(0.6);
        }}, new Image("pics/tools/oldFishingRod.png"));
    }

    public FishingRod pineFishingRod() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.pineLumber(), 6);
        initialIngredients.add(allOfProducts.thread(), 4);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.pineLumber(), 3);
        repairItems.add(allOfProducts.thread(), 1);
        return new FishingRod("Wood", new HashMap<String, Double>() {{
        }}, 1, "Pine Fishing Rod", 0,
                new ArrayList<Task>() {{
                }}, 3, new Dissassemblity(0, initialIngredients, true), 800, 80, repairItems, new Probability() {{
            setProbability(0.8);
        }}, new Image("pics/tools/pineFishingRod.png"));
    }

    public FishingRod oakFishingRod() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oakLumber(), 3);
        initialIngredients.add(allOfProducts.thread(), 5);
        HashMultiset<Item> repairItems = HashMultiset.create();
        repairItems.add(allOfMinerals.oakLumber(), 2);
        repairItems.add(allOfProducts.thread(), 1);
        return new FishingRod("Wood", new HashMap<String, Double>() {{put("Energy", 10d);
        }}, 1, "Oak Fishing Rod", 0,
                new ArrayList<Task>() {{
                }}, 4, new Dissassemblity(0, initialIngredients, true), 2000, 200, repairItems, new Probability() {{
            setProbability(1);
        }}, new Image("pics/tools/oakFishingRod.png"));
    }

    public CookingUtensil fryingPan() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.ironOre(), 7);
        return new CookingUtensil(new HashMap<String, Double>() {{
        }}, 1, "Frying Pan", new Probability() {{
            setProbability(0.1);
        }},
                0, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, initialIngredients, true), 0, 0, HashMultiset.<Item>create(), new Image("pics/tools/fryingPan.png"));
    }

    public CookingUtensil knife() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.ironOre(), 3);
        initialIngredients.add(allOfMinerals.oldLumber(), 2);
        return new CookingUtensil(new HashMap<String, Double>() {{
        }}, 1, "Knife", new Probability() {{
            setProbability(0.2);
        }},
                0, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, initialIngredients, true), 0, 0, HashMultiset.<Item>create(), new Image("pics/tools/knife.png"));
    }

    public CookingUtensil foodMixer() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.silverOre(), 5);
        initialIngredients.add(allOfMinerals.pineLumber(), 3);
        return new CookingUtensil(new HashMap<String, Double>() {{
        }}, 1, "Food Mixer", new Probability() {{
            setProbability(0.1);
        }},
                0, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, initialIngredients, true), 0, 0, HashMultiset.<Item>create(), new Image("pics/tools/foodMixer.png"));
    }

    public CookingUtensil pot() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.stone(), 10);
        return new CookingUtensil(new HashMap<String, Double>() {{
        }}, 1, "Pot", new Probability() {{
            setProbability(0.2);
        }},
                0, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, initialIngredients, true), 0, 0, HashMultiset.<Item>create(), new Image("pics/tools/pot.png"));
    }

    public CookingUtensil oven() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.adamantiumOre(), 3);
        initialIngredients.add(allOfMinerals.oakLumber(), 3);
        return new CookingUtensil(new HashMap<String, Double>() {{
        }}, 3, "Oven", new Probability() {{
            setProbability(0.05);
        }},
                0, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, initialIngredients, true), 0, 0, HashMultiset.<Item>create(), new Image("pics/tools/oven.png"));
    }

    public Machine juicer() {
        return new Machine(new HashMap<String, Double>() {{
        }}, 1, "Juicer", new Probability() {{
        }},
                1000, new ArrayList<Task>() {{
            add(new DrinkMaker());
        }}, 1, 1, new Dissassemblity(0, HashMultiset.<Item>create(), false), 100, 0, HashMultiset.<Item>create(), new Image("pics/tools/juicer.png"));
    }

    public Machine spinningWheel() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.adamantiumOre(), 3);
        initialIngredients.add(allOfMinerals.branch(), 3);
        return new Machine(new HashMap<String, Double>() {{
        }}, 1, "Spinning Wheel", new Probability() {{
        }},
                2000, new ArrayList<Task>() {{
            ;
        }}, 1, 1, new Dissassemblity(0, initialIngredients, true), 100, 0, HashMultiset.<Item>create(), new Image("pics/tools/spinningWheel.png"));
    }

    public Machine cheeseMaker() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.oldLumber(), 2);
        initialIngredients.add(allOfMinerals.stone(), 3);
        return new Machine(new HashMap<String, Double>() {{
        }}, 1, "Cheese Maker", new Probability() {{
        }},
                3000, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, initialIngredients, true), 100, 0, HashMultiset.<Item>create(), new Image("pics/tools/cheeseMaker.png"));
    }

    public Machine tomatoPaste() {
        HashMultiset<Item> initialIngredients = HashMultiset.create();
        initialIngredients.add(allOfMinerals.ironOre(), 4);
        initialIngredients.add(allOfMinerals.stone(), 2);
        return new Machine(new HashMap<String, Double>() {{
        }}, 1, "Tomato Paste", new Probability() {{
        }},
                1000, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, initialIngredients, false), 100, 0, HashMultiset.<Item>create(), new Image("pics/tools/tomatoPaste.png"));
    }

    public Machine milker() {
        return new Machine(new HashMap<String, Double>() {{
        }}, 1, "Milker", new Probability() {{
        }},
                3000, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, HashMultiset.<Item>create(), false), 100, 0, HashMultiset.<Item>create(), new Image("pics/tools/milker.png"));
    }

    public Machine scissors() {
        return new Machine(new HashMap<String, Double>() {{
        }}, 1, "Scissors", new Probability() {{
        }},
                3000, new ArrayList<Task>() {{
        }}, 1, 1, new Dissassemblity(0, HashMultiset.<Item>create(), false), 100, 0, HashMultiset.<Item>create(), new Image("pics/tools/scissors.png"));
    }

    public Tool hand() {
        return new Tool("Default", new HashMap<String, Double>() {{
        }}, 1, "Hand", new Probability() {{
        }},
                0, new ArrayList<Task>() {{
        }}, 0, 1, new Dissassemblity(0, HashMultiset.<Item>create(), false), 0, 0, HashMultiset.<Item>create(), new Image("pics/tools/silverShovel.png"));
    }
}