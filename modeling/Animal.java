import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;

public class Animal {

    private boolean feed;
    private String type;
    private int age;
    private ArrayList<Feature> feature;
    private ArrayList<Task> task;
    private ArrayList<Product> product;
    private String name;
    private ArrayList<Item> foods;
    private int produceCycle;
    private int produceProcess;
    private double price;
    private HashMultiset<Product> availabeProducts;
    private Image picture;

    public boolean isFeed() {
        return feed;
    }

    public void setFeed(boolean feed) {
        this.feed = feed;
    }

    public Image getPicture() {
        return picture;
    }

    public void setPicture(Image picture) {
        this.picture = picture;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ArrayList<Feature> getFeature() {
        return feature;
    }

    public void setFeature(ArrayList<Feature> feature) {
        this.feature = feature;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public ArrayList<Product> getProduct() {
        return product;
    }

    public void setProduct(ArrayList<Product> product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Item> getFoods() {
        return foods;
    }

    public void setFoods(ArrayList<Item> foods) {
        this.foods = foods;
    }

    public int getProduceCycle() {
        return produceCycle;
    }

    public void setProduceCycle(int produceCycle) {
        this.produceCycle = produceCycle;
    }

    public int getProduceProcess() {
        return produceProcess;
    }

    public void setProduceProcess(int produceProcess) {
        this.produceProcess = produceProcess;
    }

    public HashMultiset<Product> getAvailabeProducts() {
        return availabeProducts;
    }

    public void setAvailabeProducts(HashMultiset<Product> availabeProducts) {
        this.availabeProducts = availabeProducts;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Animal(String type, ArrayList<Feature> feature, ArrayList<Task> task, ArrayList<Product> product,
                  ArrayList<Item> foods, int produceCycle, double price) {
        this.type = type;
        this.feature = feature;
        this.task = task;
        this.product = product;
        this.foods = foods;
        this.produceCycle = produceCycle;
        this.price = price;
        availabeProducts = HashMultiset.create();
        picture = new Image("pics/animals/" + type.toLowerCase() + ".png");
    }

    public Animal clone(){
        return new Animal(type, feature, task, product, foods, produceCycle, price);
    }
}