import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AnimalMedicine extends Medicine {
    public AnimalMedicine(HashMap<String, Double> featureChangeRate, double capacity, String name,
                          Probability invalid, double price, ArrayList<Task> task, int maxLevel, int level, Image picture) {
        super("Animal Medicine", featureChangeRate, capacity, name, invalid, price, task, maxLevel, level, false, picture);
    }
}
