import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class AnimalProduct extends Product {


    public AnimalProduct(HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
                         double price, ArrayList<Tool> tools, ArrayList<Task> task, boolean eatable, boolean accumulation,
                         Object source, Image picture) {
        super("Animal Product", featureChangeRate, capacity, name, invalid, price, tools, task, eatable, accumulation,
                new Dissassemblity(0, HashMultiset.<Item>create(), false), source, picture);
    }

}