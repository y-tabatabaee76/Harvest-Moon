
import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Backpack {

	private HashMultiset<Item> item;
	private double capacity;
	private double currentFullness;
	private ArrayList<Task> task;
	private int level;
	private ArrayList<Animal> animals;

	public HashMultiset<Item> getItem() {
		return item;
	}

	public void setItem(HashMultiset<Item> item) {
		this.item = item;
	}

	public double getCapacity() {
		return capacity;
	}

	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}

	public double getCurrentFullness() {
		return currentFullness;
	}

	public void setCurrentFullness(double currentFullness) {
		this.currentFullness = currentFullness;
	}

	public ArrayList<Task> getTask() {
		return task;
	}

	public void setTask(ArrayList<Task> task) {
		this.task = task;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public ArrayList<Animal> getAnimals() {
		return animals;
	}

	public void setAnimals(ArrayList<Animal> animals) {
		this.animals = animals;
	}

	public Backpack(double capacity, int level) {
		this.item = HashMultiset.<Item>create();
		this.capacity = capacity;
		this.currentFullness = 0;
		this.level = level;
		animals = new ArrayList<>();
	}
}