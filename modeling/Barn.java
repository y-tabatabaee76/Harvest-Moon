import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Barn extends Building {

    private ArrayList<BarnPart> barnPart;
    private ArrayList<Machine> machines;
    private int maxBarnPartNumber;

    public Barn(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, int maxBarnPartNumber) {
        super(isRuin, ruin, repairObjects, repairPrice);
        this.maxBarnPartNumber = maxBarnPartNumber;
        machines = new ArrayList<>();

        AllOfAnimals allOfAnimals = new AllOfAnimals();
        BarnPart cows = new BarnPart("Cow", 6, this,
                new ArrayList<Animal>(){{for(int i = 0; i < (int)(Math.random() * 6); i++) add(allOfAnimals.cow());}});
        BarnPart chickens = new BarnPart("Chicken", 8, this,
                new ArrayList<Animal>(){{for(int i = 0; i < (int)(Math.random() * 8); i++) add(allOfAnimals.chicken());}});
        BarnPart sheep = new BarnPart("Sheep", 6, this,
                new ArrayList<Animal>(){{for(int i = 0; i < (int)(Math.random() * 6); i++) add(allOfAnimals.sheep());}});
        barnPart = new ArrayList<BarnPart>(){{add(cows);add(sheep);add(chickens);}};
    }

    public Barn(int maxBarnPartNumber) {
        super();
        this.maxBarnPartNumber = maxBarnPartNumber;
        machines = new ArrayList<>();

        AllOfAnimals allOfAnimals = new AllOfAnimals();
        BarnPart cows = new BarnPart("Cow", 6, this,
                new ArrayList<Animal>(){{for(int i = 0; i < (int)(Math.random() * 6); i++) add(allOfAnimals.cow());}});
        BarnPart chickens = new BarnPart("Chicken", 8, this,
                new ArrayList<Animal>(){{for(int i = 0; i < (int)(Math.random() * 8); i++) add(allOfAnimals.chicken());}});
        BarnPart sheep = new BarnPart("Sheep", 6, this,
                new ArrayList<Animal>(){{for(int i = 0; i < (int)(Math.random() * 6); i++) add(allOfAnimals.sheep());}});
        barnPart = new ArrayList<BarnPart>(){{add(cows);add(sheep);add(chickens);}};
    }

    public ArrayList<BarnPart> getBarnPart() {
        return barnPart;
    }

    public void setBarnPart(ArrayList<BarnPart> barnPart) {
        this.barnPart = barnPart;
    }

    public int getMaxBarnPartNumber() {
        return maxBarnPartNumber;
    }

    public void setMaxBarnPartNumber(int maxBarnPartNumber) {
        this.maxBarnPartNumber = maxBarnPartNumber;
    }

    public ArrayList<Machine> getMachines() {
        return machines;
    }

    public void setMachines(ArrayList<Machine> machines) {
        this.machines = machines;
    }
}