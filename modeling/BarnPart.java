import java.util.ArrayList;

public class BarnPart {
    private ArrayList<Animal> animal;
    private String animalType;
    private int maxCapacity;
    private Barn barn;

    public BarnPart(String animalType, int maxCapacity, Barn barn){
        this.barn = barn;
        this.animalType = animalType;
        this.maxCapacity = maxCapacity;
        animal = new ArrayList<>();

    }

    public BarnPart(String animalType, int maxCapacity, Barn barn, ArrayList<Animal> animals){
        this.barn = barn;
        this.animalType = animalType;
        this.maxCapacity = maxCapacity;
        this.animal = animals;
    }

    public ArrayList<Animal> getAnimal() {
        return animal;
    }

    public void setAnimal(ArrayList<Animal> animal) {
        this.animal = animal;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public Barn getBarn() {
        return barn;
    }

    public void setBarn(Barn barn) {
        this.barn = barn;
    }
}
