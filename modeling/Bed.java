import java.util.ArrayList;

public class Bed {

    private ArrayList<Save> save;
    private ArrayList<Task> task;

    public Bed(ArrayList<Save> save, ArrayList<Task> task) {
        this.save = save;
        this.task = task;
    }

    public Bed(ArrayList<Task> task) {
        this.save = new ArrayList<>();
        this.task = task;
    }

    public Bed() {
        this.save = new ArrayList<>();
        this.task = new ArrayList<>();
    }

    public ArrayList<Save> getSave() {
        return save;
    }

    public void setSave(ArrayList<Save> save) {
        this.save = save;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

}