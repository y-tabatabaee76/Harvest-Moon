import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Cafe extends Shop {

    private Board board;
    private ArrayList<DiningTable> diningTable;

    public Cafe(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice,
                HashMultiset goods, Board board, ArrayList<DiningTable> diningTable) {
        super(isRuin, ruin, repairObjects, repairPrice, goods, "Cafe");
        this.board = board;
        this.diningTable = diningTable;
    }

    public Cafe(HashMultiset goods, Board board) {
        super(goods, "Cafe");
        this.board = board;
        diningTable = new ArrayList<DiningTable>();
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public ArrayList<DiningTable> getDiningTable() {
        return diningTable;
    }

    public void setDiningTable(ArrayList<DiningTable> diningTable) {
        this.diningTable = diningTable;
    }

}