import java.util.ArrayList;
import java.util.HashMap;

public class Cave implements Place {

	private Mineral[][] content;
	private HashMap<String, Double> probability;
	private int cycle;

	public Cave() {
		probability = new HashMap<>();
		cycle = 3;
		ArrayList<Mineral> minerals = new ArrayList<>();
		AllOfMinerals allOfMinerals = new AllOfMinerals();
		minerals.add(allOfMinerals.adamantiumOre());
		minerals.add(allOfMinerals.ironOre());
		minerals.add(allOfMinerals.silverOre());
		minerals.add(allOfMinerals.stone());
		content = new Mineral[7][3];
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 7; i++) {
				int num = (int) (Math.random() * 4);
				content[i][j] = minerals.get(num);
			}
		}
	}

	public Mineral[][] getContent() {
		return content;
	}

	public void setContent(Mineral[][] content) {
		this.content = content;
	}

	public HashMap<String, Double> getProbability() {
		return probability;
	}

	public void setProbability(HashMap<String, Double> probability) {
		this.probability = probability;
	}

	public int getCycle() {
		return cycle;
	}

	public void setCycle(int cycle) {
		this.cycle = cycle;
	}
}