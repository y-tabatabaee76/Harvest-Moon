import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class CaveUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private Cave cave;
    private Rectangle[][] cellG;

    public CaveUI(Group root, Game game) {
        this.root = root;
        this.cave = game.getCave();
    }

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }


    public Scene sceneBuilder(Stage primaryStage) {
        primaryStage.setTitle("Cave");
        Scene caveScene = new Scene(root, 1000, 700, Color.BLACK);
        Image cavePic = new Image("pics/maps/caveMap.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(cavePic));
        root.getChildren().add(rectangle);

        cellG = new Rectangle[7][3];
        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 7; i++) {
                cellG[i][j] = new Rectangle();
                if (cave.getContent()[i][j].getName().equals("AdamantiumOre") || cave.getContent()[i][j].getName().equals("SilverOre"))
                    cellG[i][j] = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(cave.getContent()[i][j].getPicture()), tileSize * (2 * i + 5), tileSize * (3 * j + 5));
                else if (cave.getContent()[i][j].getName().equals("IronOre") || cave.getContent()[i][j].getName().equals("Stone"))
                    cellG[i][j] = rectangleBuilder(tileSize * 2, tileSize * 1, new ImagePattern(cave.getContent()[i][j].getPicture()), tileSize * (2 * i + 5), tileSize * (3 * j + 6));
                root.getChildren().addAll(cellG[i][j]);
            }
        }

        return caveScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.8) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 19.6) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= tileSize * 2.8) {
            return true;
        }
        return false;
    }

    private int ai;
    private int jey;

    public int getAi() {
        return ai;
    }

    public int getJey() {
        return jey;
    }


    public Mineral objectToCollect(Rectangle player) {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 3; j++) {
                if (cellG[i][j] != null) {
                    if (player.getX() + player.getWidth() / 2 <= cellG[i][j].getX() + cellG[i][j].getWidth() &&
                            player.getX() + player.getWidth() / 2 >= cellG[i][j].getX() &&
                            player.getY() + player.getHeight() <= cellG[i][j].getY() + cellG[i][j].getHeight() &&
                            player.getY() + player.getHeight() >= cellG[i][j].getY()) {
                        ai = i;
                        jey = j;
                        return cave.getContent()[i][j];
                    }
                }
            }
        }
        return null;
    }

    public void collect(Player player, Mineral mineral, Place place, int i, int j, boolean[] flags) {
        Collect collect = new Collect(new AllOfTools().hand());
        UI.showPopup(collect.run(player, mineral, place, i, j, cellG, root), root);
    }

    public void collect(Player player, Tool tool, Mineral mineral, Place place, int i, int j, boolean[] flags) {
        Collect collect = new Collect(tool);
        UI.showPopup(collect.run(player, tool, mineral, place, i, j, cellG, root), root);
        flags[2] = true;
    }

    public Rectangle[][] getCellG() {
        return cellG;
    }

    public void setCellG(Rectangle[][] cellG) {
        this.cellG = cellG;
    }
}
