import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class CheatCode extends Task {
    public CheatCode() {
        super.name = "CheatCode";
        super.featureChangeRate = new HashMap<>();
        super.executionTime = new Date();
    }

    public void randomGenerator(Backpack backpack) throws IllegalAccessException, InstantiationException {
        Option option = new Option();
        ArrayList<Item> items = new ArrayList<>();
        Class productsClass = AllOfProducts.class;
        Class drinkClass = AllOfDrinks.class;
        Class mineralClass = AllOfMinerals.class;
        Class fruitClass = AllOfFruits.class;
        Object obj1 = productsClass.newInstance();
        Object obj2 = drinkClass.newInstance();
        Object obj3 = mineralClass.newInstance();
        Object obj4 = fruitClass.newInstance();
        Method[] products = productsClass.getDeclaredMethods();
        Method[] drinks = drinkClass.getDeclaredMethods();
        Method[] minerals = mineralClass.getDeclaredMethods();
        Method[] fruits = fruitClass.getDeclaredMethods();

        for (Method method : products) {
            try {
                Product product = (Product) method.invoke(obj1);
                for (int i = 0; i < (int) (Math.random() * 5) + 1; i++) {
                    items.add(product);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        for (Method method : drinks) {
            try {
                Drink drink = (Drink) method.invoke(obj2);
                for (int i = 0; i < (int) (Math.random() * 5) + 1; i++) {
                    items.add(drink);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        for (Method method : minerals) {
            try {
                Mineral mineral = (Mineral) method.invoke(obj3);
                for (int i = 0; i < (int) (Math.random() * 5) + 1; i++) {
                    items.add(mineral);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        for (Method method : fruits) {
            try {
                Fruit fruit = (Fruit) method.invoke(obj4);
                for (int i = 0; i < (int) (Math.random() * 5) + 1; i++) {
                    items.add(fruit);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("Before this code:");
        option.inspectBackpack(backpack);
        for (int i = 0; i < (int) (items.size() / 30); i++) {
            backpack.getItem().add(items.get((int) (Math.random() * items.size() + 1)));
        }
        System.out.println("After this code:");
        option.inspectBackpack(backpack);
    }

    public void earnMoney(Player player) {
        System.out.println("Before this code:");
        System.out.println(player.getMoney());
        player.setMoney(player.getMoney() + (int) (Math.random() * 2000 + 1));
        System.out.println("After this code:");
        System.out.println(player.getMoney());
    }

    public void hero(Player player) {
        System.out.println("Before this code:");
        StringBuilder data1 = new StringBuilder();
        for (int i = 0; i < player.getFeature().size(); i++) {
            data1.append(player.getFeature().get(i).getName());
            data1.append(":\nMaxMax: ");
            data1.append(player.getFeature().get(i).getMaxMax());
            data1.append("\nMaxCurrent: ");
            data1.append(player.getFeature().get(i).getMaxCurrent());
            data1.append("\nCurrent: ");
            data1.append(player.getFeature().get(i).getCurrent());
            data1.append("\n");
        }
        System.out.println(data1);
        for (int i = 0; i < player.getFeature().size(); i++) {
            player.getFeature().get(i).setMaxMax(player.getFeature().get(i).getMaxMax() + 1000000);
            player.getFeature().get(i).setMaxCurrent(player.getFeature().get(i).getMaxCurrent() + 1000000);
            player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent() + 1000000);
        }
        StringBuilder data2 = new StringBuilder();
        for (int i = 0; i < player.getFeature().size(); i++) {
            data2.append(player.getFeature().get(i).getName());
            data2.append(":\nMaxMax: ");
            data2.append(player.getFeature().get(i).getMaxMax());
            data2.append("\nMaxCurrent: ");
            data2.append(player.getFeature().get(i).getMaxCurrent());
            data2.append("\nCurrent: ");
            data2.append(player.getFeature().get(i).getCurrent());
            data2.append("\n");
        }
        System.out.println(data2);
    }
}
