
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.io.*;
import java.net.Socket;


public class Client implements Runnable {
    private int port;
    private Socket client;
    private String name;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private OutputStream outputStream;
    private InputStream inputStream;
    private ObservableList clientLog;
    private Thread clientListenerThread;
    private Group root;
    private Group villageRoot;
    private Rectangle player;
    private Rectangle[] players;
    private String[] names;
    private boolean[] flag;
    private int indexOf = -1;
    private Text[] texts;
    private int chat = -1;
    private int chatReqFrom = -1;
    private boolean acceptChatReq = false;
    private Game game;
    private boolean receivedStatusReq = false;
    private PlayerStatus requestedPlayerStatus;
    private boolean[] friendReq;
    private boolean[] friend;
    private boolean[] lockInTo;
    private boolean[] lockInFrom;
    private int trade = -1;
    private int tradeReqFrom = -1;
    private boolean lockIn = false;
    private boolean acceptTradeReq = false;
    private ItemPack itemPack;


    public boolean isLockIn() {
        return lockIn;
    }

    public void setLockIn(boolean lockIn) {
        this.lockIn = lockIn;
    }

    public boolean[] getLockInTo() {
        return lockInTo;
    }

    public void setLockInTo(boolean[] lockInTo) {
        this.lockInTo = lockInTo;
    }

    public boolean[] getLockInFrom() {
        return lockInFrom;
    }

    public void setLockInFrom(boolean[] lockInFrom) {
        this.lockInFrom = lockInFrom;
    }

    public int getTrade() {
        return trade;
    }

    public int getTradeReqFrom() {
        return tradeReqFrom;
    }

    public boolean isAcceptTradeReq() {
        return acceptTradeReq;
    }

    public void setTrade(int trade) {
        this.trade = trade;
    }

    public void setAcceptTradeReq(boolean acceptTradeReq) {
        this.acceptTradeReq = acceptTradeReq;
    }


    public boolean[] getFriendReq() {
        return friendReq;
    }

    public void setFriendReq(boolean[] friendReq) {
        this.friendReq = friendReq;
    }

    public boolean[] getFriend() {
        return friend;
    }

    public void setFriend(boolean[] friend) {
        this.friend = friend;
    }

    public boolean isReceivedStatusReq() {
        return receivedStatusReq;
    }

    public PlayerStatus getRequestedPlayerStatus() {
        return requestedPlayerStatus;
    }

    public void setReceivedStatusReq(boolean receivedStatusReq) {
        this.receivedStatusReq = receivedStatusReq;
    }

    public int getChat() {
        return chat;
    }


    public int getChatReqFrom() {
        return chatReqFrom;
    }

    public String[] getNames() {
        return names;
    }

    public int getIndexOf() {
        return indexOf;
    }

    public boolean isAcceptChatReq() {
        return acceptChatReq;
    }

    public void setAcceptChatReq(boolean acceptChatReq) {
        this.acceptChatReq = acceptChatReq;
    }

    public void setChat(int chat) {
        this.chat = chat;
    }


    public Client(int port, String name, Group root, boolean[] flag, Group villageRoot, Rectangle player, Game game) {
        this.port = port;
        this.name = name;
        this.root = root;
        this.flag = flag;
        this.villageRoot = villageRoot;
        game.getPlayer().setName(name);
        this.player = player;//new Rectangle(player.getX(), player.getY(), player.getWidth(), player.getHeight());
        clientLog = FXCollections.observableArrayList();
        this.game = game;
    }

    public ObservableList getClientLog() {
        return clientLog;
    }

    public String getName() {
        return name;
    }


    @Override
    public void run() {
        connect2Server();
        getIOStreams();
        process();
        try {
            clientListenerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        closeConnection();
    }

    private void connect2Server() {
        try {
            client = new Socket("localhost", port);
            Text connection = UI.buttonDesigner("Connection established with the server", 160, 200, 20, Color.SADDLEBROWN);
            Platform.runLater(() -> root.getChildren().add(connection));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean ios = false;

    private void getIOStreams() {
        try {
            objectOutputStream = new ObjectOutputStream(client.getOutputStream());
            objectInputStream = new ObjectInputStream(client.getInputStream());
            outputStream = client.getOutputStream();
            inputStream = client.getInputStream();
            Platform.runLater(() -> clientLog.add("Client established I/O streams"));
            ios = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private boolean firstDataComes = false;
    private boolean[] setNames;

    private void process() {
        flag[0] = true;
        //this.villageRoot.getChildren().add(this.player);
        //Platform.runLater(() -> clientLog.add("Start processing"));
        clientListenerThread = new Thread(() -> {
            do {
                if (!ios) {
                    getIOStreams();
                }
                //System.out.println(firstDataComes);
                DataPack dataPack;
                FirstData firstData;
                NamePack namePack;
                Request request;
                Message message;
                PlayerStatus playerStatus;
                ItemPack itemPack;
                Item item = null;
                int size = 0;
                String fileName = "";
                String sender = "";
                Object object;
                try {
                    object = objectInputStream.readObject();
                    //System.out.println(object.getClass());
                    if (object instanceof DataPack && firstDataComes) {
                        dataPack = (DataPack) object;
                        players[dataPack.getIndex()].setX(dataPack.getPlayerX());
                        players[dataPack.getIndex()].setY(dataPack.getPlayerY());
                        players[dataPack.getIndex()].setWidth(dataPack.getPlayerWidth());
                        players[dataPack.getIndex()].setHeight(dataPack.getPlayerHeight());
                        players[dataPack.getIndex()].setFill(new ImagePattern(new Image(dataPack.getPlayerImageURL())));
                        if (texts != null && texts[dataPack.getIndex()] != null) {
                            texts[dataPack.getIndex()].setX(dataPack.getPlayerX());
                            texts[dataPack.getIndex()].setY(dataPack.getPlayerY() - 10);
                        }
                        sendName();
                        //System.out.println(dataPack.getDontAdd());
                        //if (dataPack.getDontAdd() != -1)
                        //Platform.runLater(() -> villageRoot.getChildren().remove(players[dataPack.getDontAdd()]));
                        /*Platform.runLater(() -> villageRoot.getChildren().remove(player));
                        player = new Rectangle(dataPack.getPlayerX(), dataPack.getPlayerY(), dataPack.getPlayerWidth(), dataPack.getPlayerHeight());
                        Platform.runLater(() -> villageRoot.getChildren().add(player));*/
                    } else if (object instanceof FirstData && !firstDataComes) {
                        firstDataComes = true;
                        firstData = (FirstData) object;
                        int num = firstData.getNumOfPlayers();
                        indexOf = firstData.getClientIndex();
                        setNames = new boolean[num];
                        names = new String[num];
                        texts = new Text[num];
                        //System.out.println(indexOf);
                        //System.out.println(num);
                        players = new Rectangle[num];
                        friend = new boolean[num];
                        friendReq = new boolean[num];
                        lockInTo = new boolean[num];
                        lockInFrom = new boolean[num];
                        //System.out.println("Length : " + players.length);
                        for (int i = 0; i < num; i++) {
                            //System.out.println(1230);
                            players[i] = new Rectangle(0.01, 0.01, 0.01, 0.01);
                            if (indexOf != i) {
                                int finalI = i;
                                Platform.runLater(() -> villageRoot.getChildren().add(players[finalI]));
                            }
                            //if (finalI != firstData.getDontAdd())
                            //System.out.println("Added : " + i);
                        }
                        sendName();
                    } else if (object instanceof NamePack && firstDataComes) {
                        namePack = (NamePack) object;
                        int namePackIndex = namePack.getIndexOf();
                        String namePackName = namePack.getPlayerName();
                        if (!setNames[namePackIndex]) {
                            setNames[namePackIndex] = true;
                            names[namePackIndex] = namePack.getPlayerName();
                            Text text;
                            text = UI.buttonDesigner(namePackName, players[namePackIndex].getX(),
                                    players[namePackIndex].getY() - 10, 20, Color.SADDLEBROWN);
                            texts[namePackIndex] = text;
                            Platform.runLater(() -> villageRoot.getChildren().add(texts[namePackIndex]));
                        }
                    } else if (object instanceof Request) {
                        request = (Request) object;
                        if (request.getReq().equals("Chat")) {
                            chat = request.getTo();
                            chatReqFrom = request.getFrom();
                            if (request != null && request.getAccept() != null && request.getAccept()) {
                                acceptChatReq = true;
                                chatReqFrom = request.getFrom();
                            }
                        } else if (request.getReq().equals("Status")) {
                            sendPlayerStatus(request.getFrom());
                        } else if (request.getReq().equals("Friend")) {
                            if (request.getAccept() != null && request.getAccept()) {
                                friendReq[request.getFrom()] = false;
                                friend[request.getFrom()] = true;
                            } else if (!friend[request.getFrom()]) {
                                friendReq[request.getFrom()] = true;
                            }
                        } else if (request.getReq().equals("Remove")) {
                            friend[request.getFrom()] = false;
                        } else if (request.getReq().equals("Trade")) {
                            trade = request.getTo();
                            tradeReqFrom = request.getFrom();
                            if (request != null && request.getAccept() != null && request.getAccept()) {
                                acceptTradeReq = true;
                                tradeReqFrom = request.getFrom();
                            }
                        } else if (request.getReq().equals("LockIn")) {
                            lockInFrom[request.getFrom()] = true;
                        }
                    } else if (object instanceof Message) {
                        Media mediaa = new Media(new File("sounds/jingle-bells-sms.mp3").toURI().toString());
                        MediaPlayer mediaPlayerr = new MediaPlayer(mediaa);
                        mediaPlayerr.play();
                        message = (Message) object;
                        Message finalMessage = message;
                        if (message.getMessage().contains("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:")) {
                            String[] mess = message.getMessage().split("/");
                            size = Integer.parseInt(mess[1]);
                            fileName = mess[2];
                            sender = message.getSender();
                            File file = new File("sendFiles\\" + fileName);
                            if (file.exists())
                                file.delete();
                            FileOutputStream out = new FileOutputStream("sendFiles\\" + fileName);
                            int remainingSize = size;
                            int count;
                            byte[] buffer = new byte[32768];
                            int maxRead = Math.min(32768, remainingSize);
                            while ((count = inputStream.read(buffer, 0, maxRead)) > 0) {
                                out.write(buffer, 0, count);
                                remainingSize -= count;
                                maxRead = Math.min(32768, remainingSize);
                            }
                            if (fileName.contains(".jpg") || fileName.contains(".png") || fileName.contains(".gif")) {
                                Image image = new Image("file:\\\\\\" + file.getAbsolutePath());
                                ImageView imageView = new ImageView(image);
                                Platform.runLater(() -> clientLog.add(imageView));
                            } else if (fileName.contains(".mp3")) {
                                Media media = new Media(file.toURI().toString());
                                MediaPlayer mediaPlayer = new MediaPlayer(media);
                                String finalSender = sender;
                                String finalFileName = fileName;
                                Platform.runLater(() -> clientLog.add(finalSender + ": " + finalFileName));
                                mediaPlayer.play();
                            }
                            out.close();
                        } else
                            Platform.runLater(() -> clientLog.add(finalMessage.getSender() + ": " + finalMessage.getMessage()));
                    } else if (object instanceof PlayerStatus) {
                        playerStatus = (PlayerStatus) object;
                        requestedPlayerStatus = playerStatus;
                        receivedStatusReq = true;
                    } else if (object instanceof ItemPack) {
                        itemPack = (ItemPack) object;
                        for (int j = 0; j < UI.items.size(); j++) {
                            System.out.println();
                            if (itemPack.getName().equals(UI.items.get(j).getName())) {
                                item = UI.items.get(j).clone();
                                item.setUseCount(itemPack.getUseCount());
                                item.setValid(itemPack.getValid());
                                break;
                            }
                        }
                        if (item != null) {
                            game.getPlayer().getBackpack().getItem().add(item);
                            System.out.println(item.getName() + " added to backpack;");
                        }
                    }
                } catch (Exception e) {
                    if (w) {
                        e.printStackTrace();
                        w = false;
                    }
                }
            } while (true);
        });
        clientListenerThread.setDaemon(true);
        clientListenerThread.start();
    }

    boolean w = true;


    public void sendDataPack(DataPack dataPack) {
        writeObject(dataPack);
    }

    public void sendName() {
        NamePack namePack = new NamePack(name, indexOf);
        writeObject(namePack);
    }

    public int nearAnyPlayer() {
        for (int i = 0; i < players.length; i++)
            if (Math.sqrt((player.getX() - players[i].getX()) * (player.getX() - players[i].getX()) +
                    (player.getY() - players[i].getY()) * (player.getY() - players[i].getY())) <= 60 && i != indexOf)
                return i;
        return -1;
    }

    public void sendRequest(Request request) {
        writeObject(request);
    }

    public void sendPlayerStatus(int who) {
        Double healthCurrent = 0.0;
        Double healthMaxCurrent = 0.0;
        Double healthRefill = 0.0;
        Double healthMaxRefill = 0.0;
        Double healthConsumption = 0.0;
        Double healthMinConsumption = 0.0;
        Double energyCurrent = 0.0;
        Double energyMaxCurrent = 0.0;
        Double energyRefill = 0.0;
        Double energyMaxRefill = 0.0;
        Double energyConsumption = 0.0;
        Double energyMinConsumption = 0.0;
        Double staminaCurrent = 0.0;
        Double staminaMaxCurrent = 0.0;
        Double staminaRefill = 0.0;
        Double staminaMaxRefill = 0.0;
        Double staminaConsumption = 0.0;
        Double staminaMinConsumption = 0.0;
        Double satietyCurrent = 0.0;
        Double satietyMaxCurrent = 0.0;
        Double satietyRefill = 0.0;
        Double satietyMaxRefill = 0.0;
        Double satietyConsumption = 0.0;
        Double satietyMinConsumption = 0.0;
        Double money = 0.0;
        Integer animalCount = 0;
        for (int i = 0; i < game.getPlayer().getFeature().size(); i++) {
            if (game.getPlayer().getFeature().get(i).getName().equals("Health")) {
                healthCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                healthMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                healthRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                healthMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                healthConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                healthMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            } else if (game.getPlayer().getFeature().get(i).getName().equals("Energy")) {
                energyCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                energyMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                energyRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                energyMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                energyConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                energyMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            } else if (game.getPlayer().getFeature().get(i).getName().equals("Stamina")) {
                staminaCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                staminaMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                staminaRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                staminaMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                staminaConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                staminaMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            } else if (game.getPlayer().getFeature().get(i).getName().equals("Satiety")) {
                satietyCurrent = game.getPlayer().getFeature().get(i).getCurrent();
                satietyMaxCurrent = game.getPlayer().getFeature().get(i).getMaxCurrent();
                satietyRefill = game.getPlayer().getFeature().get(i).getRefillRate();
                satietyMaxRefill = game.getPlayer().getFeature().get(i).getMaxRefillRate();
                satietyConsumption = game.getPlayer().getFeature().get(i).getConsumptionRate();
                satietyMinConsumption = game.getPlayer().getFeature().get(i).getMinConsumptionRate();
            }
        }
        money = game.getPlayer().getMoney();
        for (int i = 0; i < game.getFarm().getBarn().getBarnPart().size(); i++) {
            animalCount += game.getFarm().getBarn().getBarnPart().get(i).getAnimal().size();
        }
        PlayerStatus playerStatus = new PlayerStatus(healthCurrent, healthMaxCurrent, healthRefill, healthMaxRefill,
                healthConsumption, healthMinConsumption, energyCurrent, energyMaxCurrent, energyRefill, energyMaxRefill,
                energyConsumption, energyMinConsumption, staminaCurrent, staminaMaxCurrent, staminaRefill, staminaMaxRefill,
                staminaConsumption, staminaMinConsumption, satietyCurrent, satietyMaxCurrent, satietyRefill, satietyMaxRefill,
                satietyConsumption, satietyMinConsumption, money, animalCount, name);
        playerStatus.setNumOfReceiver(who);
        writeObject(playerStatus);
    }

    public void sendItem(Item item, int count, int who) {
        for (int i = 0; i < count; i++)
            writeObject(new ItemPack(item.isValid(), item.getName(), item.getUseCount(), who));
    }


    public void sendTextPublic(String string) {
        Message message = new Message(string, name, "Server");
        message.setAll("All");
        message.setFrom(indexOf);
        writeObject(message);
        Platform.runLater(() -> clientLog.add(message.getSender() + ": " + message.getMessage()));
    }

    public void sendTextPV(String string, int who) {
        Message message = new Message(string, name, "Server");
        message.setNumOfReceiver(who);
        writeObject(message);
        Platform.runLater(() -> clientLog.add(message.getSender() + ": " + message.getMessage()));
    }


    public void sendImagePublic(String path, String fileName) {
        try {
            File file = new File(path);
            int count;
            int size = (int) file.length();
            Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Server");
            message.setAll("All");
            writeObject(message);
            byte[] buffer = new byte[32768];
            FileInputStream in = new FileInputStream(file);
            while ((count = in.read(buffer)) > 0)
                outputStream.write(buffer, 0, count);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image image = new Image("file:\\\\\\" + path);
        ImageView imageView = new ImageView(image);
        Platform.runLater(() -> clientLog.add(imageView));
    }

    public void sendImagePV(String path, String fileName, int who) {
        try {
            File file = new File(path);
            int count;
            int size = (int) file.length();
            Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Server");
            message.setNumOfReceiver(who);
            writeObject(message);
            byte[] buffer = new byte[32768];
            FileInputStream in = new FileInputStream(file);
            while ((count = in.read(buffer)) > 0)
                outputStream.write(buffer, 0, count);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image image = new Image("file:\\\\\\" + path);
        ImageView imageView = new ImageView(image);
        Platform.runLater(() -> clientLog.add(imageView));
    }


    public void sendAudioPublic(String path, String fileName) {
        try {
            File file = new File(path);
            int count;
            int size = (int) file.length();
            Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Server");
            message.setAll("All");
            writeObject(message);
            byte[] buffer = new byte[32768];
            FileInputStream in = new FileInputStream(file);
            while ((count = in.read(buffer)) > 0)
                outputStream.write(buffer, 0, count);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> clientLog.add(name + ": " + fileName));
    }


    public void sendAudioPV(String path, String fileName, int who) {
        try {
            File file = new File(path);
            int count;
            int size = (int) file.length();
            Message message = new Message("@@@@@@@@@@@@@@@@@@@@@@@##FILESIZE:/" + size + "/" + fileName, name, "Server");
            message.setNumOfReceiver(who);
            writeObject(message);
            byte[] buffer = new byte[32768];
            FileInputStream in = new FileInputStream(file);
            while ((count = in.read(buffer)) > 0)
                outputStream.write(buffer, 0, count);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Platform.runLater(() -> clientLog.add(name + ": " + fileName));
    }


    public void closeConnection() {
        try {
            objectOutputStream.close();
            objectInputStream.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeObject(Object object) {
        synchronized (objectOutputStream) {
            try {
                objectOutputStream.writeObject(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

