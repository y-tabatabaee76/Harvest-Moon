import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class ClinicUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;

    private static Rectangle chair;
    private static Rectangle drugCabinet;
    private static Rectangle table;
    private static Rectangle mainBed;
    private static Rectangle flowerPot1;
    private static Rectangle flowerPot2;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public ClinicUI(Group root) {
        this.root = root;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Scene clinicScene = new Scene(root, 1000, 700, Color.BLACK);
        primaryStage.setTitle("Clinic");
        Image clinicPic = new Image("pics/maps/clinicMap.png");
        Image doorPic = new Image("pics/doors/clinicDoor.png");
        Image chairPic = new Image("pics/chair.png");
        Image tablePic = new Image("pics/clinicTable.png");
        Image mainBedPic = new Image("pics/bed1.png");
        Image bedPic = new Image("pics/clinicBed.png");
        Image flowerPic = new Image("pics/flowerPot4.png");
        Image drugCabinetPic = new Image("pics/drugCabinet.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(clinicPic));
        root.getChildren().add(rectangle);

        Rectangle door = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(doorPic), 1000 - tileSize * 3, tileSize);
        chair = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(chairPic), tileSize * 2, tileSize * 3);
        table = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(tablePic), tileSize, tileSize * 5);
        mainBed = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(mainBedPic), tileSize * 5, tileSize * 3);
        drugCabinet = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(drugCabinetPic), tileSize * 12, tileSize * 2);
        flowerPot1 = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(flowerPic), 1000 - tileSize * 3, 700 - tileSize * 3);
        flowerPot2 = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(flowerPic), 1000 - tileSize * 3, 700 - tileSize * 5);
        root.getChildren().addAll(door, chair, table, mainBed, drugCabinet, flowerPot1, flowerPot2);

        for (int i = 0; i < 4; i++) {
            Rectangle bed = rectangleBuilder(tileSize, tileSize * 3, new ImagePattern(bedPic), tileSize * (4 * i + 2), tileSize * 9);
            root.getChildren().add(bed);
        }

        return clinicScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.5 &&
                !(player.getX() + player.getWidth() >= drugCabinet.getX() + tileSize * 0.4 &&
                        player.getX() <= drugCabinet.getX() + drugCabinet.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= drugCabinet.getY() + drugCabinet.getHeight() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= drugCabinet.getY() + drugCabinet.getHeight() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.6 &&
                        player.getX() <= tileSize * 2.4 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.6 &&
                        player.getX() <= tileSize * 6.4 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.6 &&
                        player.getX() <= tileSize * 10.4 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 14.6 &&
                        player.getX() <= tileSize * 14.4 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2) &&
                !(player.getX() + player.getWidth() >= flowerPot1.getX() + tileSize * 0.6 &&
                        player.getX() <= flowerPot1.getX() + flowerPot1.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= flowerPot1.getY() + flowerPot1.getHeight() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= flowerPot1.getY() + flowerPot1.getHeight() + tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= flowerPot2.getX() + tileSize * 0.6 &&
                        player.getX() <= flowerPot1.getX() + flowerPot2.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= flowerPot2.getY() + flowerPot2.getHeight() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= flowerPot2.getY() + flowerPot2.getHeight() + tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= tileSize * 0 &&
                        player.getX() <= tileSize * 8.6 &&
                        player.getY() + player.getHeight() >= tileSize * 8.6 &&
                        player.getY() + player.getHeight() <= tileSize * 8.8) &&
                !(player.getX() + player.getWidth() >= tileSize * 9.2 &&
                        player.getX() <= tileSize * 8.6 &&
                        player.getY() + player.getHeight() >= tileSize * 4.2 &&
                        player.getY() + player.getHeight() <= tileSize * 4.4) &&
                !(player.getX() + player.getWidth() >= mainBed.getX() + tileSize * 0.8 &&
                        player.getX() <= mainBed.getX() + mainBed.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= mainBed.getY() + mainBed.getHeight() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= mainBed.getY() + mainBed.getHeight() + tileSize * 0.6) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + table.getHeight() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() - tileSize * 0.2)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= drugCabinet.getX() + tileSize * 0.4 &&
                        player.getX() <= drugCabinet.getX() + drugCabinet.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= drugCabinet.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= drugCabinet.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.6 &&
                        player.getX() <= tileSize * 2.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.6 &&
                        player.getX() <= tileSize * 6.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.6 &&
                        player.getX() <= tileSize * 10.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 14.6 &&
                        player.getX() <= tileSize * 14.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9 &&
                        player.getY() + player.getHeight() <= tileSize * 9.2) &&
                !(player.getX() + player.getWidth() >= flowerPot1.getX() + tileSize * 0.6 &&
                        player.getX() <= flowerPot1.getX() + flowerPot1.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= flowerPot1.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= flowerPot1.getY()) &&
                !(player.getX() + player.getWidth() >= flowerPot2.getX() + tileSize * 0.6 &&
                        player.getX() <= flowerPot1.getX() + flowerPot2.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= flowerPot2.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= flowerPot2.getY()) &&
                !(player.getX() + player.getWidth() >= tileSize * 0 &&
                        player.getX() <= tileSize * 8.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7.8 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 9.2 &&
                        player.getX() <= tileSize * 8.6 &&
                        player.getY() + player.getHeight() >= tileSize * 5.8 &&
                        player.getY() + player.getHeight() <= tileSize * 6) &&
                !(player.getX() + player.getWidth() >= mainBed.getX() + tileSize * 0.8 &&
                        player.getX() <= mainBed.getX() + mainBed.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= mainBed.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= mainBed.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY())) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= drugCabinet.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= drugCabinet.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= drugCabinet.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= drugCabinet.getY() + drugCabinet.getHeight() - tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.4 &&
                        player.getX() + player.getWidth() <= tileSize * 2.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.4 &&
                        player.getX() + player.getWidth() <= tileSize * 6.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.4 &&
                        player.getX() + player.getWidth() <= tileSize * 10.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() + player.getWidth() >= tileSize * 14.4 &&
                        player.getX() + player.getWidth() <= tileSize * 14.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() + player.getWidth() >= flowerPot1.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= flowerPot1.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= flowerPot1.getY() &&
                        player.getY() + player.getHeight() <= flowerPot1.getY() + flowerPot1.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= flowerPot2.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= flowerPot2.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= flowerPot2.getY() &&
                        player.getY() + player.getHeight() <= flowerPot2.getY() + flowerPot2.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 9 &&
                        player.getX() + player.getWidth() <= tileSize * 9.2 &&
                        player.getY() + player.getHeight() >= tileSize * 2.8 &&
                        player.getY() + player.getHeight() <= tileSize * 4.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 9 &&
                        player.getX() + player.getWidth() <= tileSize * 9.2 &&
                        player.getY() + player.getHeight() >= tileSize * 6 &&
                        player.getY() + player.getHeight() <= tileSize * 8.2) &&
                !(player.getX() + player.getWidth() >= mainBed.getX() + tileSize * 0.6 &&
                        player.getX() + player.getWidth() <= mainBed.getX() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= mainBed.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= mainBed.getY() + mainBed.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= table.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() - tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= chair.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight())) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= drugCabinet.getX() + drugCabinet.getWidth() - tileSize * 0.4 &&
                        player.getX() <= drugCabinet.getX() + drugCabinet.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= drugCabinet.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= drugCabinet.getY() + drugCabinet.getHeight() - tileSize * 0.4) &&
                !(player.getX() >= tileSize * 2.4 &&
                        player.getX() <= tileSize * 2.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() >= tileSize * 6.4 &&
                        player.getX() <= tileSize * 6.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() >= tileSize * 10.4 &&
                        player.getX() <= tileSize * 10.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() >= tileSize * 14.4 &&
                        player.getX() <= tileSize * 14.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() >= flowerPot1.getX() + flowerPot1.getWidth() - tileSize * 0.6 &&
                        player.getX() <= flowerPot1.getX() + flowerPot1.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= flowerPot1.getY() &&
                        player.getY() + player.getHeight() <= flowerPot1.getY() + flowerPot1.getHeight() + tileSize * 0.4) &&
                !(player.getX() >= flowerPot2.getX() + flowerPot2.getWidth() - tileSize * 0.6 &&
                        player.getX() <= flowerPot2.getX() + flowerPot2.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= flowerPot2.getY() &&
                        player.getY() + player.getHeight() <= flowerPot2.getY() + flowerPot2.getHeight() + tileSize * 0.4) &&
                !(player.getX() >= tileSize * 8.6 &&
                        player.getX() <= tileSize * 8.8 &&
                        player.getY() + player.getHeight() >= tileSize * 2.8 &&
                        player.getY() + player.getHeight() <= tileSize * 4.2) &&
                !(player.getX() >= tileSize * 8.6 &&
                        player.getX() <= tileSize * 8.8 &&
                        player.getY() + player.getHeight() >= tileSize * 6 &&
                        player.getY() + player.getHeight() <= tileSize * 8.6) &&
                !(player.getX() >= mainBed.getX() + mainBed.getWidth() - tileSize * 0.8 &&
                        player.getX() <= mainBed.getX() + mainBed.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= mainBed.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= mainBed.getY() + mainBed.getHeight() + tileSize * 0.4) &&
                !(player.getX() >= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= table.getY() &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() - tileSize * 0.4) &&
                !(player.getX() >= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight())) {
            return true;
        }
        return false;
    }

    public boolean closeToDrugStore(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 12 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 15 &&
                player.getY() + player.getHeight() <= tileSize * 6 &&
                player.getY() + player.getHeight() >= tileSize * 3) {
            return true;
        }
        return false;
    }

    public boolean closeToBed(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 4 &&
                player.getX() + player.getWidth() / 2 <= tileSize * 8 &&
                player.getY() + player.getHeight() <= tileSize * 7 &&
                player.getY() + player.getHeight() >= tileSize * 3) {
            return true;
        }
        return false;
    }

    public void heal(Player player, boolean[] flags) {
        HealUp healUp = new HealUp();
        UI.showPopup(healUp.run(player), root);
        flags[2] = true;

    }
}
