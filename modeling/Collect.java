
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Collect extends Task {

    public Collect(Tool tool) {
        super.name = "Collect";
        super.featureChangeRate = new HashMap<String, Double>();
        super.featureChangeRate = tool.getFeatureChangeRate();
        super.executionTime = new Date();
    }

    public String run(Player player, Tool tool, Mineral mineral, Place place, int i, int j, Rectangle[][] cellG, Group root) {
        if (tool.isValid()) {
            tool.setUseCount(tool.getUseCount() + 1);
            int num = 0;
            if (mineral.getLevel() - tool.getLevel() >= 2) {
                return "Your " + tool.getName() + " is not enough strong!";
            }
            if (mineral.getType().equals("Metal")) {
                num = (int) (Math.random() * 2 * (1 << (tool.getLevel() + 1 - mineral.getLevel()))) + 1;
            } else if (mineral.getType().equals("Wood")) {
                num = (int) (Math.random() * 2.5 * (1 << (tool.getLevel() + 1 - mineral.getLevel()))) + 1;
            }
            if (player.getBackpack().getCurrentFullness() + num < player.getBackpack().getCapacity()) {
                for (int k = 0; k < num; k++) {
                    player.getBackpack().getItem().add(mineral);
                }
                for (Map.Entry<String, Double> entry : featureChangeRate.entrySet()) {
                    for (int k = 0; k < player.getFeature().size(); k++) {
                        if (player.getFeature().get(k).getName().equals(entry.getKey())) {
                            System.out.println(entry.getKey() + " : " + entry.getValue());
                            player.getFeature().get(k).setCurrent(player.getFeature().get(k).getCurrent()
                                    + featureChangeRate.get(entry.getKey()) + tool.getFeatureChangeRate().get(entry.getKey()));
                            break;
                        }
                    }
                }
                if (place instanceof Jungle) {
                    ((Jungle) place).getContent()[i][j] = null;
                    cellG[i][j].setFill(null);
                }
                if (place instanceof Cave) {
                    ((Cave) place).getContent()[i][j] = null;
                    cellG[i][j].setFill(null);
                }
                Double random = Math.random();
                if (tool.getInvalid().getProbability() > random) {
                    tool.setValid(false);
                    return "Your " + tool.getName() + " broke after using now!";
                }
                for (int k = 0; k < num; k++) {
                    UI.missionHandler(name, mineral.getName());
                }
                return num + " " + mineral.getName() + "s where collected.\n";
            } else {
                return "Backpack in full";
            }
        } else {
            return tool.getName() + " is broken";
        }
    }

    public String run(Player player, Mineral mineral, Place place, int i, int j, Rectangle[][] cellG, Group root) {
        int num = 0;
        if (mineral.getType().equals("Metal")) {
            num = (int) (Math.random() * 2 / (1 << (mineral.getLevel()))) + 1;
        } else if (mineral.getType().equals("Wood")) {
            num = (int) (Math.random() * 5 / (1 << (mineral.getLevel()))) + 1;
        }
        Double random = Math.random() * num;
        if (player.getBackpack().getCurrentFullness() + random < player.getBackpack().getCapacity()) {
            for (int k = 0; k < random; k++) {
                player.getBackpack().getItem().add(mineral);
            }

            for (Map.Entry<String, Double> entry : featureChangeRate.entrySet()) {
                for (int k = 0; k < player.getFeature().size(); k++) {
                    if (player.getFeature().get(k).getName().equals(entry.getKey())) {
                        System.out.println(entry.getKey() + " : " + entry.getValue());
                        player.getFeature().get(k).setCurrent(player.getFeature().get(k).getCurrent()
                                + featureChangeRate.get(entry.getKey()));
                        break;
                    }
                }
            }
            if (place instanceof Jungle) {
                ((Jungle) place).getCells()[i][j].setContent(null);
                cellG[i][j].setFill(null);
            }
            if (place instanceof Cave) {
                ((Cave) place).getContent()[i][j] = null;
                cellG[i][j].setFill(null);
            }
            for (int k = 0; k < num; k++) {
                UI.missionHandler(name, mineral.getName());
            }
            return num + " " + mineral.getName() + "s where collected.";
        } else return "Backpack in full";
    }

    public String run(Player player, GardenTree[][] gardenTrees, Rectangle[][] cellG, int i, int j) {
        try {
            Image image = new Image("pics/tree/tree.png");
            int fruitNum = gardenTrees[i][j].getAvailableFruits().size();
            if (player.getBackpack().getCapacity() < player.getBackpack().getCurrentFullness() + fruitNum) {
                return "Backpack is full";
            } else {
                for (int k = 0; k < fruitNum; k++) {
                    player.getBackpack().getItem().add(gardenTrees[i][j].getFruit());
                }
                gardenTrees[i][j].setAvailableFruits(new ArrayList<>());
                cellG[i][j].setFill(new ImagePattern(image));
            }
            for (int k = 0; k < fruitNum; k++) {
                UI.missionHandler(name, gardenTrees[i][j].getFruit().getName());
            }
            return fruitNum + " " + gardenTrees[i][j].getFruit().getName() + "s where collected.";
        } catch (Exception e) {
            return "There is no Fruit to collect";
        }
    }
}
