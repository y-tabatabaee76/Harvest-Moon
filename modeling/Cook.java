import java.util.HashMap;

public class Cook extends Task {

    public Cook() {
        super.name = "Cook";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Food food, Player player, ToolShelf toolShelf) {
       for(Tool tool: food.getCookingTools()){
           if(!exists(tool, toolShelf)){
               return tool.getName() + " does not exist in the toolshelf!";
           }
       }
       player.getBackpack().getItem().add(food);
        for (Item item : food.getDissassemblity().getInitialIngredients().elementSet()) {
            player.getBackpack().getItem().remove(item, food.getDissassemblity().getInitialIngredients().count(item));
        }
        Double random = Math.random();
        UI.missionHandler(name, food.getName());
        for (Tool tool : food.getCookingTools()) {
            for(Tool tool1: toolShelf.getExistingTools()){
                if(tool1.getName().equals(tool.getName())){
                    if (tool1.getInvalid().getProbability() > random) {
                        tool1.setValid(false);
                        return (food.getName() + " is cooked! and your " + tool.getName() + " broke after using now!");
                    }
                }
            }
        }
        return (food.getName() + " is cooked!");
    }

    public boolean exists(Tool tool, ToolShelf toolShelf){
        for(Tool tool1: toolShelf.getExistingTools()){
            if(tool1.getName().equals(tool.getName()))
                return true;
        }
        return false;
    }
}
