import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class CookingUtensil extends Tool {

    public CookingUtensil(HashMap<String, Double> featureChangeRate, double capacity, String name,
                          Probability invalid, double price, ArrayList<Task> task, int maxLevel,
                          int level, Dissassemblity dissassemblity, double buildingPrice, double repairPrice,
                          HashMultiset<Item> repairItems, Image picture) {

        super("Cooking Utensil", featureChangeRate, capacity, name, invalid, price, task, maxLevel,
                level, dissassemblity, buildingPrice, repairPrice, repairItems, picture);
    }

    public CookingUtensil(){super();}
}
