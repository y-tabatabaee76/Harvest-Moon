import java.io.Serializable;

public class DataPack implements Serializable {
    private Double playerX;
    private Double playerY;
    private Double playerWidth;
    private Double playerHeight;
    private Integer index;
    private Integer dontAdd;
    private String playerImageURL;


    public DataPack(Double playerX, Double playerY, Double playerWidth, Double playerHeight, String playerImageURL) {
        this.playerX = playerX;
        this.playerY = playerY;
        this.playerWidth = playerWidth;
        this.playerHeight = playerHeight;
        this.index = -1;
        this.dontAdd = -1;
        this.playerImageURL = playerImageURL;
    }

    public Double getPlayerX() {
        return playerX;
    }

    public Double getPlayerY() {
        return playerY;
    }

    public Double getPlayerWidth() {
        return playerWidth;
    }

    public Double getPlayerHeight() {
        return playerHeight;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getDontAdd() {
        return dontAdd;
    }

    public String getPlayerImageURL() {
        return playerImageURL;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setDontAdd(Integer dontAdd) {
        this.dontAdd = dontAdd;
    }


}
