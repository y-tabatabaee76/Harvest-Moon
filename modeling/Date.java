import java.util.ArrayList;

public class Date {

    private Time time;
    private int year;
    private int month;
    private int day;
    private Season season;
    private Time dayDuration;
    private ArrayList<Task> nextDay;

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Time getDayDuration() {
        return dayDuration;
    }

    public void setDayDuration(Time dayDuration) {
        this.dayDuration = dayDuration;
    }

    public ArrayList<Task> getNextDay() {
        return nextDay;
    }

    public void setNextDay(ArrayList<Task> nextDay) {
        this.nextDay = nextDay;
    }

    public Date(Time time, int year, int month, int day, Season season, Time dayDuration) {
        this.time = time;
        this.year = year;
        this.month = month;
        this.day = day;
        this.season = season;
        this.dayDuration = dayDuration;
    }

    public Date() {
        time = new Time();
        season = new Season();
        time.setHour(6);
    }
}