import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;
import java.util.Map;

public class DestroyCrops extends Task {

    public DestroyCrops() {
        super.name = "Destroy Crops";
        super.featureChangeRate = new HashMap<String, Double>();
        featureChangeRate.put("Energy", -16.0);
        super.executionTime = new Date();
    }

    public String run(Cell[][] cell, Player player,  int x, int y, Rectangle[][] cellG, Rectangle[][] cropCellG) {
        Image image = new Image("pics/farmingField.png");
        for (int i = 0; i < cell.length; i++)
            for (int j = 0; j < cell[i].length; j++)
                if (cell[i][j] != null) {
                    cell[i][j].setContent(null);
                    cellG[i + x][j + y].setFill(new ImagePattern(image));
                    cellG[i + x][j + y].setFill(null);
                }
        for (Map.Entry<String, Double> entry : featureChangeRate.entrySet()) {
            for (int i = 0; i < player.getFeature().size(); i++) {
                if (player.getFeature().get(i).getName().equals(entry.getKey())) {
                    player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent()
                            + featureChangeRate.get(entry.getKey()));
                    break;
                }
            }
        }
        return "Successfully destroyed";
    }
}
