import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class DrinkMaker extends Task{

    public DrinkMaker() {
        super.name = "DrinkMaker";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public Drink drinkMaker(Fruit fruit) throws IllegalAccessException, InstantiationException {
        Class drinkClass = AllOfDrinks.class;
        Object obj = drinkClass.newInstance();
        Method[] drinks = drinkClass.getDeclaredMethods();
        Drink drink;
        for(Method method : drinks) {
            try {
                drink = (Drink)method.invoke(obj);
                if(drink.getName().contains(fruit.getName())) {
                    fruit = null;
                    return drink;
                }
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("unable to make drink out of this fruit");
        return null;
    }
}
