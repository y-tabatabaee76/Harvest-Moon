import java.util.HashMap;

public class Drop extends Task{
    public Drop() {
        super.name = "Drop";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public void run (Backpack backpack, Item item) {
        backpack.getItem().remove(item);
        System.out.println(item.getName() + " dropped");
        UI.missionHandler("Drop", item.getName());
    }
}
