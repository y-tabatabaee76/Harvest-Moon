import java.lang.reflect.InvocationTargetException;

public class Farm implements Place {

	private FarmingPlace farmingPlace;
	private Pond pond;
	private Barn barn;
	private Greenhouse greenhouse;
	private Garden garden;
	private Home home;

	public Farm() throws InstantiationException, IllegalAccessException {

		// TODO: 5/12/2017 - dorost kardane hameye farmingField haye zir!
		farmingPlace = new FarmingPlace();
		pond = new Pond();
		barn = new Barn(3);
		greenhouse = new Greenhouse(new Probability());
		garden = new Garden();
		try {
			home = new Home();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	public FarmingPlace getFarmingPlace() {
		return farmingPlace;
	}

	public void setFarmingPlace(FarmingPlace farmingPlace) {
		this.farmingPlace = farmingPlace;
	}

	public Pond getPond() {
		return pond;
	}

	public void setPond(Pond pond) {
		this.pond = pond;
	}

	public Barn getBarn() {
		return barn;
	}

	public void setBarn(Barn barn) {
		this.barn = barn;
	}

	public Greenhouse getGreenhouse() {
		return greenhouse;
	}

	public void setGreenhouse(Greenhouse greenhouse) {
		this.greenhouse = greenhouse;
	}

	public Garden getGarden() {
		return garden;
	}

	public void setGarden(Garden garden) {
		this.garden = garden;
	}

	public Home getHome() {
		return home;
	}

	public void setHome(Home home) {
		this.home = home;
	}
}