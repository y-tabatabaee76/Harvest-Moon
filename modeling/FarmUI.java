
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class FarmUI implements Mover {
    private Group root = new Group();
    static double tileSize = (double) (100) / 3;
    private static Rectangle home;
    private static Rectangle greenhouse;
    private static Rectangle barn;
    private static Rectangle pond;
    private Farm farm;
    private Rectangle[][] cellG;
    private Rectangle[][] gardenCellG;
    private Rectangle[][] treeCellG;
    private Rectangle[][] cropCellG;
    private static Ellipse[][] ellipse;
    private static int cellX;
    private static int cellY;


    public FarmUI(Group root, Game game) {
        this.root = root;
        this.farm = game.getFarm();
    }

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Image farmPic = new Image("pics/maps/farm-summer.png");
        Scene farmScene = new Scene(root, 1000, 700, new ImagePattern(farmPic));
        Image homePic = new Image("pics/buildings/home.png");
        Image barnPic = new Image("pics/buildings/barn.png");
        Image greenhousePic = new Image("pics/buildings/greenhouse.png");
        Image pondPic = new Image("pics/pond.png");
        Image farmingFieldPic = new Image("pics/farmingField.png");
        Image treePlacePic = new Image("pics/treePlace.png");
        Image rodePic = new Image("pics/rode.png");


        cellG = new Rectangle[9][9];
        for (int i = 0; i < 9; i++)
            cellG[i] = new Rectangle[9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                cellG[i][j] = rectangleBuilder(tileSize, tileSize, new ImagePattern(farmingFieldPic),
                        (i + 18) * tileSize, (j + 9) * tileSize);
                root.getChildren().add(cellG[i][j]);
            }
        }
        cropCellG = new Rectangle[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                cropCellG[i][j] = rectangleBuilder(tileSize, tileSize, null,
                        (i + 18) * tileSize, (j + 9) * tileSize);
                root.getChildren().add(cropCellG[i][j]);
            }
        }
        gardenCellG = new Rectangle[3][2];
        treeCellG = new Rectangle[3][2];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                gardenCellG[i][j] = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(treePlacePic),
                        (3 * i + 2) * tileSize, (3 * j + 13) * tileSize);
                root.getChildren().add(gardenCellG[i][j]);
            }
        }
        for (int i = 0; i < 4; i++) {
            Rectangle cell = rectangleBuilder(tileSize * 3, tileSize, new ImagePattern(rodePic), (i * 3 + 1) * tileSize, 11 * tileSize);
            root.getChildren().add(cell);
        }

        home = rectangleBuilder(tileSize * 4, tileSize * 3, new ImagePattern(homePic), tileSize * 2, tileSize * 3);
        barn = rectangleBuilder(tileSize * 3, tileSize * 3, new ImagePattern(barnPic), tileSize * 8, tileSize * 3);
        greenhouse = rectangleBuilder(tileSize * 3, tileSize * 5, new ImagePattern(greenhousePic), tileSize * 19, tileSize * 2);
        pond = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(pondPic), tileSize * 14, tileSize * 14);
        root.getChildren().addAll(home, barn, greenhouse, pond);

        return farmScene;
    }

    public boolean moveUp(Rectangle player) {
        if (((player.getY() + player.getHeight() >= tileSize * 2.5) ||
                player.getY() + player.getHeight() <= tileSize * 2.5 &&
                        player.getX() >= tileSize * 13.75 &&
                        player.getX() + player.getWidth() <= tileSize * 16.25) &&
                !(player.getX() + player.getWidth() >= home.getX() + 10 &&
                        player.getX() <= home.getX() + home.getWidth() - 10 &&
                        player.getY() + player.getHeight() >= home.getY() + home.getHeight() &&
                        player.getY() + player.getHeight() <= home.getY() + home.getHeight() + 10) &&
                !(player.getX() + player.getWidth() >= barn.getX() + 20 &&
                        player.getX() <= barn.getX() + barn.getWidth() - 30 &&
                        player.getY() + player.getHeight() >= barn.getY() + barn.getHeight() &&
                        player.getY() + player.getHeight() <= barn.getY() + barn.getHeight() + 10) &&
                !(player.getX() + player.getWidth() >= greenhouse.getX() + 20 &&
                        player.getX() <= greenhouse.getX() + greenhouse.getWidth() - 10 &&
                        player.getY() + player.getHeight() >= greenhouse.getY() + greenhouse.getHeight() + 10 &&
                        player.getY() + player.getHeight() <= greenhouse.getY() + greenhouse.getHeight() + 20) &&
                !(player.getX() + player.getWidth() >= pond.getX() + 10 &&
                        player.getX() <= pond.getX() + pond.getWidth() - 10 &&
                        player.getY() + player.getHeight() >= pond.getY() + pond.getHeight() + 10 &&
                        player.getY() + player.getHeight() <= pond.getY() + pond.getHeight() + 20) &&
                !(player.getX() + player.getWidth() >= tileSize * 12.25 &&
                        player.getX() <= tileSize * 13.75 &&
                        player.getY() + player.getHeight() >= tileSize * 4 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 16.25 &&
                        player.getX() <= tileSize * 17.75 &&
                        player.getY() + player.getHeight() >= tileSize * 4 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 4 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 25 &&
                        player.getX() <= tileSize * 26 &&
                        player.getY() + player.getHeight() >= tileSize * 6 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 6 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 4 &&
                        player.getX() <= tileSize * 5.25 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 10 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 7 &&
                        player.getX() <= tileSize * 8.25 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 10 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 &&
                        player.getX() <= tileSize * 6.5 &&
                        player.getY() + player.getHeight() >= tileSize * 11 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 11 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 0 &&
                        player.getX() <= tileSize * 12.5 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.5 &&
                        player.getX() <= tileSize * 3.75 &&
                        player.getY() + player.getHeight() >= tileSize * 15 &&
                        player.getY() + player.getHeight() <= tileSize * 15 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 &&
                        player.getX() <= tileSize * 6.75 &&
                        player.getY() + player.getHeight() >= tileSize * 15 &&
                        player.getY() + player.getHeight() <= tileSize * 15 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.5 &&
                        player.getX() <= tileSize * 9.75 &&
                        player.getY() + player.getHeight() >= tileSize * 15 &&
                        player.getY() + player.getHeight() <= tileSize * 15 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.5 &&
                        player.getX() <= tileSize * 3.75 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 &&
                        player.getX() <= tileSize * 6.75 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.5 &&
                        player.getX() <= tileSize * 9.75 &&
                        player.getY() + player.getHeight() >= tileSize * 18 &&
                        player.getY() + player.getHeight() <= tileSize * 18 + 10)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (((player.getY() + player.getHeight() <= tileSize * 19.25) ||
                player.getY() + player.getHeight() >= tileSize * 19.25 &&
                        player.getX() >= tileSize * 13.25 &&
                        player.getX() + player.getWidth() <= tileSize * 16) &&
                !(player.getX() + player.getWidth() >= home.getX() + 10 &&
                        player.getX() <= home.getX() + home.getWidth() - 10 &&
                        player.getY() + player.getHeight() >= home.getY() &&
                        player.getY() + player.getHeight() <= home.getY() + 10) &&
                !(player.getX() + player.getWidth() >= barn.getX() + 20 &&
                        player.getX() <= barn.getX() + barn.getWidth() - 30 &&
                        player.getY() + player.getHeight() >= barn.getY() &&
                        player.getY() + player.getHeight() <= barn.getY() + 10) &&
                !(player.getX() + player.getWidth() >= greenhouse.getX() + 20 &&
                        player.getX() <= greenhouse.getX() + greenhouse.getWidth() - 10 &&
                        player.getY() + player.getHeight() >= greenhouse.getY() + 30 &&
                        player.getY() + player.getHeight() <= greenhouse.getY() + 40) &&
                !(player.getX() + player.getWidth() >= pond.getX() + 10 &&
                        player.getX() <= pond.getX() + pond.getWidth() - 10 &&
                        player.getY() + player.getHeight() >= pond.getY() &&
                        player.getY() + player.getHeight() <= pond.getY() + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 25 &&
                        player.getX() <= tileSize * 26 &&
                        player.getY() + player.getHeight() >= tileSize * 3 + 5 &&
                        player.getY() + player.getHeight() <= tileSize * 3 + 15) &&
                !(player.getX() + player.getWidth() >= tileSize * 4 &&
                        player.getX() <= tileSize * 5.25 &&
                        player.getY() + player.getHeight() >= tileSize * 6.75 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 6.75 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 7 &&
                        player.getX() <= tileSize * 8.25 &&
                        player.getY() + player.getHeight() >= tileSize * 6.75 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 6.75 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 &&
                        player.getX() <= tileSize * 6.5 &&
                        player.getY() + player.getHeight() >= tileSize * 10 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 10 + 5) &&
                !(player.getX() + player.getWidth() >= tileSize * 0 &&
                        player.getX() <= tileSize * 12.5 &&
                        player.getY() + player.getHeight() >= tileSize * 11.25 &&
                        player.getY() + player.getHeight() <= tileSize * 11.25 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.5 &&
                        player.getX() <= tileSize * 3.75 &&
                        player.getY() + player.getHeight() >= tileSize * 13 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 &&
                        player.getX() <= tileSize * 6.75 &&
                        player.getY() + player.getHeight() >= tileSize * 13 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.5 &&
                        player.getX() <= tileSize * 9.75 &&
                        player.getY() + player.getHeight() >= tileSize * 13 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 13) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.5 &&
                        player.getX() <= tileSize * 3.75 &&
                        player.getY() + player.getHeight() >= tileSize * 16 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 16) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 &&
                        player.getX() <= tileSize * 6.75 &&
                        player.getY() + player.getHeight() >= tileSize * 16 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 16) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.5 &&
                        player.getX() <= tileSize * 9.75 &&
                        player.getY() + player.getHeight() >= tileSize * 16 - 10 &&
                        player.getY() + player.getHeight() <= tileSize * 16)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 29 &&
                !(player.getX() + player.getWidth() >= home.getX() &&
                        player.getX() + player.getWidth() <= home.getX() + 10 &&
                        player.getY() + player.getHeight() >= home.getY() + 10 &&
                        player.getY() + player.getHeight() <= home.getY() + home.getHeight()) &&
                !(player.getX() + player.getWidth() >= barn.getX() + 10 &&
                        player.getX() + player.getWidth() <= barn.getX() + 20 &&
                        player.getY() + player.getHeight() >= barn.getY() + 10 &&
                        player.getY() + player.getHeight() <= barn.getY() + barn.getHeight()) &&
                !(player.getX() + player.getWidth() >= greenhouse.getX() + 10 &&
                        player.getX() + player.getWidth() <= greenhouse.getX() + 20 &&
                        player.getY() + player.getHeight() >= greenhouse.getY() + 40 &&
                        player.getY() + player.getHeight() <= greenhouse.getY() + greenhouse.getHeight() + 10) &&
                !(player.getX() + player.getWidth() >= pond.getX() &&
                        player.getX() + player.getWidth() <= pond.getX() + 10 &&
                        player.getY() + player.getHeight() >= pond.getY() + 10 &&
                        player.getY() + player.getHeight() <= pond.getY() + pond.getHeight() + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 12.25 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 12.25 &&
                        player.getY() + player.getHeight() >= 0 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() + player.getWidth() >= tileSize * 16.25 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 16.25 &&
                        player.getY() + player.getHeight() >= 0 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() + player.getWidth() >= tileSize * 25 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 25 &&
                        player.getY() + player.getHeight() >= tileSize * 3 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 6 - 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 4 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 4 &&
                        player.getY() + player.getHeight() >= tileSize * 6.75 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9.75 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 7 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 7 &&
                        player.getY() + player.getHeight() >= tileSize * 6.75 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9.75 + 10) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 5.5 &&
                        player.getY() + player.getHeight() >= tileSize * 10.25 &&
                        player.getY() + player.getHeight() <= tileSize * 10.75) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.5 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 2.5 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 15) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 5.5 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 15) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.5 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 8.5 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 15) &&
                !(player.getX() + player.getWidth() >= tileSize * 2.5 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 2.5 &&
                        player.getY() + player.getHeight() >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 5.5 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 5.5 &&
                        player.getY() + player.getHeight() >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() + player.getWidth() >= tileSize * 8.5 - 10 &&
                        player.getX() + player.getWidth() <= tileSize * 8.5 &&
                        player.getY() + player.getHeight() >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 18)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= tileSize &&
                !(player.getX() >= home.getX() + home.getWidth() - 10 &&
                        player.getX() <= home.getX() + home.getWidth() &&
                        player.getY() + player.getHeight() >= home.getY() + 10 &&
                        player.getY() + player.getHeight() <= home.getY() + home.getHeight()) &&
                !(player.getX() >= barn.getX() + barn.getWidth() - 30 &&
                        player.getX() <= barn.getX() + barn.getWidth() - 20 &&
                        player.getY() + player.getHeight() >= barn.getY() + 10 &&
                        player.getY() + player.getHeight() <= barn.getY() + barn.getHeight()) &&
                !(player.getX() >= greenhouse.getX() + greenhouse.getWidth() - 10 &&
                        player.getX() <= greenhouse.getX() + greenhouse.getWidth() &&
                        player.getY() + player.getHeight() >= greenhouse.getY() + 40 &&
                        player.getY() + player.getHeight() <= greenhouse.getY() + greenhouse.getHeight() + 10) &&
                !(player.getX() >= pond.getX() + pond.getWidth() - 10 &&
                        player.getX() <= pond.getX() + pond.getWidth() &&
                        player.getY() + player.getHeight() >= pond.getY() + 10 &&
                        player.getY() + player.getHeight() <= pond.getY() + pond.getHeight() + 10) &&
                !(player.getX() >= tileSize * 14.25 - 20 &&
                        player.getX() <= tileSize * 14.25 - 10 &&
                        player.getY() + player.getHeight() >= 0 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() >= tileSize * 18.25 - 20 &&
                        player.getX() <= tileSize * 18.25 - 10 &&
                        player.getY() + player.getHeight() >= 0 &&
                        player.getY() + player.getHeight() <= tileSize * 4) &&
                !(player.getX() >= tileSize * 26 &&
                        player.getX() <= tileSize * 26 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 3 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 6 - 10) &&
                !(player.getX() >= tileSize * 6 - 30 &&
                        player.getX() <= tileSize * 6 - 20 &&
                        player.getY() + player.getHeight() >= tileSize * 6.75 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9.75 + 10) &&
                !(player.getX() >= tileSize * 9 - 30 &&
                        player.getX() <= tileSize * 9 - 20 &&
                        player.getY() + player.getHeight() >= tileSize * 6.75 + 10 &&
                        player.getY() + player.getHeight() <= tileSize * 9.75 + 10) &&
                !(player.getX() >= tileSize * 6.5 &&
                        player.getX() <= tileSize * 6.5 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 10.25 &&
                        player.getY() + player.getHeight() <= tileSize * 10.75) &&
                !(player.getX() >= tileSize * 13 - 10 &&
                        player.getX() <= tileSize * 13 &&
                        player.getY() + player.getHeight() >= tileSize * 12 - 5 &&
                        player.getY() + player.getHeight() <= tileSize * 12 + 5) &&
                !(player.getX() >= tileSize * 3.5 &&
                        player.getX() <= tileSize * 3.5 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 15) &&
                !(player.getX() >= tileSize * 6.5 &&
                        player.getX() <= tileSize * 6.5 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 15) &&
                !(player.getX() >= tileSize * 9.5 &&
                        player.getX() <= tileSize * 9.5 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 13 &&
                        player.getY() + player.getHeight() <= tileSize * 15) &&
                !(player.getX() >= tileSize * 3.5 &&
                        player.getX() <= tileSize * 3.5 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 6.5 &&
                        player.getX() <= tileSize * 6.5 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 18) &&
                !(player.getX() >= tileSize * 9.5 &&
                        player.getX() <= tileSize * 9.5 + 10 &&
                        player.getY() + player.getHeight() >= tileSize * 16 &&
                        player.getY() + player.getHeight() <= tileSize * 18)) {
            return true;
        }
        return false;
    }

    public int clickOnCell(MouseEvent event) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (event.getX() >= cellG[i][j].getX() &&
                        event.getX() <= cellG[i][j].getX() + cellG[i][j].getWidth() &&
                        event.getY() >= cellG[i][j].getY() &&
                        event.getY() <= cellG[i][j].getY() + cellG[i][j].getHeight()) {
                    return 9 * i + j;
                }
            }
        }
        return -1;
    }

    public boolean rightPlace(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 <= tileSize * 29 &&
                player.getX() + player.getWidth() / 2 >= tileSize * 16 &&
                player.getY() + player.getHeight() <= tileSize * 20 &&
                player.getY() + player.getHeight() >= tileSize * 7) {
            return true;
        }
        return false;
    }

    public boolean rightPlaceForFill(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 <= tileSize * 28 &&
                player.getX() + player.getWidth() / 2 >= tileSize * 22 &&
                player.getY() + player.getHeight() <= tileSize * 7 &&
                player.getY() + player.getHeight() >= tileSize * 2.5)
            return true;
        if (player.getX() + player.getWidth() / 2 <= pond.getX() + pond.getWidth() + 40 &&
                player.getX() + player.getWidth() / 2 >= pond.getX() - 40 &&
                player.getY() + player.getHeight() <= pond.getY() + pond.getHeight() + 40 &&
                player.getY() + player.getHeight() >= pond.getY() - 40)
            return true;
        return false;
    }

    public boolean closeToGreenhouse(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 <= tileSize * 22 &&
                player.getX() + player.getWidth() / 2 >= tileSize * 19 &&
                player.getY() + player.getHeight() <= tileSize * 9 &&
                player.getY() + player.getHeight() >= tileSize * 7) {
            return true;
        }
        return false;
    }

    private boolean firstPPressed = true;

    public Cell[][] chooseCell(Item item, KeyEvent event, Rectangle player, boolean[] flags) {
        AllOfTools allOfTools = new AllOfTools();
        if (firstPPressed) {
            ellipse = new Ellipse[3][3];
            for (int i = 0; i < 3; i++) {
                ellipse[i] = new Ellipse[3];
                for (int j = 0; j < 3; j++) {
                    ellipse[i][j] = new Ellipse(tileSize / 2, tileSize / 4);
                    ellipse[i][j].setFill(new ImagePattern(new Image("pics/icons/arrow.png")));
                    ellipse[i][j].setCenterX(cellG[0][0].getX() + cellG[0][0].getWidth() / 2 + j * cellG[0][0].getWidth());
                    ellipse[i][j].setCenterY(cellG[0][0].getY() + cellG[0][0].getHeight() / 4 + i * cellG[0][0].getHeight());
                }
            }
        }
        int k = 0, l = 0;
        if (item.getLevel() == 1) {
            k = 1;
            l = 1;
        } else if (item.getLevel() == 2) {
            k = 1;
            l = 2;
        } else if (item.getLevel() == 3) {
            k = 1;
            l = 3;
        } else if (item.getLevel() == 4) {
            k = 3;
            l = 3;
        }

        if (firstPPressed)
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    root.getChildren().add(ellipse[i][j]);
        firstPPressed = false;
        if (event.getCode() == KeyCode.RIGHT && ellipse[0][0].getCenterX() < cellG[0][0].getX() + cellG[0][0].getWidth() * (9.5 - l)) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterX(ellipse[i][j].getCenterX() + cellG[0][0].getWidth());
            //System.out.println("1");
            return null;
        } else if (event.getCode() == KeyCode.LEFT && ellipse[0][0].getCenterX() > cellG[0][0].getX() + cellG[0][0].getWidth() * 0.5) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterX(ellipse[i][j].getCenterX() - cellG[0][0].getWidth());
            //System.out.println("2");
            return null;
        } else if (event.getCode() == KeyCode.UP && ellipse[0][0].getCenterY() > cellG[0][0].getY() + cellG[0][0].getHeight() * 0.25) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterY(ellipse[i][j].getCenterY() - cellG[0][0].getHeight());
            //System.out.println("3");
            return null;
        } else if (event.getCode() == KeyCode.DOWN && ellipse[0][0].getCenterY() < cellG[0][0].getY() + cellG[0][0].getHeight() * (8.25 - k)) {
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    ellipse[i][j].setCenterY(ellipse[i][j].getCenterY() + cellG[0][0].getHeight());
            //System.out.println("4");
            return null;
        } else if (event.getCode() == KeyCode.ENTER) {
            //System.out.println("5");
            firstPPressed = true;
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    root.getChildren().remove(ellipse[i][j]);
            Cell[][] cell = new Cell[3][3];
            for (int i = 0; i < k; i++) {
                //cell[i] = new Cell[l];
                for (int j = 0; j < l; j++) {
                    cell[i][j] = new Cell();
                }
            }
            cellX = (int) ((ellipse[0][0].getCenterX() - cellG[0][0].getWidth() / 2 - cellG[0][0].getX()) / cellG[0][0].getWidth());
            cellY = (int) ((ellipse[0][0].getCenterY() - cellG[0][0].getHeight() / 4 - cellG[0][0].getY()) / cellG[0][0].getHeight() + 0.1);
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    cell[i][j] = farm.getFarmingPlace().getCell()
                            [(int) ((ellipse[i][j].getCenterX() - cellG[0][0].getWidth() / 2 - cellG[0][0].getX()) / cellG[0][0].getWidth())]
                            [(int) ((ellipse[i][j].getCenterY() - cellG[0][0].getHeight() / 4 - cellG[0][0].getY()) / cellG[0][0].getHeight() + 0.1)];
            flags[1] = false;
            flags[2] = true;
            return cell;
        }
        return null;
    }

    public void plow(Shovel shovel, Cell[][] cell, Player player, boolean[] flags) {
        Plow plow = new Plow(shovel);
        UI.showPopup(plow.run(shovel, cell, player, cellX, cellY, cellG), root);
        flags[3] = true;
    }

    public void watering(WateringCan wateringCan, Cell[][] cell, Player player, boolean[] flags) {
        Watering watering = new Watering(wateringCan);
        UI.showPopup(watering.run(wateringCan, cell, player, cellX, cellY, cellG), root);
        flags[3] = true;
    }

    public void destroyCrops(Cell[][] cell, Player player, boolean[] flags) {
        DestroyCrops destroyCrops = new DestroyCrops();
        UI.showPopup(destroyCrops.run(cell, player, cellX, cellY, cellG, cropCellG), root);
        flags[3] = true;
    }

    public void harvestCrops(Cell[][] cell, Player player, boolean[] flags) {
        Harvest harvest = new Harvest();
        UI.showPopup(harvest.run(cell, player, cellX, cellY, cellG, cropCellG), root);
        flags[3] = true;
    }

    public void plantSeeds(Seed seed, Cell[][] cell, Player player, boolean[] flags, Weather weather) {
        PlantSeeds plantSeeds = new PlantSeeds();
        UI.showPopup(plantSeeds.run(seed, cell, player, weather, cellX, cellY, cellG), root);
        flags[3] = true;
    }

    public void fill(WateringCan wateringCan, Player player, boolean[] flags) {
        Fill fill = new Fill();
        UI.showPopup(fill.run(wateringCan, player), root);
        flags[2] = true;
    }

    public int chosenFieldNum(Rectangle player) {
        for (int i = 0; i < gardenCellG.length; i++) {
            for (int j = 0; j < gardenCellG[0].length; j++) {
                if (player.getX() + player.getWidth() / 2 <= gardenCellG[i][j].getX() + gardenCellG[i][j].getWidth() + 30 &&
                        player.getX() + player.getWidth() / 2 >= gardenCellG[i][j].getX() - 30 &&
                        player.getY() + player.getHeight() <= gardenCellG[i][j].getY() + gardenCellG[i][j].getHeight() + 30 &&
                        player.getY() + player.getHeight() >= gardenCellG[i][j].getY() - 30) {
                    return 3 * i + j;
                }
            }
        }
      return -1;
    }

    public void plantTree(String choosedButton, boolean[] flags, int i, int j) {
        GardenTree gardenTree = new AllOfGardenTrees().apple();
        for (int k = 0; k < farm.getGarden().getGardenTree().size(); k++) {
            if(choosedButton.contains(farm.getGarden().getGardenTree().get(k).getType())){
                gardenTree = farm.getGarden().getGardenTree().get(k);
            }
        }
        treeCellG[i][j] = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(gardenTree.getPicture()),
                gardenCellG[i][j].getX(), gardenCellG[i][j].getY() - 50);
        root.getChildren().addAll(treeCellG[i][j]);
        farm.getGarden().getAvailableTree()[i][j] = gardenTree.clone();
        flags[3] = true;
    }

    public void waterTree(WateringCan wateringCan, boolean[] flags, int i, int j, Player player) {
        Watering watering = new Watering(wateringCan);
        UI.showPopup(watering.run(wateringCan, gardenCellG[i][j], player, farm.getGarden().getAvailableTree()[i][j]), root);
        flags[2] = true;
    }

    public void collectFruit(boolean[] flags, int i, int j, Player player) {
        Collect collect = new Collect(new AllOfTools().hand());
        UI.showPopup(collect.run(player, farm.getGarden().getAvailableTree(), treeCellG, i, j), root);
        flags[1] = true;
    }

    public void repair(Player player, boolean[] flags) {
        Update update = new Update();
        UI.showPopup(update.run(farm.getGreenhouse(), player), root);
        flags[3] = true;
    }

    public Rectangle[][] getCellG() {
        return cellG;
    }

    public void setCellG(Rectangle[][] cellG) {
        this.cellG = cellG;
    }

    public Rectangle[][] getGardenCellG() {
        return gardenCellG;
    }

    public void setGardenCellG(Rectangle[][] gardenCellG) {
        this.gardenCellG = gardenCellG;
    }

    public Rectangle[][] getTreeCellG() {
        return treeCellG;
    }

    public void setTreeCellG(Rectangle[][] treeCellG) {
        this.treeCellG = treeCellG;
    }

    public Rectangle[][] getCropCellG() {
        return cropCellG;
    }

    public void setCropCellG(Rectangle[][] cropCellG) {
        this.cropCellG = cropCellG;
    }



}
