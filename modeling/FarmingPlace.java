
import java.lang.reflect.Method;
import java.util.ArrayList;

public class FarmingPlace implements Place {
    private Cell[][] cell;
    private ArrayList<Task> task;


    public FarmingPlace() throws IllegalAccessException, InstantiationException {
        cell = new Cell[9][9];
        for (int i = 0; i < 9; i++){
            //cell[i] = new Cell[9];
            for (int j = 0; j < 9; j++) {
                cell[i][j] = new Cell();
            }
        }
        task = new ArrayList<>();
    }

    public Cell[][] getCell() {
        return cell;
    }

    public void setCell(Cell[][] cell) {
        this.cell = cell;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }
}
