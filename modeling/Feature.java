import java.util.ArrayList;

public class Feature {

    private String name;
    private double refillRate;
    private double maxRefillRate;
    private double consumptionRate;
    private double minConsumptionRate;
    private double maxCurrent;
    private double maxMax;
    private double current;
    private ArrayList<Task> task;

    public double getRefillRate() {
        return refillRate;
    }

    public void setRefillRate(double refillRate) {
        this.refillRate = refillRate;
    }

    public double getMaxRefillRate() {
        return maxRefillRate;
    }

    public void setMaxRefillRate(double maxRefillRate) {
        this.maxRefillRate = maxRefillRate;
    }

    public double getConsumptionRate() {
        return consumptionRate;
    }

    public void setConsumptionRate(double consumptionRate) {
        this.consumptionRate = consumptionRate;
    }

    public double getMinConsumptionRate() {
        return minConsumptionRate;
    }

    public void setMinConsumptionRate(double minConsumptionRate) {
        this.minConsumptionRate = minConsumptionRate;
    }

    public double getMaxCurrent() {
        return maxCurrent;
    }

    public void setMaxCurrent(double maxCurrent) {
        this.maxCurrent = maxCurrent;
    }

    public double getMaxMax() {
        return maxMax;
    }

    public void setMaxMax(double maxMax) {
        this.maxMax = maxMax;
    }

    public double getCurrent() {
        return current;
    }

    public void setCurrent(double current) {
        this.current = current;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }


    public Feature(String name, double refillRate, double maxRefillRate, double consumptionRate, double minConsumptionRate,
                   double maxCurrent, double maxMax, double current, ArrayList<Task> task) {
        this.name = name;
        this.refillRate = refillRate;
        this.maxRefillRate = maxRefillRate;
        this.consumptionRate = consumptionRate;
        this.minConsumptionRate = minConsumptionRate;
        this.maxCurrent = maxCurrent;
        this.maxMax = maxMax;
        this.current = current;
        this.task = task;
        this.current = maxCurrent;
    }

    public Feature(String name, double current) {
        this.name = name;
        this.refillRate = 100;
        this.maxRefillRate = 200;
        this.consumptionRate = 200;
        this.minConsumptionRate = 100;
        this.maxCurrent = 500;
        this.maxMax = 1000;
        this.current = current;
        this.task = new ArrayList<>();
        this.current = maxCurrent;
    }

    public Feature(String name, double current, double max, double consumptionRate, double refillRate) {
        this.name = name;
        this.refillRate = refillRate;
        this.maxRefillRate = 200;
        this.consumptionRate = consumptionRate;
        this.minConsumptionRate = 100;
        this.maxCurrent = max;
        this.maxMax = 1000;
        this.current = current;
        this.task = new ArrayList<>();
        this.current = maxCurrent;
    }

    public Feature() {
    }


    // TODO: 5/5/2017  - hanooz karesh tamoom nashode bayad kamelesh kard

}