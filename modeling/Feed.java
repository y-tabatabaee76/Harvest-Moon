import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.HashMap;
import java.util.Map;

public class Feed extends Task {
    public Feed() {
        super.name = "Feed";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Animal animal, Item animalFood, Player player, Rectangle[][] cellG, int x, int y, Group root) {
        Image cow = new Image("pics/cow/up3.png");
        Image sheep = new Image("pics/sheep/up3.png");
        Image chickem = new Image("pics/chicken/right3.png");
        for (Map.Entry<String, Double> entry : animalFood.getFeatureChangeRate().entrySet()) {
            for (int i = 0; i < animal.getFeature().size(); i++) {
                if (animal.getFeature().get(i).getName().equalsIgnoreCase(entry.getKey())) {
                    //if (animal.getFeature().get(i).getCurrent() >= 0.99 * animal.getFeature().get(i).getMaxCurrent()) {
                    if (animal.getFeature().get(i).getCurrent() + entry.getValue() <
                            animal.getFeature().get(i).getMaxCurrent()) {
                        animal.getFeature().get(i).setCurrent(animal.getFeature().get(i).getCurrent() + entry.getValue());
                    } else {
                        animal.getFeature().get(i).setCurrent(animal.getFeature().get(i).getMaxCurrent());
                    }
                    break;
                    //}
                }
            }
        }
        Timeline timeline;
        if(animal.getType().equals("Cow")){
            cellG[x][y].setFill(new ImagePattern(cow));
            timeline = new Timeline(new KeyFrame(Duration.millis(2000), event1 -> {
                cellG[x][y].setFill(new ImagePattern(animal.getPicture()));
            }));
        } else if (animal.getType().equals("Sheep")) {
            cellG[x][y].setFill(new ImagePattern(sheep));
            timeline = new Timeline(new KeyFrame(Duration.millis(2000), event1 -> {
                cellG[x][y].setFill(new ImagePattern(animal.getPicture()));
            }));
        }else {
            cellG[x][y].setFill(new ImagePattern(chickem));
            timeline = new Timeline(new KeyFrame(Duration.millis(2000), event1 -> {
                cellG[x][y].setFill(new ImagePattern(animal.getPicture()));
            }));
        }
        timeline.play();

        player.getBackpack().getItem().remove(animalFood);
        player.getBackpack().setCurrentFullness(player.getBackpack().getCurrentFullness() - animalFood.getCapacity());
        UI.missionHandler(name, animal.getType());
        animal.setFeed(true);
        return (animal.getType() + " was fed successfully!");
    }
}
