import java.util.HashMap;
import java.util.Map;

public class Fishing extends Task{

    public Fishing() {
        super.name = "Fishing";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Player player, FishingRod fishingRod) {
        if(fishingRod.isValid()){
            fishingRod.setUseCount(fishingRod.getUseCount() + 1);
            for(int i = 0; i < fishingRod.getFishingProbability().getProbability() * 10; i++) {
                player.getBackpack().getItem().add(new AllOfAnimalProducts().fishMeat());
            }
            for (Map.Entry<String, Double> entry : fishingRod.getFeatureChangeRate().entrySet()) {
                for (int i = 0; i < player.getFeature().size(); i++) {
                    if (player.getFeature().get(i).getName().equals(entry.getKey())) {
                        player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent() + fishingRod.getFeatureChangeRate().get(entry.getKey()));
                        break;
                    }
                }
            }
            UI.missionHandler(name, "Fishing Rod");
            Double random = Math.random();
            if (fishingRod.getInvalid().getProbability() > random) {
                fishingRod.setValid(false);
                return Double.toString(fishingRod.getFishingProbability().getProbability() * 10) +
                        " Num Fish was caught\n" + "Your Tool broke after using!";
            }
            return Double.toString(fishingRod.getFishingProbability().getProbability() * 10) + " Num Fish was caught\n";
        }
        else
            return "Fishing Rod is Broken!";
    }
}

// TODO: 5/18/2017 - ehtemale kharab shodano bayad vabaste be useCount ziad konim!
