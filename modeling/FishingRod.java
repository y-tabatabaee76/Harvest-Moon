import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class FishingRod extends Tool{

    private Probability fishingProbability;

    public Probability getFishingProbability() {
        return fishingProbability;
    }

    public void setFishingProbability(Probability fishingProbability) {
        this.fishingProbability = fishingProbability;
    }

    public FishingRod(String type, HashMap<String, Double> featureChangeRate, double capacity, String name,
                      double price, ArrayList<Task> task, int level, Dissassemblity dissassemblity, double buildingPrice,
                      double repairPrice, HashMultiset<Item> repairItems, Probability fishingProbability, Image picture) {
        super(type, featureChangeRate, capacity, name, new Probability(){{setProbability((double)(5 - level) / 20);}}, price, task, 4,
                level, dissassemblity, buildingPrice, repairPrice, repairItems, picture);
        this.fishingProbability = fishingProbability;
    }

    public FishingRod() {
    }
}
