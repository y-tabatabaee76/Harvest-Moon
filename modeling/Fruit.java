import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Fruit extends Product {

    private Weather weather;

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Fruit(HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
                 double price, ArrayList<Tool> tools, ArrayList<Task> task, boolean accumulation, Weather weather, Image picture) {
        super("Fruit", featureChangeRate, capacity, name, invalid, price, tools, task, true, accumulation,
                new Dissassemblity(0, HashMultiset.<Item>create(), false), null, picture);
        this.weather = weather;
    }

    public Fruit(){super();}

}