import java.util.ArrayList;

public class Garden implements Place {

    private ArrayList<GardenTree> gardenTree;
    private GardenTree[][] availableTree;

    public Garden() {
        gardenTree = new ArrayList<>();
        availableTree = new GardenTree[3][2];

        AllOfGardenTrees allOfGardenTrees = new AllOfGardenTrees();
        gardenTree.add(allOfGardenTrees.pomegranate());
        gardenTree.add(allOfGardenTrees.pear());
        gardenTree.add(allOfGardenTrees.orange());
        gardenTree.add(allOfGardenTrees.peach());
        gardenTree.add(allOfGardenTrees.apple());
        gardenTree.add(allOfGardenTrees.lemon());
    }

    public ArrayList<GardenTree> getGardenTree() {
        return gardenTree;
    }

    public void setGardenTree(ArrayList<GardenTree> gardenTree) {
        this.gardenTree = gardenTree;
    }

    public GardenTree[][] getAvailableTree() {
        return availableTree;
    }

    public void setAvailableTree(GardenTree[][] availableTree) {
        this.availableTree = availableTree;
    }



}