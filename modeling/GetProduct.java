import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.HashMap;

public class GetProduct extends Task{

    public GetProduct() {
        super.name = "GetProduct";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Animal animal, Item getter, Player player, Rectangle[][] cellG, int x, int y, Group root) {
        Image cow = new Image("pics/cow/up3.png");
        Image sheep = new Image("pics/sheep/up3.png");
        Image chickem = new Image("pics/chicken/right3.png");
        Timeline timeline;
        if(player.getBackpack().getCapacity() > player.getBackpack().getCurrentFullness() + 1 && animal.getProduceCycle() == animal.getProduceProcess()){
            AllOfAnimalProducts animalProducts = new AllOfAnimalProducts();
            if(animal.getType().equals("Cow")){
                player.getBackpack().getItem().add(animalProducts.milk());
                UI.missionHandler(name, "Milk");
                cellG[x][y].setFill(new ImagePattern(cow));
                timeline = new Timeline(new KeyFrame(Duration.millis(2000), event1 -> {
                    cellG[x][y].setFill(new ImagePattern(animal.getPicture()));
                }));
                timeline.play();
                return ("Cow was milked successfully");
            }
            else if(animal.getType().equals("Sheep")){
                player.getBackpack().getItem().add(animalProducts.wool());
                UI.missionHandler(name, "Wool");
                cellG[x][y].setFill(new ImagePattern(sheep));
                timeline = new Timeline(new KeyFrame(Duration.millis(2000), event1 -> {
                    cellG[x][y].setFill(new ImagePattern(animal.getPicture()));
                }));
                timeline.play();
                return ("Sheep was sheared successfully");
            }
            else if(animal.getType().equals("Chicken")){
                int num = (int)(Math.random() * 10);
                player.getBackpack().getItem().add(animalProducts.egg(), num);
                UI.missionHandler(name, "Egg");
                cellG[x][y].setFill(new ImagePattern(chickem));
                timeline = new Timeline(new KeyFrame(Duration.millis(2000), event1 -> {
                    cellG[x][y].setFill(new ImagePattern(animal.getPicture()));
                }));
                timeline.play();
                return (num + " eggs were collected");
            }
            Double random = Math.random();
            if (getter.getInvalid().getProbability() > random) {
                getter.setValid(false);
                return "Your " + getter.getName() + " broke after using now!";
            }
        }
        else {
            return "This animal doesn't have any product!";
        }
        return (animal.getType() + " backpack is full!");
    }

}
