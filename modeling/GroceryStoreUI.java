import com.google.common.collect.HashMultiset;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GroceryStoreUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Rectangle firePlace;
    private static Rectangle chair;
    private static Rectangle table;
    private static Ellipse ellipse;
    private static Item[] items;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public GroceryStoreUI(Group root) {
        this.root = root;
    }

    public Scene sceneBuilder(Stage primaryStage) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Scene groceryStoreScene = new Scene(root, 1000, 700, Color.BLACK);
        primaryStage.setTitle("Grocery Store");
        Image groceryStorePic = new Image("pics/maps/groceryMap.png");
        Image doorPic = new Image("pics/doors/groceryStoreDoor.png");
        Image tablePic = new Image("pics/woodenTable.png");
        Image chairPic = new Image("pics/modernChair.png");
        Image firePlacePic = new Image("pics/firePlace.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(groceryStorePic));
        root.getChildren().add(rectangle);

        Rectangle door = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(doorPic), 1000 - tileSize * 4, 0);
        firePlace = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(firePlacePic), tileSize * 2, tileSize);
        table = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(tablePic), tileSize * 14, tileSize * 6);
        chair = rectangleBuilder(tileSize, tileSize, new ImagePattern(chairPic), tileSize * 15, tileSize * 5);
        root.getChildren().addAll(door, firePlace, table, chair);

        //in hashmultiset good haie grocery store... chon place hamun mokhtasat mikhastan felan ghabele estefade nist hich placi inaro dasti dadam:))
        HashMultiset goods = HashMultiset.<Item>create();
        Class fruitsClass = AllOfFruits.class;
        Object obj1 = fruitsClass.newInstance();
        Method[] fruits = fruitsClass.getDeclaredMethods();
        for (Method fruit : fruits) {
            try {
                goods.add(fruit.invoke(obj1), (int) (Math.random() * 9) + 1);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        int count = 0;
        double y = tileSize * 7, x = 0;
        //in basi kasife!!!
        // TODO: 6/20/2017 badan check she ke age masalan ye mive tamom shode bod emptyBoxo bezare
        items = new Item[goods.size()];
        int cnt = 0;
        for (Object object : goods.elementSet()) {
            if (object instanceof Item && ((Item) object).getName() != "Junk") {
                items[cnt++] = (Item) object;
                String url = "pics/fruits/" + ((Item) object).getName().toLowerCase() + "Box.png";
                Image fruitPic = new Image(url);
                Rectangle box = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(fruitPic), x + tileSize * (count + 1), y);
                root.getChildren().addAll(box);
                count++;
                if (count == 7) {
                    y = tileSize * 10;
                    x -= tileSize * 7;
                } else if (count == 14)
                    count += 3;
            }
        }

        return groceryStoreScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.4 &&
                !(player.getX() + player.getWidth() >= firePlace.getX() + tileSize * 0.6 &&
                        player.getX() <= firePlace.getX() + firePlace.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= firePlace.getY() + firePlace.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= firePlace.getY() + firePlace.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + table.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() + chair.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 8.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8.6) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 11.4 &&
                        player.getY() + player.getHeight() <= tileSize * 11.6) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.4 &&
                        player.getX() <= tileSize * 17.6 &&
                        player.getY() + player.getHeight() >= tileSize * 11.4 &&
                        player.getY() + player.getHeight() <= tileSize * 11.6)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= firePlace.getX() + tileSize * 0.6 &&
                        player.getX() <= firePlace.getX() + firePlace.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= firePlace.getY() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= firePlace.getY() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY()) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chair.getY()) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 7 &&
                        player.getY() + player.getHeight() <= tileSize * 7.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 10.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.4 &&
                        player.getX() <= tileSize * 17.6 &&
                        player.getY() + player.getHeight() >= tileSize * 10 &&
                        player.getY() + player.getHeight() <= tileSize * 10.2)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20 &&
                !(player.getX() + player.getWidth() >= firePlace.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= firePlace.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= firePlace.getY() &&
                        player.getY() + player.getHeight() <= firePlace.getY() + firePlace.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= table.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= chair.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight()) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.2 &&
                        player.getX() + player.getWidth() <= tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 8.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.2 &&
                        player.getX() + player.getWidth() <= tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= tileSize * 10.2 &&
                        player.getY() + player.getHeight() <= tileSize * 11.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.2 &&
                        player.getX() + player.getWidth() <= tileSize * 11.4 &&
                        player.getY() + player.getHeight() >= tileSize * 10.2 &&
                        player.getY() + player.getHeight() <= tileSize * 11.4)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= firePlace.getX() + firePlace.getWidth() - tileSize * 0.6 &&
                        player.getX() <= firePlace.getX() + firePlace.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= firePlace.getY() &&
                        player.getY() + player.getHeight() <= firePlace.getY() + firePlace.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= table.getY() &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight()) &&
                !(player.getX() >= tileSize * 7.6 &&
                        player.getX() <= tileSize * 7.8 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 8.4) &&
                !(player.getX() >= tileSize * 7.6 &&
                        player.getX() <= tileSize * 7.8 &&
                        player.getY() + player.getHeight() >= tileSize * 10.2 &&
                        player.getY() + player.getHeight() <= tileSize * 11.4) &&
                !(player.getX() >= tileSize * 17.6 &&
                        player.getX() <= tileSize * 17.8 &&
                        player.getY() + player.getHeight() >= tileSize * 10.2 &&
                        player.getY() + player.getHeight() <= tileSize * 11.4)) {
            return true;
        }
        return false;
    }


    public int rightPlace(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 <= tileSize * 8 &&
                player.getX() + player.getWidth() / 2 >= tileSize * 1 &&
                player.getY() + player.getHeight() <= tileSize * 8 &&
                player.getY() + player.getHeight() >= tileSize * 6.5)
            return 1;
        if (player.getX() + player.getWidth() / 2 <= tileSize * 8 &&
                player.getX() + player.getWidth() / 2 >= tileSize * 1 &&
                player.getY() + player.getHeight() <= tileSize * 11 &&
                player.getY() + player.getHeight() >= tileSize * 9.5)
            return 2;
        if (player.getX() + player.getWidth() / 2 <= tileSize * 19 &&
                player.getX() + player.getWidth() / 2 >= tileSize * 11 &&
                player.getY() + player.getHeight() <= tileSize * 11 &&
                player.getY() + player.getHeight() >= tileSize * 9.5)
            return 3;
        return 0;
    }

    private boolean firstBPressed = true;

    public String buy(KeyEvent event, int place) {
        if (place == 1) {
            if (event.getCode() == KeyCode.B && firstBPressed) {
                ellipse = new Ellipse(tileSize / 2, tileSize / 4);
                ellipse.setFill(new ImagePattern(new Image("pics/icons/arrow.png")));
                ellipse.setCenterX(tileSize * 1.5);
                ellipse.setCenterY(tileSize * 7);
                root.getChildren().add(ellipse);
                firstBPressed = false;
                return "";
            } else if (event.getCode() == KeyCode.RIGHT && ellipse.getCenterX() != tileSize * 7.5) {
                ellipse.setCenterX(ellipse.getCenterX() + tileSize);
                return "";
            } else if (event.getCode() == KeyCode.LEFT && ellipse.getCenterX() != tileSize * 1.5) {
                ellipse.setCenterX(ellipse.getCenterX() - tileSize);
                return "";
            } else if (event.getCode() == KeyCode.ENTER) {
                root.getChildren().remove(ellipse);
                firstBPressed = true;
                if (ellipse.getCenterX() == tileSize * 1.5) {
                    return items[0].getName();
                } else if (ellipse.getCenterX() == tileSize * 2.5) {
                    return items[1].getName();
                } else if (ellipse.getCenterX() == tileSize * 3.5) {
                    return items[2].getName();
                } else if (ellipse.getCenterX() == tileSize * 4.5) {
                    return items[3].getName();
                } else if (ellipse.getCenterX() == tileSize * 5.5) {
                    return items[4].getName();
                } else if (ellipse.getCenterX() == tileSize * 6.5) {
                    return items[5].getName();
                } else if (ellipse.getCenterX() == tileSize * 7.5) {
                    return items[6].getName();
                }
            }
        } else if (place == 2) {
            if (event.getCode() == KeyCode.B && firstBPressed) {
                ellipse = new Ellipse(tileSize / 2, tileSize / 4);
                ellipse.setFill(new ImagePattern(new Image("pics/icons/arrow.png")));
                ellipse.setCenterX(tileSize * 1.5);
                ellipse.setCenterY(tileSize * 10);
                root.getChildren().add(ellipse);
                firstBPressed = false;
                return "";
            } else if (event.getCode() == KeyCode.RIGHT && ellipse.getCenterX() != tileSize * 7.5) {
                ellipse.setCenterX(ellipse.getCenterX() + tileSize);
                return "";
            } else if (event.getCode() == KeyCode.LEFT && ellipse.getCenterX() != tileSize * 1.5) {
                ellipse.setCenterX(ellipse.getCenterX() - tileSize);
                return "";
            } else if (event.getCode() == KeyCode.ENTER) {
                root.getChildren().remove(ellipse);
                firstBPressed = true;
                if (ellipse.getCenterX() == tileSize * 1.5) {
                    return items[7].getName();
                } else if (ellipse.getCenterX() == tileSize * 2.5) {
                    return items[8].getName();
                } else if (ellipse.getCenterX() == tileSize * 3.5) {
                    return items[9].getName();
                } else if (ellipse.getCenterX() == tileSize * 4.5) {
                    return items[10].getName();
                } else if (ellipse.getCenterX() == tileSize * 5.5) {
                    return items[11].getName();
                } else if (ellipse.getCenterX() == tileSize * 6.5) {
                    return items[12].getName();
                } else if (ellipse.getCenterX() == tileSize * 7.5) {
                    return items[13].getName();
                }
            }
        } else if (place == 3) {
            if (event.getCode() == KeyCode.B && firstBPressed) {
                ellipse = new Ellipse(tileSize / 2, tileSize / 4);
                ellipse.setFill(new ImagePattern(new Image("pics/icons/arrow.png")));
                ellipse.setCenterX(tileSize * 11.5);
                ellipse.setCenterY(tileSize * 10);
                root.getChildren().add(ellipse);
                firstBPressed = false;
                return "";
            } else if (event.getCode() == KeyCode.RIGHT && ellipse.getCenterX() != tileSize * 17.5) {
                ellipse.setCenterX(ellipse.getCenterX() + tileSize);
                return "";
            } else if (event.getCode() == KeyCode.LEFT && ellipse.getCenterX() != tileSize * 11.5) {
                ellipse.setCenterX(ellipse.getCenterX() - tileSize);
                return "";
            } else if (event.getCode() == KeyCode.ENTER) {
                root.getChildren().remove(ellipse);
                firstBPressed = true;
                if (ellipse.getCenterX() == tileSize * 11.5) {
                    return items[14].getName();
                } else if (ellipse.getCenterX() == tileSize * 12.5) {
                    return items[15].getName();
                } else if (ellipse.getCenterX() == tileSize * 13.5) {
                    return items[16].getName();
                } else if (ellipse.getCenterX() == tileSize * 14.5) {
                    return items[17].getName();
                } else if (ellipse.getCenterX() == tileSize * 15.5) {
                    return items[18].getName();
                } else if (ellipse.getCenterX() == tileSize * 16.5) {
                    return items[19].getName();
                } else if (ellipse.getCenterX() == tileSize * 17.5) {
                    return items[20].getName();
                }
            }
        }
        return "";
    }

    public void buy() {
        Buy buy = new Buy();
        //buy.run();
    }

    public void sell() {

    }
}
