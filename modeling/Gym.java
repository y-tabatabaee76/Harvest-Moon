import com.google.common.collect.HashMultiset;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class Gym extends Building {

    private ArrayList<Train> train;

    public Gym() {
        super();
        Object obj = new Object();
        Method[] methods;
        train = new ArrayList<>();
        train.add(new AllOfTrains().bodyBuilding());
        train.add(new AllOfTrains().bicycle());
        train.add(new AllOfTrains().football());
        train.add(new AllOfTrains().tredmil());
        train.add(new AllOfTrains().soccer());
        train.add(new AllOfTrains().swimming());
        train.add(new AllOfTrains().volleyball());
    }

    public Gym(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, ArrayList<Train> train) {
        super(isRuin, ruin, repairObjects, repairPrice);
        this.train= train;

    }

    public ArrayList<Train> getTrain() {
        return train;
    }

    public void setTrain(ArrayList<Train> train) {
        this.train = train;
    }
}