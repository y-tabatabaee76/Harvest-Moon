import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;
import java.util.Map;

public class Harvest extends Task {

    public Harvest() {
        super.name = "Harvest";
        super.featureChangeRate = new HashMap<String, Double>();
        featureChangeRate.put("Energy", -10.0);
        super.executionTime = new Date();
    }

    public String run(Cell[][] cell, Player player, int x, int y, Rectangle[][] cellG, Rectangle[][] cropCellG) {
        Image image = new Image("pics/farmingField.png");
        for (int i = 0; i < cell.length; i++)
            for (int j = 0; j < cell[i].length; j++) {
                if (cell[i][j] != null) {
                    if (cell[i][j].getContent() instanceof Crop) {
                        if (cropCellG[i + x][j + y].getFill() != null) {
                            cropCellG[i + x][j + y].setFill(null);
                            ((Crop) cell[i][j].getContent()).setHarvestLeft(((Crop) cell[i][j].getContent()).getHarvestLeft() - 1);
                            player.getBackpack().getItem().add(((Crop) cell[i][j].getContent()).getFruit());
                            UI.missionHandler("Harvest", ((Crop) cell[i][j].getContent()).getType());
                            return ((Crop) cell[i][j].getContent()).getType() + " harvested successfully!";
                        } else
                            return "Crops are not fully grown!";
                    } else if (cell[i][j].getContent() instanceof Fruit) {
                        cell[i][j].setContent(null);
                        cropCellG[i + x][j + y].setFill(null);
                        cellG[i + x][j + y].setFill(new ImagePattern(image));
                    } else {
                        return "There is no plant in this cell to harvest!";
                    }
                }
            }

        for (Map.Entry<String, Double> entry : featureChangeRate.entrySet()) {
            for (int i = 0; i < player.getFeature().size(); i++) {
                if (player.getFeature().get(i).getName().equals(entry.getKey())) {
                    player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent()
                            + featureChangeRate.get(entry.getKey()));
                    break;
                }
            }
        }
        return "";
    }
}