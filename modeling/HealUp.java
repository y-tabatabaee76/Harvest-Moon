import java.util.HashMap;

public class HealUp extends Task {

    public HealUp() {
        super.name = "HealUp";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Player player) {
        if(player.getMoney() < 500){
            return "You don't have enough money to Heal Up";
        }
        for (Feature feature : player.getFeature()) {
            feature.setCurrent(feature.getMaxCurrent());
        }
        UI.missionHandler(name, "Player");
        return (" You healed up for 500 Gils!");
    }
}
