import java.io.Serializable;

public class ItemPack implements Serializable {
    private Boolean valid;
    private String name;
    private Integer useCount;
    private Integer numOfReceiver = -1;

    public ItemPack(Boolean valid, String name, Integer useCount, Integer numOfReceiver) {
        this.valid = valid;
        this.name = name;
        this.useCount = useCount;
        this.numOfReceiver = numOfReceiver;
    }

    public Boolean getValid() {
        return valid;
    }

    public String getName() {
        return name;
    }

    public Integer getUseCount() {
        return useCount;
    }

    public Integer getNumOfReceiver() {
        return numOfReceiver;
    }
}
