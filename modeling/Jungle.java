import java.util.ArrayList;
import java.util.HashMap;

public class Jungle implements Place {

	private River river;
	private Object[][] content;
	private Cell[][] cells;
	private HashMap<String, Double> probability;
	private int cycle;

	public Jungle() {
		river = new River(new Probability());
		probability = new HashMap<>();
		AllOfMinerals allOfMinerals = new AllOfMinerals();
		AllOfTrees allOfTrees = new AllOfTrees();
		cells = new Cell[30][20];
		content = new Object[7][3];
		for (int i = 0; i < 30; i++) {
			for (int j = 0; j < 5; j++) {
				cells[i][j] = new Cell();
			}
		}
		for (int i = 1; i < 30; i++) {
			for (int j = 5; j < 20; j++) {
				cells[i][j] = new Cell();
				if (Math.random() > 0.95) {
					if(Math.random() > 0.5)
						cells[i][j].setContent(allOfMinerals.branch());
					else
						cells[i][j].setContent(allOfMinerals.stone());
				}
			}
		}
		ArrayList treeAndLumber = new ArrayList();
		treeAndLumber.add(allOfTrees.oak());
		treeAndLumber.add(allOfTrees.pine());
		treeAndLumber.add(allOfTrees.oldTree());
		treeAndLumber.add(allOfMinerals.oakLumber());
		treeAndLumber.add(allOfMinerals.pineLumber());
		treeAndLumber.add(allOfMinerals.oldLumber());
		treeAndLumber.add(allOfTrees.oak());
		treeAndLumber.add(allOfTrees.pine());
		treeAndLumber.add(allOfTrees.oldTree());

		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 7; i++) {
				int num = (int) (Math.random() * 9);
				content[i][j] = treeAndLumber.get(num);
			}
		}

	}

	public River getRiver() {
		return river;
	}

	public void setRiver(River river) {
		this.river = river;
	}

	public Object[][] getContent() {
		return content;
	}

	public void setContent(Object[][] content) {
		this.content = content;
	}

	public HashMap<String, Double> getProbability() {
		return probability;
	}

	public void setProbability(HashMap<String, Double> probability) {
		this.probability = probability;
	}

	public Cell[][] getCells() {
		return cells;
	}

	public void setCells(Cell[][] cells) {
		this.cells = cells;
	}

	public int getCycle() {
		return cycle;
	}

	public void setCycle(int cycle) {
		this.cycle = cycle;
	}

}