import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Kitchen {

    private ArrayList<Recipe> recipe;
    private ToolShelf toolshelf;

    public Kitchen(ArrayList<Recipe> recipe, ToolShelf toolshelf) {
        this.recipe = recipe;
        this.toolshelf = toolshelf;
    }

    public Kitchen(ArrayList<Recipe> recipe) {
        this.recipe = recipe;
        try {
            toolshelf = new ToolShelf();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public Kitchen() throws IllegalAccessException, InstantiationException, InvocationTargetException {
        recipe = new ArrayList<>();
        toolshelf = new ToolShelf();
        Class foodClass = AllOfFoods.class;
        Object obj = foodClass.newInstance();
        Method[] foods = foodClass.getDeclaredMethods();
        for (Method food : foods) {
            Food fd = (Food) food.invoke(obj);
            recipe.add(fd.getRecipe());
        }

    }

    public ArrayList<Recipe> getRecipe() {
        return recipe;
    }

    public void setRecipe(ArrayList<Recipe> recipe) {
        this.recipe = recipe;
    }

    public ToolShelf getToolshelf() {
        return toolshelf;
    }

    public void setToolshelf(ToolShelf toolshelf) {
        this.toolshelf = toolshelf;
    }
}