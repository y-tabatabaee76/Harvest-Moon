import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Laboratory extends Shop {

    private ArrayList<Tool> machine;

    public Laboratory(HashMultiset goods, ArrayList<Tool> machine) {
        super(goods, "Laboratory");
        this.machine = machine;
    }

    public Laboratory(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, HashMultiset goods, ArrayList<Tool> machine) {
        super(isRuin, ruin, repairObjects, repairPrice, goods, "Laboratory");
        this.machine = machine;
    }

    public ArrayList<Tool> getMachine() {
        return machine;
    }

    public void setMachine(ArrayList<Tool> machine) {
        this.machine = machine;
    }

}