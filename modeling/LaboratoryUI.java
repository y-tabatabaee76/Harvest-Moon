import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class LaboratoryUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;
    private static Rectangle bookcase;
    private static Rectangle desk;
    private static Rectangle chair;
    private Laboratory laboratory;


    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public LaboratoryUI(Group root, Laboratory laboratory) {
        this.root = root;
        this.laboratory = laboratory;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Scene laboratoryScene = new Scene(root, 1000, 700, Color.BLACK);
        Image laboratoryPic = new Image("pics/maps/laboratoryMap.png");
        Image doorPic = new Image("pics/doors/laboratoryDoor.png");
        Image bookcasePic = new Image("pics/labBookcase.png");
        Image deskPic = new Image("pics/labDesk.png");
        Image tablePic = new Image("pics/labTable.png");
        Image chairPic = new Image("pics/labChair.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(laboratoryPic));
        root.getChildren().add(rectangle);

        Rectangle door = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(doorPic), 1000 - tileSize * 3, tileSize);
        bookcase = rectangleBuilder(tileSize * 7, tileSize * 2, new ImagePattern(bookcasePic), tileSize * 2, tileSize * 2);
        desk = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(deskPic), 1000 - tileSize * 5, tileSize * 5);
        chair = rectangleBuilder(tileSize, tileSize * 2, new ImagePattern(chairPic), 1000 - tileSize * 4, tileSize * 3);
        root.getChildren().addAll(door, bookcase, desk, chair);

        for (int i = 0; i < 3; i++) {
            Rectangle table1 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(tablePic), tileSize * (5 * i + 1), tileSize * 5);
            Rectangle table2 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(tablePic), tileSize * (5 * i + 1), tileSize * 9);
            root.getChildren().addAll(table1, table2);
        }


        return laboratoryScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.4 &&
                !(player.getX() + player.getWidth() >= bookcase.getX() + tileSize * 0.4 &&
                        player.getX() <= bookcase.getX() + bookcase.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= bookcase.getY() + bookcase.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= bookcase.getY() + bookcase.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= desk.getX() + tileSize * 0.6 &&
                        player.getX() <= desk.getX() + desk.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= desk.getY() + desk.getHeight() + tileSize * 0 &&
                        player.getY() + player.getHeight() <= desk.getY() + desk.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.6 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() + chair.getHeight() + tileSize * 0 &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 2.6 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.4 &&
                        player.getX() <= tileSize * 12.6 &&
                        player.getY() + player.getHeight() >= tileSize * 8 &&
                        player.getY() + player.getHeight() <= tileSize * 8.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 2.6 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.4 &&
                        player.getX() <= tileSize * 12.6 &&
                        player.getY() + player.getHeight() >= tileSize * 12 &&
                        player.getY() + player.getHeight() <= tileSize * 12.2)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= bookcase.getX() + tileSize * 0.4 &&
                        player.getX() <= bookcase.getX() + bookcase.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= bookcase.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= bookcase.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= desk.getX() + tileSize * 0.6 &&
                        player.getX() <= desk.getX() + desk.getWidth() - tileSize * 0.8 &&
                        player.getY() + player.getHeight() >= desk.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= desk.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.6 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() <= chair.getY() + tileSize * 0.8) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 2.6 &&
                        player.getY() + player.getHeight() >= tileSize * 5.2 &&
                        player.getY() + player.getHeight() <= tileSize * 5.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 5.2 &&
                        player.getY() + player.getHeight() <= tileSize * 5.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.4 &&
                        player.getX() <= tileSize * 12.6 &&
                        player.getY() + player.getHeight() >= tileSize * 5.2 &&
                        player.getY() + player.getHeight() <= tileSize * 5.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.4 &&
                        player.getX() <= tileSize * 2.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.4 &&
                        player.getX() <= tileSize * 7.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.4 &&
                        player.getX() <= tileSize * 12.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.2 &&
                        player.getY() + player.getHeight() <= tileSize * 9.4)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= bookcase.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= bookcase.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= bookcase.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= bookcase.getY() + bookcase.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= desk.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= desk.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= desk.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= desk.getY() + desk.getHeight()) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= chair.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight()) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.2 &&
                        player.getX() + player.getWidth() <= tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= tileSize * 5.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.2 &&
                        player.getX() + player.getWidth() <= tileSize * 6.4 &&
                        player.getY() + player.getHeight() >= tileSize * 5.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.2 &&
                        player.getX() + player.getWidth() <= tileSize * 11.4 &&
                        player.getY() + player.getHeight() >= tileSize * 5.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() + player.getWidth() >= tileSize * 1.2 &&
                        player.getX() + player.getWidth() <= tileSize * 1.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() + player.getWidth() >= tileSize * 6.2 &&
                        player.getX() + player.getWidth() <= tileSize * 6.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() + player.getWidth() >= tileSize * 11.2 &&
                        player.getX() + player.getWidth() <= tileSize * 11.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 12)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= bookcase.getX() + bookcase.getWidth() - tileSize * 0.4 &&
                        player.getX() <= bookcase.getX() + bookcase.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= bookcase.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= bookcase.getY() + bookcase.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= desk.getX() + desk.getWidth() - tileSize * 0.8 &&
                        player.getX() <= desk.getX() + desk.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= desk.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= desk.getY() + desk.getHeight()) &&
                !(player.getX() >= chair.getX() + chair.getWidth() - tileSize * 0.6 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() + tileSize * 0.8 &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight()) &&
                !(player.getX() >= tileSize * 2.6 &&
                        player.getX() <= tileSize * 2.8 &&
                        player.getY() + player.getHeight() >= tileSize * 5.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 7.6 &&
                        player.getX() <= tileSize * 7.8 &&
                        player.getY() + player.getHeight() >= tileSize * 5.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 12.6 &&
                        player.getX() <= tileSize * 12.8 &&
                        player.getY() + player.getHeight() >= tileSize * 5.4 &&
                        player.getY() + player.getHeight() <= tileSize * 8) &&
                !(player.getX() >= tileSize * 2.6 &&
                        player.getX() <= tileSize * 2.8 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() >= tileSize * 7.6 &&
                        player.getX() <= tileSize * 7.8 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 12) &&
                !(player.getX() >= tileSize * 12.6 &&
                        player.getX() <= tileSize * 12.8 &&
                        player.getY() + player.getHeight() >= tileSize * 9.4 &&
                        player.getY() + player.getHeight() <= tileSize * 12)) {
            return true;
        }
        return false;
    }

    public void make(Item choosedItem, Player player, Barn barn, boolean[] flags, BarnUI barnUI) {
        Make make = new Make();
        UI.showPopup(make.run((Tool)choosedItem, player, laboratory, barn, barnUI), root);
        flags[3] = true;
    }
}
