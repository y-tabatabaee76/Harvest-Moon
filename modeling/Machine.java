import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Machine extends Tool {
    private HashMultiset input;
    private HashMultiset output;

    public HashMultiset getInput() {
        return input;
    }

    public void setInput(HashMultiset input) {
        this.input = input;
    }

    public HashMultiset getOutput() {
        return output;
    }

    public void setOutput(HashMultiset output) {
        this.output = output;
    }

    public Machine(HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
                   double price, ArrayList<Task> task, int maxLevel, int level, Dissassemblity dissassemblity, double buildingPrice,
                   double repairPrice, HashMultiset<Item> repairItems, Image picture) {
        super("Machine", featureChangeRate, capacity, name, invalid, price, task, maxLevel, level, dissassemblity, buildingPrice,
                repairPrice, repairItems, picture);
    }

    public Machine() {
    }

    public Machine(HashMultiset input, HashMultiset output) {
        this.input = input;
        this.output = output;
    }
}
