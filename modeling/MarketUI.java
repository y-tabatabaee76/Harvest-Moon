import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;

public class MarketUI implements Mover {
    Group root = new Group();
    static double tileSize = 50;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public MarketUI(Group root) {
        this.root = root;
    }

    public Scene sceneBuilder(Stage primaryStage) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Scene marketScene = new Scene(root, 1000, 700, Color.BLACK);
        Image marketPic = new Image("pics/maps/marketMap.png");
        Image butcheryPic = new Image("pics/butchery.png");
        Image groceryPic = new Image("pics/grocery.png");
        Image generalStorePic = new Image("pics/generalStore.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(marketPic));
        root.getChildren().add(rectangle);

        Rectangle butchery = rectangleBuilder(tileSize * 5, tileSize * 8, new ImagePattern(butcheryPic), tileSize, tileSize * 4);
        Rectangle grocery = rectangleBuilder(tileSize * 5, tileSize * 8, new ImagePattern(groceryPic), tileSize * 7, tileSize * 4);
        Rectangle generalStore = rectangleBuilder(tileSize * 5, tileSize * 8, new ImagePattern(generalStorePic), tileSize * 13, tileSize * 4);
        root.getChildren().addAll(butchery, grocery, generalStore);


        return marketScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 11) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2) {
            return true;
        }
        return false;
    }

}