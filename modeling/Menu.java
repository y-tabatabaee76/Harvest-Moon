import java.util.ArrayList;

public class Menu {

    private ArrayList<String> foodName;

    public Menu(ArrayList<String> foodName) {
        this.foodName = foodName;
    }

    public ArrayList<String> getFoodName() {
        return foodName;
    }

    public void setFoodName(ArrayList<String> foodName) {
        this.foodName = foodName;
    }
}