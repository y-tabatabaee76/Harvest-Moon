

import java.io.Serializable;

public class Message implements Serializable{

    private String sender;
    private String receiver;
    private String message;
    private Integer numOfReceiver = -1;
    private Integer from = -1;
    private String all = "No";

    public Message(String message, String sender, String receiver) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    public Message(String senderANDmessage, String receiver) {
        this.message = senderANDmessage;
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNumOfReceiver() {
        return numOfReceiver;
    }

    public void setNumOfReceiver(Integer numOfReceiver) {
        this.numOfReceiver = numOfReceiver;
    }

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }
}
