import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Mineral extends Item {

    private Object source;
    private String tools;

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public String getTools() {
        return tools;
    }

    public void setTools(String tools) {
        this.tools = tools;
    }

    Mineral(String type, HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
            double price, int level, String tools, ArrayList<Task> task, Object source, Image picture) {
        super(type, featureChangeRate, capacity, name, invalid, price, task, 4, level, false,
                false, new Dissassemblity(0, HashMultiset.<Item>create(), false), picture);
        this.source = source;
        this.tools = tools;

    }
}