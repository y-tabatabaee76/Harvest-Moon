import javafx.scene.shape.Rectangle;

public interface Mover {
    boolean moveUp(Rectangle player);

    boolean moveDown(Rectangle player);

    boolean moveRight(Rectangle player);

    boolean moveLeft(Rectangle player);
}
