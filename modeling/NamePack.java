import java.io.Serializable;

public class NamePack implements Serializable {
    private String playerName;
    private Integer indexOf;

    public NamePack(String playerName, Integer indexOf) {
        this.playerName = playerName;
        this.indexOf = indexOf;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getIndexOf() {
        return indexOf;
    }

    public void setIndexOf(Integer indexOf) {
        this.indexOf = indexOf;
    }
}
