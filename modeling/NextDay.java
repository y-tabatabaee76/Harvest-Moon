import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

import java.util.ArrayList;
import java.util.HashMap;

public class NextDay extends Task{
    public NextDay() {
        super.name = "NextDay";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Game game, Date date, Player player) {
        run(game.getCave(), UI.caveUI);
        run(game.getFarm(), UI.farmUI);
        run(game.getFarm().getGreenhouse(), UI.greenhouseUI);
        run(game.getJungle(), UI.jungleUI);
        run(game.getFarm().getBarn());
        run(player);
        date.setDay(date.getDay() + 1);
        date.getTime().setHour(6);
        String[] string = {"Next Day started! Date : " , "Le lendemain a commencé! Date:",
                "¡El día siguiente comenzó! Fecha:"};
        return string[UI.language] + date.getDay();
    }

    private void run(Cave cave, CaveUI caveUI) {
        ArrayList<Mineral> minerals = new ArrayList<>();
        AllOfMinerals allOfMinerals = new AllOfMinerals();
        minerals.add(allOfMinerals.adamantiumOre());
        minerals.add(allOfMinerals.ironOre());
        minerals.add(allOfMinerals.silverOre());
        minerals.add(allOfMinerals.stone());
        for (int i = 0; i < cave.getContent().length; i++) {
            for (int j = 0; j < cave.getContent()[0].length; j++) {
                if(!(cave.getContent()[i][j] instanceof Mineral)){
                    int num = (int) (Math.random() * 4);
                    if(Math.random() * 3 <= (4 - cave.getCycle())) {
                        cave.getContent()[i][j] = minerals.get(num);
                        caveUI.getCellG()[i][j].setFill(new ImagePattern(minerals.get(num).getPicture()));
                    }
                }
            }
        }
        cave.setCycle(cave.getCycle() - 1);
        if(cave.getCycle() == 0){
            cave.setCycle(3);
        }
    }

    private void run(Jungle jungle, JungleUI jungleUI) {
        ArrayList<Mineral> minerals = new ArrayList<>();
        AllOfMinerals allOfMinerals = new AllOfMinerals();
        minerals.add(allOfMinerals.stone());
        minerals.add(allOfMinerals.branch());
        for (int i = 0; i < jungle.getCells().length; i++) {
            for (int j = 5; j < jungle.getCells()[0].length; j++) {
                try {
                    if(!(jungle.getCells()[i][j].getContent() instanceof Mineral)){
                        if(Math.random() * 3 <= (4 - jungle.getCycle())) {
                            if (Math.random() > 0.95) {
                                if(Math.random() > 0.5) {
                                    jungle.getCells()[i][j].setContent(allOfMinerals.branch());
                                    jungleUI.getCellG()[i][j].setFill(new ImagePattern(allOfMinerals.branch().getPicture()));
                                }
                                else {
                                    jungle.getCells()[i][j].setContent(allOfMinerals.stone());
                                    jungleUI.getCellG()[i][j].setFill(new ImagePattern(allOfMinerals.stone().getPicture()));
                                }
                            }
                        }
                    }
                }catch (Exception e){
                }
            }
        }
        jungle.setCycle(jungle.getCycle() - 1);
        if(jungle.getCycle() == 0){
            jungle.setCycle(3);
        }
    }

    private void run(Farm farm, FarmUI farmUI) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                try{
                    if(farm.getGarden().getAvailableTree()[i][j].isWatered()){
                        ArrayList<Fruit> fruits = new ArrayList<>();
                        for(int k = 0; k < Math.random() * 3 + 1; k++)
                            fruits.add(farm.getGarden().getAvailableTree()[i][j].getFruit());
                        farm.getGarden().getAvailableTree()[i][j].setAvailableFruits(fruits);
                        farmUI.getTreeCellG()[i][j].setFill(new ImagePattern(farm.getGarden().getAvailableTree()[i][j].getPicture()));
                    }
                    farm.getGarden().getAvailableTree()[i][j].setWatered(false);
                }catch (Exception e){
                }
            }
        }
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                try{
                    if(farm.getFarmingPlace().getCell()[i][j].getContent() instanceof Crop){
                        if(farm.getFarmingPlace().getCell()[i][j].isWatered()){
                            ((Crop) farm.getFarmingPlace().getCell()[i][j].getContent()).setAge(((Crop) farm.getFarmingPlace().getCell()[i][j].getContent()).getAge() + 1);
                            if( ((Crop) farm.getFarmingPlace().getCell()[i][j].getContent()).getAge() % ((Crop) farm.getFarmingPlace().getCell()[i][j].getContent()).getLifeCycle() == 0) {
                                farmUI.getCropCellG()[i][j].setFill(new ImagePattern(((Crop) farm.getFarmingPlace().getCell()[i][j].getContent()).getPicture()));
                            }
                            if(((Crop) farm.getFarmingPlace().getCell()[i][j].getContent()).getAge() ==  ((Crop) farm.getFarmingPlace().getCell()[i][j].getContent()).getSpoilageTime()){
                                farm.getFarmingPlace().getCell()[i][j].setContent(new AllOfFruits().junk());
                                farmUI.getCropCellG()[i][j].setFill(new ImagePattern(((Fruit) farm.getFarmingPlace().getCell()[i][j].getContent()).getPicture()));
                            }
                        }
                    }
                    else if(farm.getFarmingPlace().getCell()[i][j].getContent() instanceof Fruit){

                    }
                }catch (Exception e){
                }
            }

        }
    }

    private void run(Greenhouse greenhouse, GreenhouseUI greenhouseUI){
       for(int k = 0; k < greenhouse.getFarmingField().size(); k ++){
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    try{
                        if(greenhouse.getFarmingField().get(k).getCell()[i][j].getContent() instanceof Crop){
                            ((Crop) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).setAge(((Crop) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).getAge() + 1);
                            if( ((Crop) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).getAge() % ((Crop) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).getLifeCycle() == 0) {
                                greenhouseUI.getFieldCrop().get(k)[i][j].setFill(new ImagePattern(((Crop) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).getPicture()));
                            }
                            if(((Crop) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).getAge() ==  ((Crop) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).getSpoilageTime()){
                                greenhouse.getFarmingField().get(k).getCell()[i][j].setContent(new AllOfFruits().junk());
                                greenhouseUI.getFieldCrop().get(k)[i][j].setFill(new ImagePattern(((Fruit) greenhouse.getFarmingField().get(k).getCell()[i][j].getContent()).getPicture()));
                            }
                        }
                        else if(greenhouse.getFarmingField().get(k).getCell()[i][j].getContent() instanceof Fruit){

                        }
                    }catch (Exception e){
                    }
                }
            }
        }
    }

    private void run(Barn barn) {
        for(BarnPart barnPart: barn.getBarnPart()){
            run(barnPart);
        }
    }

    private void run(BarnPart barnPart) {
        for(Animal animal: barnPart.getAnimal()){
            run(animal);
        }
    }

    private void run(Animal animal) {
        if(animal.isFeed()){
            if(animal.getProduceProcess() < animal.getProduceCycle()) {
                animal.setProduceProcess(animal.getProduceProcess() + 1);
            }
             else{
                HashMultiset products = HashMultiset.<Product>create();
                for(Product product: animal.getProduct()){
                    products.add(product, (int)(Math.random() * 3) + 1);
                }
                animal.setAvailabeProducts(products);
                animal.setProduceProcess(0);
            }
        }

        animal.setAge(animal.getAge() + 1);
        animal.setFeed(false);
    }

    private void run(FarmingField farmingField) {
      // run(farmingField.getCrop());
       for(Cell[] cells: farmingField.getCell()) {
           for (Cell cell : cells) {
               cell.setPlowed(false);
               cell.setWatered(false);
           }
       }
    }

    private void run(Player player) {
        for(Item item: player.getBackpack().getItem()){
            if(item instanceof Product){
                if(((Product) item).getExpirationTime() == 0) {
                    item.setValid(false);
                    item.setPicture(new Image("pics/fruits/junk.png"));
                }
                else {
                    ((Product) item).setExpirationTime(((Product) item).getExpirationTime() - 1);
                }
            }
        }
    }









}
