import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Option extends Task {

    public Option() {
        super.name = "Option";
        super.featureChangeRate = new HashMap<>();
        super.executionTime = new Date();
    }

    /*public void inspectField(Farm farm) {
        int cnt = 0;
        StringBuilder data = new StringBuilder("Field:\n");
        for (int i = 0; i < farm.getFarmingPlace().size(); i++) {
            for (int j = 0; j < farm.getFarmingPlace().get(i).getFarmingField().size(); j++) {
                cnt++;
                data.append(cnt);
                data.append(". ");
                try {
                    data.append(farm.getFarmingPlace().get(i).getFarmingField().get(j).getCrop().getType());
                }
                catch (Exception e){
                    data.append("Empty");
                }
                data.append(" Field\n");
            }
        }
        System.out.println(data);
    }*/

   /* public void inspectField(Greenhouse greenhouse) {
        int cnt = 0;
        StringBuilder data = new StringBuilder("Field:\n");
        for (int j = 0; j < greenhouse.getFarmingField().size(); j++) {
            cnt++;
            data.append(cnt);
            data.append(". ");
            try {
                data.append(greenhouse.getFarmingField().get(j).getCrop().getType());
            }
            catch (Exception e){
                data.append("Empty");
            }
            data.append(" Field\n");
        }
        System.out.println(data);
    }*/

    public void inspectFarmingPlace(FarmingPlace farmingPlace) {
        StringBuilder data = new StringBuilder("Farming Field:\n");
        data.append("1. Inspect Field\n");
        data.append("2. Extend Field\n");
        System.out.println(data);
    }

    public void inspectSpecialField(FarmingField farmingField) {
        try {
            StringBuilder data = new StringBuilder();
            //data.append(farmingField.getCrop().getType());
            data.append(" Field:\n");
            data.append("1. Status\n");
            data.append("2. Water this field\n");
            data.append("3. Harvest crops\n");
            data.append("4. Destroy crops\n");
            System.out.println(data);
        }
        catch (Exception e){
            StringBuilder data = new StringBuilder("Empty Field:\n");
            data.append("1. Status\n");
            data.append("2. Plow this field\n");
            data.append("3. Water this field\n");
            data.append("4. Plant seeds\n");
            System.out.println(data);
        }
    }

    public ArrayList<String> inspectFruitGarden(Garden garden, Weather weather) {
        ArrayList<String> data = new ArrayList<>();
        for (int i = 0; i < garden.getGardenTree().size(); i++) {
            if(garden.getGardenTree().get(i).getWeather().getType().equals(weather.getType()))
            data.add(garden.getGardenTree().get(i).getType() + " Tree");
        }
        return data;
    }

    public ArrayList<String> inspectBed(Bed bed) {
        ArrayList<String> data = new ArrayList<>();
        data.add("Sleep and save game\n");
        data.add("Sleep without saving\n");
        return data;
    }

    public void saveSlot(Bed bed) {
        StringBuilder data = new StringBuilder("Choose a save slot:\n");
        for (int i = 0; i < bed.getSave().size(); i++) {
            data.append(i + 1);
            data.append(". ");
            data.append(bed.getSave().get(i).getName());
        }
        data.append("4. Create New Save Slot…");
        System.out.println(data);
    }

    public ArrayList<String> inspectBoard() {
        ArrayList<String> data = new ArrayList<>();
        data.add("Check available missions");
        data.add("Manage active missions");
        return data;
    }

    public void inspectBackpack(Backpack backpack) {
        StringBuilder data = new StringBuilder();
        int count = 0;
        for (Item item : backpack.getItem().elementSet()) {
            count++;
            data.append(count);
            data.append(". ");
            data.append(item.getName());
            if (backpack.getItem().count(item) > 1) {
                data.append(" x");
                data.append(backpack.getItem().count(item));
            }
            data.append("\n");
        }
        if(backpack.getAnimals().size() > 0)
            data.append((count + 1) + ". Fish x" + backpack.getAnimals().size());
        System.out.println(data);
    }

    public void inspectRiver(River river) {
        StringBuilder data = new StringBuilder("River:\n");
        data.append("1. Start fishing\n");
        System.out.println(data);
    }

    public void inspectPond(Pond pond) {
        StringBuilder data = new StringBuilder("Pond:\n");
        data.append("1. Fill a Watering Can\n");
        System.out.println(data);
    }

    public void inspectRocks(Jungle jungle) {
        StringBuilder data = new StringBuilder("Rocks:\n");
        data.append("1. Collect Stone\n");
        System.out.println(data);
    }

    public void inspectRocks(Cave cave) {
        // TODO: 5/20/2017  this can be changed
        StringBuilder data = new StringBuilder("Rocks:\n");
        data.append("1. Collect Stone\n" +
                "2. Collect Iron Ore\n" +
                "3. Collect Silver Ore\n" +
                "4. Collect Adamantium Ore\n");
        System.out.println(data);
    }

    public void inspectWoods(Jungle jungle) {
        // FIXME: 5/16/2017 this can be changed
        StringBuilder data = new StringBuilder("Woods:\n");
        data.append("1. Collect Branch\n" +
                "2. Collect Old Lumber\n" +
                "3. Collect Pine Lumber\n" +
                "4. Collect Oak Lumber\n");
        System.out.println(data);
    }

    public void inspectTree(GardenTree gardenTree) {
        StringBuilder data = new StringBuilder();
        if (gardenTree.isFree()) {
            data.append(gardenTree.getType());
            data.append(" Tree:\n");
            data.append("1. Status\n");
            data.append("2. Water this tree\n");
            data.append("3. Collect fruits\n");
        } else {
            data.append("You will buy ");
            data.append(gardenTree.getType());
            data.append(" for ");
            data.append(gardenTree.getPrice());
            data.append(" Gil.Is this okay? (Y/N)");
        }
        System.out.println(data);
    }

    public void inspectKitchen(Kitchen kitchen) {
        StringBuilder data = new StringBuilder("Kitchen:\n");
        data.append("1. Cook a meal\n");
        data.append("2. Check Tool Shelf\n");
        data.append("3. Check recipes\n");
        System.out.println(data);
    }

    public void inspectStorageBox(StorageBox storageBox) {
        StringBuilder data = new StringBuilder("Storage Box:\n");
        data.append("1. Put in item\n");
        data.append("2. Take out item\n");
        System.out.println(data);
    }

    public void checkStorageBox(StorageBox storageBox) {
        StringBuilder data = new StringBuilder("Storage Box:\n");
        int count = 1;
        for(Item item : storageBox.getItem()){
            data.append( count + ". " + item.getName() + "\n");
            count ++;
        }
        System.out.println(data);
    }

    public void inspectAnimal(Animal animal, BarnPart barnPart) {
        StringBuilder data = new StringBuilder();
        data.append(animal.getType());
        data.append(" ");
        data.append("No" + (barnPart.getAnimal().indexOf(animal) + 1) + "\n");
        data.append("1. Status\n");
        data.append("2. Feed this ");
        data.append(animal.getType());
        data.append("\n3. Heal this ");
        data.append(animal.getType());
        switch (animal.getType()) {
            case "Cow":
                data.append("\n4. Milk this Cow\n");
                break;
            case "Chicken":
                data.append("\n4. Collect Eggs\n");
                break;
            case "Sheep":
                data.append("\n4. Shear this Sheep\n");
                break;
        }
        System.out.println(data);
    }

    public ArrayList<String> inspectMachine() {
        ArrayList<String> data = new ArrayList<>();
        data.add("Status");
        data.add("Use this machine");
        return data;
    }

    public void inspectShop(Shop shop) {
        StringBuilder data = new StringBuilder();
        data.append(shop.getName());
        data.append(":\n");
        if (shop.getName().equals("Cafe")) {
            data.append("1. Mission Board\n");
            data.append("2. Dining Table\n");
        } else if(shop.getName().equals("Laboratory")){
            data.append("1. Check machines\n");
            data.append("2. Build a machine\n");
        }
        else {
            data.append("1. Check this shop\n");
            data.append("2. Buy an item\n");
            switch (shop.getName()) {
                case "Ranch":
                    data.append("3. Buy an animal\n");
                    break;
                case "Clinic":
                    data.append("3. Heal up\n");
                    break;
                case "Butchery":
                    data.append("3. Sell an animal\n");
                    break;
                default:
                    data.append("3. Sell an item\n");
                    break;
            }
        }
        System.out.println(data);
    }

    public ArrayList<String> inspectWorkshop(Workshop workshop) {
        ArrayList<String> data = new ArrayList<>();
        data.add("Check this shop\n");
        data.add("Make a tool\n");
        data.add("Repair a tool\n");
        data.add("Disassemble a tool\n");
        return data;
    }

    public ArrayList<String> inspectItem(Item item) {
        ArrayList<String> data = new ArrayList<>();
        data.add("Status");
        switch (item.getType()) {
            case "Food":
            case "Fruit":
            case "Drink":
            case "Potion":
            case "Processed Product":
                data.add("Use");
                data.add("Drop this item");
                break;
            default:
                data.add("Drop this item");
                break;
        }
        return data;
    }

    public void inspectItem(Crop crop) {
        StringBuilder data = new StringBuilder();
        data.append(crop.getType());
        data.append(":\n1. Status\n");
        data.append("2. Drop this item\n");
        System.out.println(data);
    }

    public ArrayList<String> printFoodsName(Kitchen kitchen) {
        ArrayList<String> data = new ArrayList<>();
        for (int i = 0; i < kitchen.getRecipe().size(); i++) {
            data.add(kitchen.getRecipe().get(i).getFood().getName());
        }
        return data;
    }

    public ArrayList<String> printTrainsName(Gym gym) {
        ArrayList<String> data = new ArrayList<>();
        for (int i = 0; i < gym.getTrain().size(); i++) {
            data.add(gym.getTrain().get(i).getName());
        }
        return data;
    }

    public ArrayList<String> inspectToolShelf(ToolShelf toolShelf) {
        StringBuilder data;
        ArrayList<String> datas = new ArrayList<>();
        datas.add(String.format("%-20s%-20s\n", "Tool name", "Available"));
        int count = 1;
        outer:
        for (String toolName : toolShelf.getCookingTools()) {
            data = new StringBuilder();
            for (Tool tool : toolShelf.getExistingTools()) {
                try {
                    if (toolName.equals(tool.getName())) {
                        data.append(String.format("%-2s", count));
                        data.append(". ");
                        data.append(String.format("%-20s", toolName));
                        data.append(String.format("%-20s", "Yes"));
                        data.append("\n");
                        count ++;
                        datas.add(data.toString());
                        continue outer;
                    }
                } catch (Exception e) {

                }
            }
            data.append(String.format("%-2s", count));
            data.append(". ");
            data.append(String.format("%-20s", toolName));
            data.append(String.format("%-20s", "No"));
            data.append("\n");
            count ++;
            datas.add(data.toString());
        }
        return datas;
    }

    public ArrayList<String> inspectCookingTool(boolean exist) {
        ArrayList<String> data = new ArrayList<>();
        if(exist){
            data.add("Status\n");
            data.add("Replace this tool\n");
            data.add("Remove this tool");
        }else {
            data.add("Put in toolShelf\n");
        }
        return data;
    }

    public ArrayList<String> inspectWeatherMachine() {
        ArrayList<String> data = new ArrayList<>();
        data.add("Spring weather");
        data.add("Summer weather");
        data.add("Autumn weather");
        data.add("Tropical weather");
        return data;
    }

    public void inspectGreenhouse(Greenhouse greenhouse) {
        StringBuilder data = new StringBuilder();
        if (greenhouse.isRuin())
            data.append("1. Repair Greenhouse\n");
        else {
            data.append("1. Inspect Weather Machine\n");
            data.append("2. Inspect Field\n");
            data.append("3. Extend Greenhouse\n");
        }
        System.out.println(data);
    }

    public void checkShop(Shop shop) {
        StringBuilder data = new StringBuilder(shop.getName() + " Menu:\n");
        data.append(String.format("%-20s%-20s%-20s\n", "Item ", "Available count", "price"));
        int count = 0;
        for (Object object : shop.getGoods().elementSet()) {
            count++;
            data.append(String.format("%-2s", count));
            data.append(". ");
            if (object instanceof Item) {
                Item item = (Item) object;
                data.append(String.format("%-20s", item.getName()));
                data.append(String.format("x%-15s", shop.getGoods().count(item)));
                data.append(String.format("%-20s", item.getPrice()));
            } else if (object instanceof Animal) {
                Animal animal = (Animal) object;
                data.append(String.format("%-20s", animal.getName()));
                data.append(String.format("x%-15s", shop.getGoods().count(animal)));
                data.append(String.format("%-20s", animal.getPrice()));
            }
            data.append("\n");
        }
        System.out.println(data);
    }

    public void butcherySellMenu() {
        StringBuilder data = new StringBuilder("Choose the animal type you want to sell:\n");
        data.append("1. Chicken\n");
        data.append("2. Cow\n");
        data.append("3. Sheep");
        System.out.println(data);
    }

    public void inspectMarket(Market market) {
        StringBuilder data = new StringBuilder();
        for (int i = 0; i < market.getShop().size(); i++) {
            data.append((i + 1));
            data.append(". ");
            data.append(market.getShop().get(i).getName());
            data.append("\n");
        }
        System.out.println(data);
    }

    public ArrayList<String> checkWorkshop(Workshop workshop) {
        ArrayList<String> data = new ArrayList<>();
        /*for (int i = 0; i < workshop.getTool().size(); i++) {
            data.add("" + (i + 1) + ". " + workshop.getTool().get(i).getName() + "\n");
        }*/
        return data;
    }

    public ArrayList<String> initialIngredient(Tool tool) {
        ArrayList<String> data = new ArrayList<>();
        //new Status().tool(tool);
        for (Item item : tool.getDissassemblity().getInitialIngredients()) {
            data.add(item.getName() + " x" + tool.getDissassemblity().getInitialIngredients().count(item));
        }
       return data;
    }

    public void buildTool(Tool tool) {
        initialIngredient(tool);
        StringBuilder data = new StringBuilder();
        data.append("Building price : ");
        data.append(tool.getBuildingPrice());
        System.out.println(data);
    }

    public void disassembleTool(Tool tool) {
        initialIngredient(tool);
        StringBuilder data = new StringBuilder();
        data.append("Disassemble price : ");
        data.append(tool.getDissassemblity().getPrice());
        System.out.println(data);
    }

    public void printFeatures(Player player) {
        StringBuilder data = new StringBuilder();
        int count = 1;
        for (Feature feature : player.getFeature()) {
            data.append(count + ". ");
            data.append(feature.getName() + "\n");
            count++;
        }
        System.out.println(data);
    }

    public void inspectFeature(Feature feature) {
        StringBuilder data = new StringBuilder("Which ability do you want to reinforce?\n");
        data.append("1. Consumption rate\n");
        data.append("2. Refill rate\n");
        data.append("3. Max amount\n");
        System.out.println(data);
    }

    public void featureFieldMenu(Feature feature) {
        StringBuilder data = new StringBuilder();
        data.append("1. Status\n");
        data.append("2. Train\n");
        System.out.println(data);
    }

    public ArrayList<String> checkAvailableMission(Board board) {
        ArrayList<String> data = new ArrayList<>();
        try {
            for (int i = 0; i < board.getAvailableMission().size(); i++) {
                data.add(board.getAvailableMission().get(i).getName());
            }
        }
        catch (Exception e){
           // System.out.println("Mission board is empty");
        }
        return data;
    }

    public ArrayList<String> inspectAvailableMission() {
        ArrayList<String> data = new ArrayList<>();
        data.add("1. Mission briefing\n");
        data.add("2. Accept this mission");
       return data;
    }

    public ArrayList<String> missionBriefing(Mission mission) {
        ArrayList<String> data = new ArrayList<>();
        int count = 1;
        data.add("**Tasks needed to be done :");
        for(Tetrad tetrad: mission.getTetrad()){
            data.add(count + ". " + tetrad.getTask() + " " + tetrad.getObejct() + " " + tetrad.getCount() + " times!");
            count ++;
       }
       data.add("**Prize :");
       count = 1;
        for(Object object: mission.getPrice().elementSet()){
            if(object instanceof Item){
                data.add(count + ". " + mission.getPrice().count(object) + " " + ((Item) object).getName());
                count ++;
            }else if (object instanceof String){
                data.add(object.toString());
            }
        }
        data.add("**Contract fee : " + mission.getContractFee());
        data.add("**Due time : " + mission.getDueTime().getHour() + " hours");
        System.out.println(data);
        System.out.println(mission);
        return data;
    }

    public ArrayList<String> checkActiveMissions(Board board) {
       ArrayList<String> data = new ArrayList<>();
        for (int i = 0; i < board.getActiveMission().size(); i++) {
            data.add(board.getActiveMission().get(i).getName());
        }
        return data;
    }

    public ArrayList<String> inspectActiveMissions(Mission mission) {
        ArrayList<String> data = new ArrayList<>();
        data.add("Mission briefing");
        data.add("Mission progress");
        data.add("Remove this mission");
        if (mission.getProgress() >= 99.5)
            data.add("Claim prize");
        return data;
    }

    public ArrayList<String> checkMissionProgress(Mission mission) {
        ArrayList<String> data = new ArrayList<>();
        int count = 1;
        data.add("**Progress of Tasks :");
        for(Tetrad tetrad: mission.getTetrad()){
            data.add(count + ". " + tetrad.getTask() + " " + tetrad.getObejct() + " (" + tetrad.getHappened()
                    + " / " + tetrad.getCount() + ")");
        }
        return data;
    }

    public void inspectDiningTable() {
        StringBuilder data = new StringBuilder("Dining table:\n");
        data.append("1. Check the menu\n");
        data.append("2. Buy a meal\n");
        System.out.println(data);
    }

    public void ranchAnimalMenu(Ranch ranch) {
        StringBuilder data = new StringBuilder("Ranch animal Menu:\n");
        data.append(String.format("%-20s%-20s%-20s\n", "Animal ", "Available count", "price"));
        int count = 0;
        for (Animal animal : ranch.getAnimals().elementSet()) {
            count++;
            data.append(String.format("%-2s", count));
            data.append(". ");
            data.append(String.format("%-20s", animal.getType()));
            data.append(String.format("x%-15s", ranch.getAnimals().count(animal)));
            data.append(String.format("%-20s", animal.getPrice()));
            data.append("\n");
        }
        System.out.println(data);
    }

    public void checkMachine(Laboratory laboratory) {
        StringBuilder data = new StringBuilder("Machines:\n");
        for (int i = 0; i < laboratory.getMachine().size(); i++) {
            data.append(i + 1);
            data.append(". ");
            data.append(laboratory.getMachine().get(i).getName());
            data.append("\n");
        }
        System.out.println(data);
    }

    public void inspectBarn(Barn barn) {
        StringBuilder data = new StringBuilder("Barn: \n");
        for(int i = 0; i < barn.getBarnPart().size(); i++){
            data.append((i+1) + ". ");
            data.append("Inspect ");
            data.append(barn.getBarnPart().get(i).getAnimalType());
            data.append("s\n");
        }
        data.append("4. Inspect Machines\n");
        data.append("5. Extend Barn\n");
        System.out.println(data);
    }

    public ArrayList inspectCrop() throws IllegalAccessException, InstantiationException {
        StringBuilder data = new StringBuilder("Field crop type: \n");
        ArrayList<Crop> crops = new ArrayList<>();
        int count = 1;
        Class cropsClass = AllOfCrops.class;
        Object obj = cropsClass.newInstance();
        Method[] methods = cropsClass.getDeclaredMethods();
        for(Method method : methods) {
            try {
                Crop crop = (Crop) method.invoke(obj);
                data.append(count + ". " + crop.getType() + " Field\n");
                crops.add(crop);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
            count ++;
        }
        System.out.println(data);
        return crops;
    }

    public void checkMachine(Barn barn) {
        StringBuilder data = new StringBuilder("Machines:\n");
        for (int i = 0; i < barn.getMachines().size(); i++) {
            data.append(i + 1);
            data.append(". ");
            data.append(barn.getMachines().get(i).getName());
            data.append("\n");
        }
        System.out.println(data);
    }

    public void checkAnimal(BarnPart barnPart) {
        StringBuilder data = new StringBuilder(barnPart.getAnimalType() + "s:\n");
        for (int i = 0; i < barnPart.getAnimal().size(); i++) {
            data.append(i + 1);
            data.append(". ");
            data.append(barnPart.getAnimal().get(i).getType() + " No" + (i+1));
            data.append("\n");
        }
        System.out.println(data);
    }
}
