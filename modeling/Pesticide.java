import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Pesticide extends Medicine {
    public Pesticide(HashMap<String, Double> featureChangeRate, double capacity, String name, Probability invalid,
                     double price, ArrayList<Task> task, int maxLevel, int level, Image picture) {
        super("Pesticide", featureChangeRate, capacity, name, invalid, price, task, maxLevel, level, false, picture);
    }
}
