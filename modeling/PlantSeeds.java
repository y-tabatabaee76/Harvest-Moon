import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;

public class PlantSeeds extends Task {

    public PlantSeeds() {
        super.name = "Plant Seeds";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }


    public String run(Seed seed, Cell[][] cell, Player player, Weather weather, int x, int y, Rectangle[][] cellG) {
        Image image = new Image("pics/bud.png");
        for (int i = 0; i < cell.length; i++) {
            for (int j = 0; j < cell[i].length; j++)
                if (cell[i][j] != null)
                    if (!cell[i][j].isPlowed()) {
                        String[] string = {" Field is not plowed!\n", "Le champ n'est pas labouré!", "¡El campo no está arado!"};
                        return string[UI.language];
                    }
        }
        if (!seed.getSource().getWeather().getType().equals(weather.getType())) {
            String[] string = {"You can't plant this seed in this season", "Vous ne pouvez pas planter cette graine en cette saison"
                    , "No se puede plantar esta semilla en esta temporada"};
            return string[UI.language];
        } else {
            for (int i = 0; i < cell.length; i++)
                for (int j = 0; j < cell[0].length; j++)
                    if (cell[i][j] != null) {
                        cell[i][j].setContent(seed.getSource());
                        cellG[i + x][j + y].setFill(new ImagePattern(image));
                        UI.missionHandler(name, seed.getName());
                    }
            player.getBackpack().getItem().remove(seed);
            String[] string = {"Plants successfully", "Plantes réussies", "Plantas con éxito"};
            return string[UI.language];
        }
    }
}
