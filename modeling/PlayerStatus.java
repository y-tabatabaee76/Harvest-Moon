import java.io.Serializable;

public class PlayerStatus implements Serializable {
    private Double healthCurrent;
    private Double healthMaxCurrent;
    private Double healthRefill;
    private Double healthMaxRefill;
    private Double healthConsumption;
    private Double healthMinConsumption;
    private Double energyCurrent;
    private Double energyMaxCurrent;
    private Double energyRefill;
    private Double energyMaxRefill;
    private Double energyConsumption;
    private Double energyMinConsumption;
    private Double staminaCurrent;
    private Double staminaMaxCurrent;
    private Double staminaRefill;
    private Double staminaMaxRefill;
    private Double staminaConsumption;
    private Double staminaMinConsumption;
    private Double satietyCurrent;
    private Double satietyMaxCurrent;
    private Double satietyRefill;
    private Double satietyMaxRefill;
    private Double satietyConsumption;
    private Double satietyMinConsumption;
    private Double money;
    private Integer animalCount;
    private String name;
    private Integer numOfReceiver = -1;

    public PlayerStatus(Double healthCurrent, Double healthMaxCurrent, Double healthRefill, Double healthMaxRefill,
                        Double healthConsumption, Double healthMinConsumption, Double energyCurrent, Double energyMaxCurrent,
                        Double energyRefill, Double energyMaxRefill, Double energyConsumption, Double energyMinConsumption,
                        Double staminaCurrent, Double staminaMaxCurrent, Double staminaRefill, Double staminaMaxRefill,
                        Double staminaConsumption, Double staminaMinConsumption, Double satietyCurrent, Double satietyMaxCurrent,
                        Double satietyRefill, Double satietyMaxRefill, Double satietyConsumption, Double satietyMinConsumption,
                        Double money, Integer animalCount, String name) {
        this.healthCurrent = healthCurrent;
        this.healthMaxCurrent = healthMaxCurrent;
        this.healthRefill = healthRefill;
        this.healthMaxRefill = healthMaxRefill;
        this.healthConsumption = healthConsumption;
        this.healthMinConsumption = healthMinConsumption;
        this.energyCurrent = energyCurrent;
        this.energyMaxCurrent = energyMaxCurrent;
        this.energyRefill = energyRefill;
        this.energyMaxRefill = energyMaxRefill;
        this.energyConsumption = energyConsumption;
        this.energyMinConsumption = energyMinConsumption;
        this.staminaCurrent = staminaCurrent;
        this.staminaMaxCurrent = staminaMaxCurrent;
        this.staminaRefill = staminaRefill;
        this.staminaMaxRefill = staminaMaxRefill;
        this.staminaConsumption = staminaConsumption;
        this.staminaMinConsumption = staminaMinConsumption;
        this.satietyCurrent = satietyCurrent;
        this.satietyMaxCurrent = satietyMaxCurrent;
        this.satietyRefill = satietyRefill;
        this.satietyMaxRefill = satietyMaxRefill;
        this.satietyConsumption = satietyConsumption;
        this.satietyMinConsumption = satietyMinConsumption;
        this.money = money;
        this.animalCount = animalCount;
        this.name = name;
    }

    public Double getHealthCurrent() {
        return healthCurrent;
    }

    public Double getHealthMaxCurrent() {
        return healthMaxCurrent;
    }

    public Double getHealthRefill() {
        return healthRefill;
    }

    public Double getHealthMaxRefill() {
        return healthMaxRefill;
    }

    public Double getHealthConsumption() {
        return healthConsumption;
    }

    public Double getHealthMinConsumption() {
        return healthMinConsumption;
    }

    public Double getEnergyCurrent() {
        return energyCurrent;
    }

    public Double getEnergyMaxCurrent() {
        return energyMaxCurrent;
    }

    public Double getEnergyRefill() {
        return energyRefill;
    }

    public Double getEnergyMaxRefill() {
        return energyMaxRefill;
    }

    public Double getEnergyConsumption() {
        return energyConsumption;
    }

    public Double getEnergyMinConsumption() {
        return energyMinConsumption;
    }

    public Double getStaminaCurrent() {
        return staminaCurrent;
    }

    public Double getStaminaMaxCurrent() {
        return staminaMaxCurrent;
    }

    public Double getStaminaRefill() {
        return staminaRefill;
    }

    public Double getStaminaMaxRefill() {
        return staminaMaxRefill;
    }

    public Double getStaminaConsumption() {
        return staminaConsumption;
    }

    public Double getStaminaMinConsumption() {
        return staminaMinConsumption;
    }

    public Double getSatietyCurrent() {
        return satietyCurrent;
    }

    public Double getSatietyMaxCurrent() {
        return satietyMaxCurrent;
    }

    public Double getSatietyRefill() {
        return satietyRefill;
    }

    public Double getSatietyMaxRefill() {
        return satietyMaxRefill;
    }

    public Double getSatietyConsumption() {
        return satietyConsumption;
    }

    public Double getSatietyMinConsumption() {
        return satietyMinConsumption;
    }

    public Double getMoney() {
        return money;
    }

    public Integer getAnimalCount() {
        return animalCount;
    }

    public Integer getNumOfReceiver() {
        return numOfReceiver;
    }

    public String getName() {
        return name;
    }

    public void setNumOfReceiver(Integer numOfReceiver) {
        this.numOfReceiver = numOfReceiver;
    }
}
