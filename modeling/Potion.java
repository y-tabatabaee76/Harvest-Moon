import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Potion extends Medicine {

    public Potion(HashMap<String, Double> featureChangeRate, double capacity, String name,
                  Probability invalid, double price, ArrayList<Task> task, int level, Image picture) {
        super("Potion", featureChangeRate, capacity, name, invalid, price, task, 3, level, true, picture);
    }
}
