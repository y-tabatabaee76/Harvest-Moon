import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Product extends Item {

    private ArrayList<Tool> tools;
    private Object source;
    private int expirationTime;

    public int getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(int expirationTime) {
        this.expirationTime = expirationTime;
    }

    public ArrayList<Tool> getTools() {
        return tools;
    }

    public void setTools(ArrayList<Tool> tools) {
        this.tools = tools;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Product(String type, HashMap<String, Double> featureChangeRate, double capacity, String name,
                   Probability invalid, double price, ArrayList<Tool> tools, ArrayList<Task> task, boolean eatable,
                   boolean accumulation, Dissassemblity dissassemblity, Object source, Image picture) {
        super(type, featureChangeRate, capacity, name, invalid, price, task, 1, 1, eatable,
                accumulation, dissassemblity, picture);
        this.tools = tools;
        this.source = source;
        expirationTime = (int)(Math.random() * 10) + 3;
    }

    public Product(){}
}