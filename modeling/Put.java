import com.google.common.collect.HashMultiset;

import java.util.HashMap;

public class Put extends Task{

    public Put() {
        super.name = "Put";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(StorageBox storageBox, Item item, Player player) {
       storageBox.getItem().add(item);
       player.getBackpack().getItem().remove(item);
        UI.missionHandler("Take", "Backpack");
        UI.missionHandler(name, "Storage Box");
        String[] string = {" was put into the storageBox!", "A été placé dans le storageBox!",
                "Fue puesto en el storageBox!"};
        return item.getName() + string[UI.language];
    }

    public String run(ToolShelf toolShelf, Tool tool, Player player) {
        toolShelf.getExistingTools().add(tool);
        player.getBackpack().getItem().remove(tool);
        UI.missionHandler("Take", "Backpack");
        UI.missionHandler(name, "Tool Shelf");
        String[] string = {" was put into the ToolShelf!", "A été placé dans ToolShelf!",
                "Fue puesto en el ToolShelf!"};
        return tool.getName() + string[UI.language];
    }

    public boolean run(HashMultiset<Item> items, Backpack backpack) { // FIXME: 5/20/2017 - age zarfiatesh bad az ezafe shodane un item ha ham az max bishtar she bayad Backpack is fuul bege!
        for(Item item: items.elementSet()){
            // FIXME: 5/19/2017 accumulation checking
            if(backpack.getCapacity() < backpack.getCurrentFullness() + item.getCapacity()) {
                System.out.println("Backpack is full");
                return false;
            }
            backpack.getItem().add(item, items.count(item));
            backpack.setCurrentFullness(backpack.getCurrentFullness() + item.getCapacity());
        }
        return true;
    }

    public boolean run(Item item, Backpack backpack, int num) {
        if (backpack.getCapacity() < backpack.getCurrentFullness() + item.getCapacity() * num) {
            return false;
        }
        backpack.getItem().add(item, num);
        backpack.setCurrentFullness(backpack.getCurrentFullness() + item.getCapacity() * num);
        return true;
    }

    public boolean run(Item item, StorageBox storageBox, Backpack backpack) {
        storageBox.getItem().add(item);
        backpack.getItem().remove(item);
        backpack.setCurrentFullness(backpack.getCurrentFullness() - item.getCapacity());
        return true;
    }
}
