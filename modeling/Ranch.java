import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class Ranch extends Shop {

    public HashMultiset<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(HashMultiset<Animal> animals) {
        this.animals = animals;
    }

    private HashMultiset<Animal> animals;
    private ArrayList<Animal> cow;
    private ArrayList<Animal> sheep;
    private ArrayList<Animal> chicken;

    public Ranch(HashMultiset goods, HashMultiset<Animal> animals) {
        super(goods, "Ranch");
        this.animals = animals;
        sheep = new ArrayList<>();
        cow = new ArrayList<>();
        chicken = new ArrayList<>();
        for(Animal animal: animals.elementSet()){
            if(animal.getType().equals("Cow"))
                for (int i = 0; i < animals.count(animal); i++) {
                    cow.add(animal);
                }
            if(animal.getType().equals("Sheep"))
                for (int i = 0; i < animals.count(animal); i++) {
                    sheep.add(animal);
                }
            if(animal.getType().equals("Chicken"))
                for (int i = 0; i < animals.count(animal); i++) {
                    chicken.add(animal);
                }
        }
    }

    public Ranch(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, HashMultiset goods) {
        super(isRuin, ruin, repairObjects, repairPrice, goods, "Ranch");
    }

    public ArrayList<Animal> getCow() {
        return cow;
    }

    public void setCow(ArrayList<Animal> cow) {
        this.cow = cow;
    }

    public ArrayList<Animal> getSheep() {
        return sheep;
    }

    public void setSheep(ArrayList<Animal> sheep) {
        this.sheep = sheep;
    }

    public ArrayList<Animal> getChicken() {
        return chicken;
    }

    public void setChicken(ArrayList<Animal> chicken) {
        this.chicken = chicken;
    }


}