import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class RanchUI implements Mover {
    private static Group root = new Group();
    static double tileSize = 50;
    private static Rectangle chair;
    private static Rectangle table;
    private Rectangle[][] cowG;
    private Rectangle[][] chickenG;
    private Rectangle[][] sheepG;
    private static Ranch ranch;


    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public RanchUI(Group root, Ranch ranch) {
        this.root = root;
        this.ranch = ranch;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Scene ranchScene = new Scene(root, 1000, 700, Color.BLACK);

        Image ranchPic = new Image("pics/maps/ranchMap.png");
        Image tablePic = new Image("pics/woodenTable.png");
        Image chairPic = new Image("pics/woodenForwardChair.png");
        Image verticalWallPic = new Image("pics/woodenVerticalWall.png");
        Image hayPic = new Image("pics/hay.png");
        Image chickenHayPic = new Image("pics/chickenHay.png");
        Image doorPic = new Image("pics/doors/ranchDoor.png");
        Image alfalfaPic = new Image("pics/alfalfa.png");
        Image seedsPic = new Image("pics/seeds.png");
        Image medicinePic1 = new Image("pics/animalMedicine1.png");
        Image medicinePic2 = new Image("pics/animalMedicine2.png");
        Image waterPic = new Image("pics/water.png");

        cowG = new Rectangle[3][1];
        sheepG = new Rectangle[3][1];
        chickenG = new Rectangle[4][2];

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(ranchPic));
        root.getChildren().add(rectangle);

        table = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(tablePic), tileSize * 12, tileSize * 5);
        chair = rectangleBuilder(tileSize, tileSize, new ImagePattern(chairPic), tileSize * 13, tileSize * 4);
        Rectangle door = rectangleBuilder(tileSize, tileSize * 3, new ImagePattern(doorPic), 1000 - tileSize * 3, 0);
        Rectangle alfalfa = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(alfalfaPic), 1000 - tileSize * 6, tileSize * 10);
        Rectangle seed = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(seedsPic), 1000 - tileSize * 4, tileSize * 10);
        Rectangle medicine1 = rectangleBuilder(tileSize, tileSize, new ImagePattern(medicinePic1), 1000 - tileSize * 6, tileSize * 12);
        Rectangle medicine2 = rectangleBuilder(tileSize, tileSize, new ImagePattern(medicinePic2), 1000 - tileSize * 5, tileSize * 12);
        Rectangle waterCan1 = rectangleBuilder(tileSize, tileSize, new ImagePattern(waterPic), 1000 - tileSize * 4, tileSize * 12);
        Rectangle waterCan2 = rectangleBuilder(tileSize, tileSize, new ImagePattern(waterPic), 1000 - tileSize * 3, tileSize * 12);
        root.getChildren().addAll(table, chair, door, alfalfa, seed, medicine1, medicine2, waterCan1, waterCan2);

        for (int i = 0; i < 4; i++) {
            Rectangle verticalWall1 = rectangleBuilder(tileSize, tileSize, new ImagePattern(verticalWallPic), tileSize * 10, tileSize * (3 + i));
            Rectangle verticalWal2 = rectangleBuilder(tileSize, tileSize, new ImagePattern(verticalWallPic), tileSize * 10, tileSize * (13 - i));
            root.getChildren().addAll(verticalWall1, verticalWal2);
        }

        for (int i = 0; i < 3; i++) {
            cowG[i][0] = new Rectangle();
            sheepG[i][0] = new Rectangle();
            Rectangle hay1 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(hayPic), tileSize * (3 * i + 1), tileSize * 3);
            Rectangle hay2 = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(hayPic), tileSize * (3 * i + 1), tileSize * 6);
            root.getChildren().addAll(hay1, hay2);
            try {
                if (i < ranch.getCow().size()) {
                    cowG[i][0] = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(ranch.getCow().get(i).getPicture()),
                            tileSize * (3 * i + 1), tileSize * 3);
                    root.getChildren().addAll(cowG[i][0]);
                }
                if (i < ranch.getSheep().size()) {
                    sheepG[i][0] = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(ranch.getSheep().get(i).getPicture()),
                            tileSize * (3 * i + 1), tileSize * 6);
                    root.getChildren().addAll(sheepG[i][0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < 4; i++) {
            chickenG[i][0] = new Rectangle();
            chickenG[i][1] = new Rectangle();
            Rectangle hay1 = rectangleBuilder(tileSize * 2, tileSize, new ImagePattern(chickenHayPic), tileSize * 2 * i, tileSize * 10);
            Rectangle hay2 = rectangleBuilder(tileSize * 2, tileSize, new ImagePattern(chickenHayPic), tileSize * 2 * i, tileSize * 12);
            root.getChildren().addAll(hay1, hay2);
            if (i < ranch.getChicken().size()) {
                chickenG[i][0] = rectangleBuilder(tileSize * 1, tileSize * 1, new ImagePattern(ranch.getChicken().get(i).getPicture()),
                        tileSize * (2 * i + 1), tileSize * 10 - 10);
                root.getChildren().addAll(chickenG[i][0]);
            }
        }

        return ranchScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.4 &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() + table.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.6 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() + chair.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.4 &&
                        player.getX() <= tileSize * 9.8 &&
                        player.getY() + player.getHeight() >= tileSize * 7.2 &&
                        player.getY() + player.getHeight() <= tileSize * 7.4)) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 13 &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= table.getY()) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.6 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= chair.getY()) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.4 &&
                        player.getX() <= tileSize * 9.8 &&
                        player.getY() + player.getHeight() >= tileSize * 9.6 &&
                        player.getY() + player.getHeight() <= tileSize * 9.8) &&
                !(player.getX() + player.getWidth() >= tileSize * 14.4 &&
                        player.getX() <= tileSize * 17.6 &&
                        player.getY() + player.getHeight() >= tileSize * 9.6 &&
                        player.getY() + player.getHeight() <= tileSize * 9.8)) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= table.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= table.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= table.getY() &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= chair.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= chair.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.2 &&
                        player.getX() + player.getWidth() <= tileSize * 10.4 &&
                        player.getY() + player.getHeight() >= tileSize * 0 &&
                        player.getY() + player.getHeight() <= tileSize * 7.2) &&
                !(player.getX() + player.getWidth() >= tileSize * 10.2 &&
                        player.getX() + player.getWidth() <= tileSize * 10.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.8 &&
                        player.getY() + player.getHeight() <= tileSize * 14) &&
                !(player.getX() + player.getWidth() >= tileSize * 14.2 &&
                        player.getX() + player.getWidth() <= tileSize * 14.4 &&
                        player.getY() + player.getHeight() >= tileSize * 9.8 &&
                        player.getY() + player.getHeight() <= tileSize * 14)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= tileSize * 0 &&
                !(player.getX() >= table.getX() + table.getWidth() - tileSize * 0.4 &&
                        player.getX() <= table.getX() + table.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= table.getY() &&
                        player.getY() + player.getHeight() <= table.getY() + table.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= chair.getX() + chair.getWidth() - tileSize * 0.6 &&
                        player.getX() <= chair.getX() + chair.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= chair.getY() &&
                        player.getY() + player.getHeight() <= chair.getY() + chair.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= tileSize * 9.8 &&
                        player.getX() <= tileSize * 10 &&
                        player.getY() + player.getHeight() >= tileSize * 0 &&
                        player.getY() + player.getHeight() <= tileSize * 7.2) &&
                !(player.getX() >= tileSize * 9.8 &&
                        player.getX() <= tileSize * 10 &&
                        player.getY() + player.getHeight() >= tileSize * 9.8 &&
                        player.getY() + player.getHeight() <= tileSize * 14) &&
                !(player.getX() >= tileSize * 17.6 &&
                        player.getX() <= tileSize * 17.8 &&
                        player.getY() + player.getHeight() >= tileSize * 9.8 &&
                        player.getY() + player.getHeight() <= tileSize * 14)) {
            return true;
        }
        return false;
    }

    public boolean rightPlaceToBuyItem(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 >= tileSize * 10 &&
                player.getY() + player.getHeight() >= tileSize * 7) {
            return true;
        }
        return false;
    }

    public int chosenAnimal(Rectangle player) {
        for (int i = 0; i < cowG.length; i++) {
            if (player.getX() + player.getWidth() / 2 <= cowG[i][0].getX() + cowG[i][0].getWidth() + 30 &&
                    player.getX() + player.getWidth() / 2 >= cowG[i][0].getX() - 30 &&
                    player.getY() + player.getHeight() <= cowG[i][0].getY() + cowG[i][0].getHeight() + 30 &&
                    player.getY() + player.getHeight() >= cowG[i][0].getY() - 30) {
                return i;
            }
        }
        for (int i = 0; i < sheepG.length; i++) {
            if (player.getX() + player.getWidth() / 2 <= sheepG[i][0].getX() + sheepG[i][0].getWidth() + 30 &&
                    player.getX() + player.getWidth() / 2 >= sheepG[i][0].getX() - 30 &&
                    player.getY() + player.getHeight() <= sheepG[i][0].getY() + sheepG[i][0].getHeight() + 30 &&
                    player.getY() + player.getHeight() >= sheepG[i][0].getY() - 30) {
                return i + 3;
            }
        }
        for (int i = 0; i < chickenG.length; i++) {
            for (int j = 0; j < chickenG[0].length; j++) {
                if (player.getX() + player.getWidth() / 2 <= chickenG[i][j].getX() + chickenG[i][j].getWidth() + 30 &&
                        player.getX() + player.getWidth() / 2 >= chickenG[i][j].getX() - 30 &&
                        player.getY() + player.getHeight() <= chickenG[i][j].getY() + chickenG[i][j].getHeight() + 30 &&
                        player.getY() + player.getHeight() >= chickenG[i][j].getY() - 30) {
                    return 2 * j + i + 6;
                }
            }
        }
        return -1;
    }

    public static Animal chosedAnimal(int animalNum, boolean[] flags) {
        if (animalNum < 3) {
            if (animalNum <= ranch.getCow().size()) {
                flags[1] = true;
                return ranch.getCow().get(animalNum);
            }
        } else if (animalNum < 6) {
            if (animalNum - 3 <= ranch.getSheep().size()) {
                flags[1] = true;
                return ranch.getSheep().get(animalNum - 3);
            }
        } else if (animalNum < 14) {
            if (animalNum - 6 <= ranch.getChicken().size()) {
                flags[1] = true;
                return ranch.getChicken().get(animalNum - 6);
            }
        } else {
            UI.showPopup("There is no animal to buy in this hay!", root);
            UI.initializeFlags();
        }
        return null;
    }

    public void buy(Animal animal, Player player, boolean flags[], Barn barn, int animalNum, BarnUI barnUI) {
        Buy buy = new Buy();
        if (animalNum < 3) {
            if (animalNum <= ranch.getCow().size()) {
                UI.showPopup(buy.run(ranch, animal, player, barn, 1, cowG, animalNum, 0, root, barnUI), root);
            }
        } else if (animalNum < 6) {
            if (animalNum - 3 <= ranch.getSheep().size()) {
                UI.showPopup(buy.run(ranch, animal, player, barn, 1, sheepG, animalNum - 3, 0, root, barnUI), root);
            }
        } else if (animalNum < 14) {
            if (animalNum - 6 <= ranch.getChicken().size()) {
                System.out.println((animalNum - 6) % 4);
                System.out.println((animalNum - 6) / 4);
                UI.showPopup(buy.run(ranch, animal, player, barn, 1, chickenG, (animalNum - 6) % 4, (animalNum - 6) / 4, root, barnUI), root);
            }
        } else {
            UI.showPopup("There is no animal to buy in this hay!", root);
            UI.initializeFlags();
        }
        flags[3] = true;
    }
}
