public class Range {

    private int length;
    private int width;

    public int getLenght() {
        return length;
    }

    public void setLenght(int lenght) {
        this.length = lenght;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Range(int length, int width) {
        this.length = length;
        this.width = width;
    }
}
