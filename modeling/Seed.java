import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Seed extends Item{
    private Crop source;

    public Crop getSource() {
        return source;
    }

    public void setSource(Crop source) {
        this.source = source;
    }

    public Seed(String name, double price, ArrayList<Task> task, Crop source, Image picture) {
        super("Seed", new HashMap<>(), 1, name, new Probability(){{}}, price, task, 1, 1, false,
                true, new Dissassemblity(0, HashMultiset.<Item>create(), false), picture);
        this.source = source;
    }
}
