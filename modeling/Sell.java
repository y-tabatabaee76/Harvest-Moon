import java.util.HashMap;
import java.util.Map;

public class Sell extends Task{

    public Sell() {
        super.name = "Sell";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String run(Shop shop, Item item, Player player, int num) {
        player.getBackpack().getItem().remove(item, num);
        shop.getGoods().add(item, num);
        player.setMoney(player.getMoney() + item.getPrice() * num * 1.25);
        UI.missionHandler(name, item.getName());
        String[] string = {" was sold to ", "A été vendu à", "Fue vendido a"};
        return ("Item " + item.getName() + string[UI.language] + shop.getName());
    }
    public boolean run(Shop shop, Animal animal, Player player, BarnPart barnPart, int num) {
        if(barnPart.getAnimalType().equals(animal.getType())){
            if(barnPart.getAnimal().size() == 0) {
                System.out.println("There is no animal of this type in the barn");
                return false;
            }
            else {
                for(int  i = 0; i < num; i++) {
                    barnPart.getAnimal().remove(animal);
                }
                player.setMoney(player.getMoney() + animal.getPrice() * num);
                // TODO: 5/21/2017 add this to the shop
                return true;
            }
        }
        return false;
    }
}
