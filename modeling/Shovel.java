import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class Shovel extends Tool {

    private Range range;

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public Shovel(String type, HashMap<String, Double> featureChangeRate, double capacity, String name, double price, ArrayList<Task> task,
                  int level, Dissassemblity dissassemblity, double buildingPrice, double repairPrice, HashMultiset<Item> repairItems, Range range, Image picture) {
        super(type, featureChangeRate, capacity, name, new Probability(){{setProbability((double)(5 - level) / 20);}}, price, task, 4, level, dissassemblity,
                buildingPrice, repairPrice, repairItems, picture);
        this.range = range;
    }

    public Shovel(){super();}
}
