import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Status extends Task {

    private StringBuilder data;

    public StringBuilder getData() {
        return data;
    }

    public void setData(StringBuilder data) {
        this.data = data;
    }


    public Status() {
        super.name = "Status";
        super.featureChangeRate = new HashMap();
        super.executionTime = new Date();
    }

    /*public String field(FarmingField farmingField) {
        data = new StringBuilder("Status:\n");
        try {
            String str;
            if (farmingField.getCrop().getLifeCycle() - farmingField.getCrop().getAge() == 0)
                str = "Fully grown!";
            else str = Integer.toString(farmingField.getCrop().getLifeCycle() - farmingField.getCrop().getAge());
            data.append("Crop type: ");
            data.append(farmingField.getCrop().getType());
            data.append("\nCrop season: ");
            data.append(farmingField.getCrop().getWeather().getType());
            data.append("\nDays until full growth: ");
            data.append(str);
            data.append("\nWatered today: ");
            data.append(farmingField.getCell()[0][0].isWatered());
            data.append("\nDays until spoilage: ");
            data.append((farmingField.getCrop().getLifeCycle() + farmingField.getCrop().getSpoilageTime()
                    - farmingField.getCrop().getAge()));
            data.append("\nCrop harvests left: ");
            data.append(farmingField.getCrop().getHarvestLeft());
        } catch (Exception e) {
            data.append("Plowed: " + farmingField.getCell()[0][0].isPlowed() + "\n");
            data.append("watered: " + farmingField.getCell()[0][0].isWatered() + "\n");
        }
        return data.toString();
    }*/

    public String cookingUtensil(CookingUtensil cookingUtensil) {
        data = new StringBuilder("");
        String str = cookingUtensil.isValid() ? "Not broken\n" : "Broken\n";
        data.append("A cooking utensil.\n");
        data.append(str);
        return data.toString();
    }

    public String axe(Axe axe) {
        data = new StringBuilder("");
        String str = axe.isValid() ? "Not broken\n" : "Broken\n";
        data.append("A ");
        data.append(axe.getName());
        data.append(".\nEnergy required for each use: ");
        data.append((Double) axe.getFeatureChangeRate().get("Energy"));
        data.append("\n");
        data.append(str);
        return data.toString();
    }

    public String pickaxe(Pickaxe pickaxe) {
        data = new StringBuilder("");
        String str = pickaxe.isValid() ? "Not broken\n" : "Broken\n";
        data.append("A ");
        data.append(pickaxe.getName());
        data.append(".\nEnergy required for each use: ");
        data.append((Double) pickaxe.getFeatureChangeRate().get("Energy"));
        data.append("\n");
        data.append(str);
        return data.toString();
    }

    public String shovel(Shovel shovel) {
        data = new StringBuilder("");
        String str = shovel.isValid() ? "Not broken\n" : "Broken\n";
        data.append("A ");
        data.append(shovel.getName());
        data.append(".\nWith this shovel you can dig a\n");
        data.append(shovel.getRange().getLenght());
        data.append(" * ");
        data.append(shovel.getRange().getWidth());
        data.append(" range in one go.\n");
        data.append("Energy required for each use: ");
        data.append((Double) shovel.getFeatureChangeRate().get("Energy"));
        data.append("\n");
        data.append(str);
        return data.toString();
    }

    public String wateringCan(WateringCan wateringCan) {
        data = new StringBuilder("");
        String str = wateringCan.isValid() ? "Not broken\n" : "Broken\n";
        data.append("A ");
        data.append(wateringCan.getName() + ".\n");
        data.append("With this sprinkler you can water a\n");
        data.append(wateringCan.getRange().getLenght());
        data.append(" * ");
        data.append(wateringCan.getRange().getWidth());
        data.append(" range in one go.\n");
        data.append("It has the capacity to water \n");
        data.append(wateringCan.getFieldNum());
        data.append(" fields of the farm.\n");
        data.append("*Water level: ");
        data.append(wateringCan.getWaterLevel());
        data.append(" of ");
        data.append(wateringCan.getMaxWaterLevel());
        data.append("\n*Energy required for each use: ");
        data.append((Double) wateringCan.getFeatureChangeRate().get("Energy"));
        data.append("\n");
        data.append(str);
        data.append("\n");
        return data.toString();
    }

    public String fishingRod(FishingRod fishingRod) {
        data = new StringBuilder("");
        String str = fishingRod.isValid() ? "Not broken\n" : "Broken\n";
        data.append("A ");
        data.append(fishingRod.getName());
        data.append(".\nThere is ");
        data.append(Double.toString(fishingRod.getFishingProbability().getProbability() * 100.0));
        data.append(" chance to successfully \ncatch a fish in every try.\n");
        data.append("It will cost ");
        data.append(fishingRod.getFeatureChangeRate().get("Energy"));
        data.append(" Energy Points \nafter every use.\n");
        data.append(str);
        data.append("\n");
        return data.toString();
    }

    public String machine(Machine machine) {
        data = new StringBuilder("");
        String name = machine.getName();
        switch (name) {
            case "Juicer":
                data.append("A machine to extract juice from fruits and vegetables.\n");
                break;
            case "Spinning Wheel":
                data.append("A machine to make threads out of wool.\n");
                break;
            case "The Ultimate Cheese Maker":
                data.append("A machine to create cheese from milk.\n");
                break;
            case "Milker":
                data.append("A machine to get milk from cow.\n");
                break;
            case "Tomato Paste":
                data.append("A machine to make tomato paste out of tomato.\n");
                break;
            case "Scissors":
                data.append("A tool to get wool from sheep.\n");
                break;
            default:
                data.append("A machine to ");
                // FIXME: 5/16/2017 complete this
                break;
        }
        return data.toString();
    }

    public String crop(Crop crop) {
        data = new StringBuilder("");
        if (crop.getAge() == 0) {
            data.append("A ");
            data.append(crop.getType());
            data.append(" seed. When it’s fully raised, you can harvest it ");
            data.append(crop.getHarvestLeft());
            data.append(" times.\nCan only be raised ");
            if (crop.getWeather().getType().equals("Tropical")) {
                data.append("at the greenhouse.\n");
            } else {
                data.append("in ");
                data.append(crop.getWeather().getType());
                data.append("\n");
            }
        }
        return data.toString();
    }

    public String product(Product product) throws IllegalAccessException, InstantiationException {
        data = new StringBuilder("");
        switch (product.getType()) {
            case "Processed Product":
                data.append("Can be used while cooking certain foods.\n");
                data.append("Effects:\n");
                for (Map.Entry<String, Double> entry : product.getFeatureChangeRate().entrySet()) {
                    data.append(entry.getKey());
                    data.append(": ");
                    data.append(entry.getValue());
                    data.append("\n");
                }
                break;
            case "Animal Food":
                data.append("Food for ");
                Class animalClass = AllOfAnimals.class;
                Object obj = animalClass.newInstance();
                Method[] animals = animalClass.getDeclaredMethods();
                for (Method animal : animals) {
                    try {
                        Animal animal1 = (Animal) animal.invoke(obj);
                        for (Item food : animal1.getFoods()) {
                            if (food.getName().equals(product.getName())) {
                                data.append(animal1.getType());
                                data.append(" ");
                                break;
                            }

                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
                break;
            case "Animal Product":
                switch (product.getName()) {
                    case "Wool":
                        data.append("Sheep wool. By using a spinning wheel you can turn it into a thread.\n");
                        break;
                    case "Milk":
                        data.append("Can be used while cooking certain foods.\n");
                        data.append("Effects:\n");
                        for (Map.Entry<String, Double> entry : product.getFeatureChangeRate().entrySet()) {
                            data.append(entry.getKey());
                            data.append(": ");
                            data.append(entry.getValue());
                            data.append("\n");
                        }
                        break;
                    default:
                        data.append("Can be used while cooking certain foods.\n");
                        break;
                }
                break;
            default:
                if (product.getName().equals("Thread"))
                    data.append("Can be used to create specific items at the workshop.\n");
                else
                    data.append("Can be used while cooking certain foods.\n");
                break;
        }
        return data.toString();
    }

    public String fruit(Fruit fruit) {
        data = new StringBuilder("");
        data.append("A ");
        data.append(fruit.getName() + "!\n");
        data.append("It can be used while cooking.\n");
        data.append("Effects:\n");
        for (Map.Entry<String, Double> entry : fruit.getFeatureChangeRate().entrySet()) {
            data.append(entry.getKey());
            data.append(": ");
            data.append(entry.getValue());
            data.append("\n");
        }
        return data.toString();
    }

    public String drink(Drink drink) {
        data = new StringBuilder("");
        data.append("Can be used while cooking certain foods.\n");
        data.append("Effects:\n");
        for (Map.Entry<String, Double> entry : drink.getFeatureChangeRate().entrySet()) {
            data.append(entry.getKey());
            data.append(": ");
            data.append(entry.getValue());
            data.append("\n");
        }
        return data.toString();
    }

    public String food(Food food) {
        data = new StringBuilder("");
        data.append("Effects:\n");
        for (Map.Entry<String, Double> entry : food.getFeatureChangeRate().entrySet()) {
            data.append(entry.getKey());
            data.append(": ");
            data.append(entry.getValue());
            data.append("\n");
        }
        return data.toString();
    }

    public String potion(Potion potion) {
        data = new StringBuilder("");
        data.append("It heals ");
        data.append((Double) potion.getFeatureChangeRate().get("Health"));
        data.append(" health points.\n");
        return data.toString();
    }

    public ArrayList<String> recipe(Recipe recipe) {
        ArrayList<String> data = new ArrayList<>();
        data.add("** Tools needed:\n");
        for (Tool tool : recipe.getFood().getCookingTools()) {
            data.add(tool.getName() + "       ");
        }
        data.add("** Ingredients:\n");
        for (Item item : recipe.getFood().getDissassemblity().getInitialIngredients().elementSet()) {
            data.add(item.getName() + " x" + recipe.getFood().getDissassemblity().getInitialIngredients().count(item) + "       ");
        }
        data.add("** Stats:\n");
        for (Map.Entry<String, Double> entry : recipe.getFood().getFeatureChangeRate().entrySet()) {
            data.add(entry.getKey() + " : " + entry.getValue().toString() + "       ");
        }
        return data;
    }

    public String gardenTree(GardenTree gardenTree) {
        data = new StringBuilder("Status:\nSeason: ");
        data.append(gardenTree.getWeather().getType());
        data.append("\nWatered: ");
        data.append(gardenTree.isWatered());
        data.append("\nNumber of fruits: ");
        int cnt = 0;
        for (int i = 0; i < gardenTree.getFruitProduction().size(); i++)
            cnt++;
        data.append(cnt);
        data.append(" Fruits");
        return data.toString();
    }

    public String shopAnimal(Animal animal) {
        data = new StringBuilder("Status: \n");
        for (Feature feature : animal.getFeature()) {
            data.append(feature.getName());
            data.append(" : ");
            data.append(feature.getCurrent());
            data.append("\n");
        }
        data.append("Price: ");
        data.append(animal.getPrice());
        return data.toString();
    }

    public String BarnAnimal(Animal animal) {
        data = new StringBuilder("Status: \n");
        for (Feature feature : animal.getFeature()) {
            data.append(feature.getName());
            data.append(" : ");
            data.append(feature.getCurrent());
            data.append("\n");
        }
        data.append("is feed: " + animal.isFeed() + "\n");
        data.append("Days until production: ");
        data.append((animal.getProduceCycle() - animal.getProduceProcess()));
        return data.toString();
    }

    public String train(Train train, String property) {
        data = new StringBuilder("Status: \n");
        data.append(property + "\n");
        data.append("Amount added to this feature property by training: ");
        //data.append((int) train.getFeatureAddition() + "\n");
        data.append("Energy used: ");
        data.append((int) train.getNeededEnergy() + "\n");
        data.append("Train price: ");
        data.append((int) train.getPrice() + "\n");
        return data.toString();
    }

    public String stats(Player player) {
        data = new StringBuilder("Stats: \n");
        for (Feature feature : player.getFeature()) {
            if (feature.getName().equals("Energy")) {
                data.append("Energy: ");
                data.append((int) feature.getCurrent() + "\n");
            } else if (feature.getName().equals("Health")) {
                data.append("Health: ");
                data.append((int) feature.getCurrent() + "\n");
            }
        }
        data.append("Money: " + player.getMoney());
        return data.toString();
    }


    public String tool(Tool tool) {
        if (tool instanceof CookingUtensil)
            return cookingUtensil((CookingUtensil) tool);
        else if (tool instanceof Axe)
            return axe((Axe) tool);
        else if (tool instanceof Pickaxe)
            return pickaxe((Pickaxe) tool);
        else if (tool instanceof Shovel)
            return shovel((Shovel) tool);
        else if (tool instanceof FishingRod)
            return fishingRod((FishingRod) tool);
        else if (tool instanceof WateringCan)
            return wateringCan((WateringCan) tool);
        else if (tool instanceof Machine)
            return machine((Machine) tool);
        return null;
    }

    public String item(Item item) throws InstantiationException, IllegalAccessException {
        if (item instanceof Tool)
            return tool((Tool) item);
        else if (item instanceof Drink)
            return drink((Drink) item);
        else if (item instanceof Food)
            return food((Food) item);
        else if (item instanceof Fruit)
            return fruit((Fruit) item);
        else if (item instanceof Potion)
            return potion((Potion) item);
        else if (item instanceof Product)
            return product((Product) item);
        else if(item instanceof Seed)
            return crop(((Seed) item).getSource());
        return null;
    }

    public String object(Object object) throws InstantiationException, IllegalAccessException {
        if (object instanceof Item)
            return item((Item) object);
        else if (object instanceof GardenTree)
            return gardenTree((GardenTree) object);
        else if (object instanceof Crop)
            return crop((Crop) object);
        return null;
    }

}
