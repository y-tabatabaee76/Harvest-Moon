import com.google.common.collect.HashMultiset;

import java.util.ArrayList;

public class StorageBox {

    private HashMultiset<Item> item;
    private ArrayList<Task> task;

    public StorageBox(ArrayList<Task> task) {
        item = HashMultiset.create();
        this.task = task;
    }

    public StorageBox() {
        item = HashMultiset.create();
        AllOfTools allOfTools = new AllOfTools();
        item.add(allOfTools.pineFishingRod(), 3);
        task = new ArrayList<>();
    }

    public HashMultiset<Item> getItem() {
        return item;
    }

    public void setItem(HashMultiset<Item> item) {
        this.item = item;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }
}