import com.google.common.collect.HashMultiset;

import java.util.HashMap;

public class Take extends Task {

    public Take() {
        super.name = "Take";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String  run(StorageBox storageBox, Item item, Player player) {
        if (player.getBackpack().getCapacity() > player.getBackpack().getCurrentFullness() + item.getCapacity()) {
            player.getBackpack().getItem().add(item);
            storageBox.getItem().remove(item);
            UI.missionHandler("Put", "Backpack");
            UI.missionHandler(name, "Storage Box");
            String[] string = {" was taken from storageBox!", "A été retiré de storageBox!", "Fue tomada de storageBox!"};
            return item.getName() + string[UI.language];
        } else {
            String[] string = {"Backpack is full", "Le sac à dos est plein", "La mochila está llena"};
            return string[UI.language];
        }
    }

    public boolean run(ToolShelf toolShelf, Item item, Player player) {
        if (player.getBackpack().getCapacity() > player.getBackpack().getCurrentFullness() + item.getCapacity()) {
            player.getBackpack().getItem().add(item);
            player.getBackpack().setCapacity(player.getBackpack().getCapacity() + item.getCapacity());
            toolShelf.getExistingTools().remove(item);
            return true;
        } else {
            System.out.println("Backpack is full\n");
            return false;
        }
    }

    public boolean run(Item item, Player player) {
        if (player.getBackpack().getCapacity() > player.getBackpack().getCurrentFullness() + item.getCapacity()) {
            player.getBackpack().getItem().add(item);
            return true;
        } else {
            System.out.println("Backpack is full\n");
            return false;
        }
    }
}
