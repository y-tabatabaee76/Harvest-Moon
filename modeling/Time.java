import java.util.ArrayList;

public class Time {

    private int hour;
    private int minute;
    private int second;
    private ArrayList<Task> task;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public Time(int hour, int minute, int second, ArrayList<Task> task) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.task = task;
    }

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public Time() {

    }
}