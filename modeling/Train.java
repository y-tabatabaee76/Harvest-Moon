import java.util.ArrayList;
import java.util.HashMap;

public class Train extends Task{

    private double price;
    private double neededEnergy;
    private ArrayList<Feature> featureAddition;

    public Train() {
        super.name = "Train";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
        price = Math.random() * 500;
        neededEnergy = Math.random() * 100;
    }

    public Train(String name, ArrayList<Feature> featureAddition, double price, double neededEnergy) {
        super.name = name;
        this.price = price;
        this.neededEnergy = neededEnergy;
        this.featureAddition = featureAddition;
    }

    public String run(Player player) {
        for(Feature feature: player.getFeature()){
            if(feature.getName().toLowerCase().equals("energy")){
                if(feature.getCurrent() < neededEnergy)
                    return "You don't have enough energy to train!";
            }
        }
        if(player.getMoney() >=  price){
            for(Feature feature: featureAddition){
                for(Feature playerFeature: player.getFeature()){
                    if(playerFeature.getName().equalsIgnoreCase(feature.getName())){
                        playerFeature.setCurrent(playerFeature.getCurrent() + feature.getCurrent());
                        playerFeature.setMaxCurrent(playerFeature.getMaxCurrent() + feature.getMaxCurrent());
                        playerFeature.setConsumptionRate(playerFeature.getConsumptionRate() + feature.getConsumptionRate());
                        playerFeature.setRefillRate(playerFeature.getRefillRate() + feature.getRefillRate());
                        break;
                    }
                }
            }
            player.setMoney(player.getMoney() - price);
            return name + " was done successfully!";
        }
        else{
            return ("you don't have enough money for training!\n");
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getNeededEnergy() {
        return neededEnergy;
    }

    public void setNeededEnergy(double neededEnergy) {
        this.neededEnergy = neededEnergy;
    }

    public ArrayList<Feature> getFeatureAddition() {
        return featureAddition;
    }

    public void setFeatureAddition(ArrayList<Feature> featureAddition) {
        this.featureAddition = featureAddition;
    }

}