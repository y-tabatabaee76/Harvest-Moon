import java.util.HashMap;

public class Update extends Task{

    public Update() {
        super.name = "Update";
        super.featureChangeRate = new HashMap<String, Double>();
        featureChangeRate.put("Energy", -7.0);
        super.executionTime = new Date();
    }

    public String run(Greenhouse greenhouse, Player player) {
        player.setMoney(player.getMoney() - greenhouse.getRepairPrice());
        for (Object object: greenhouse.getRepairObjects().elementSet()) {
            player.getBackpack().getItem().remove(object, greenhouse.getRepairObjects().count(object));
        }
        greenhouse.setIsRuin(false);
        String[] string = {"Greenhouse was repaired successfully!", "La serre a été réparée avec succès!", "گلخانه با موفقیت تعمیر شد!",
                "¡El invernadero fue reparado con éxito!"};
        return string[UI.language];
    }



    public boolean run(BarnPart barnPart, int price, Player player ,int capacity) {
        if(player.getMoney() <= price) {
            System.out.println("You don't have enough money!");
            return false;
        }
        System.out.println(barnPart.getAnimalType() + " barnpart capacity was:" + barnPart.getMaxCapacity());
        barnPart.setMaxCapacity(barnPart.getMaxCapacity() + capacity);
        System.out.println(barnPart.getAnimalType() + "'s barnpart capacity was extended to " + barnPart.getMaxCapacity());
        return true;
    }
}
