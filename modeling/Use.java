import java.util.HashMap;
import java.util.Map;

public class Use extends Task {
    public Use() {
        super.name = "Use";
        super.featureChangeRate = new HashMap<String, Double>();
        super.executionTime = new Date();
    }

    public String  run(Player player, Item item, Backpack backpack) {
        for (Map.Entry<String, Double> entry : item.getFeatureChangeRate().entrySet()) {
            for (int i = 0; i < player.getFeature().size(); i++) {
                if (player.getFeature().get(i).getName().equalsIgnoreCase(entry.getKey())) {
                    if (player.getFeature().get(i).getCurrent() + entry.getValue() <
                            player.getFeature().get(i).getMaxCurrent()) {
                        player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent() + entry.getValue());
                    } else {
                        player.getFeature().get(i).setCurrent(player.getFeature().get(i).getMaxCurrent());
                    }
                    break;
                }
            }
        }
        backpack.getItem().remove(item);
        backpack.setCurrentFullness(backpack.getCurrentFullness() - item.getCapacity());
        UI.missionHandler(name, item.getName());
        String[] string = {" used!", " utilisé!", " ¡usado!"};
        return (item.getName() +  string[UI.language]);
    }
}
