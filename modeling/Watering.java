import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.Map;

public class Watering extends Task {

    public Watering(WateringCan wateringCan) {
        super.name = "Watering";
        super.featureChangeRate = wateringCan.getFeatureChangeRate();
        super.executionTime = new Date();
    }

    public String run(WateringCan wateringCan, Cell[][] cell, Player player, int x, int y, Rectangle[][] cellG) {
        Image image = new Image("pics/wateredEarth.png");
        Image image1 = new Image("pics/farmingField.png");
        Paint[][] imagePatterns = new ImagePattern[cell.length][cell[0].length];
        if (wateringCan.isValid()) {
            int count = 0;
            for (int i = 0; i < cell.length; i++)
                for (int j = 0; j < cell[i].length; j++) {
                    if (cell[i][j] != null) {
                        count++;
                        imagePatterns[i][j] = cellG[j + x][i + y].getFill();
                    }
                }
            if (wateringCan.getWaterLevel() >= count) {
                for (int i = 0; i < cell.length; i++)
                    for (int j = 0; j < cell[i].length; j++) {
                        if(cell[i][j] != null){
                            cell[i][j].setWatered(true);
                            cellG[j + x][i + y].setFill(new ImagePattern(image));
                        }
                    }
                Timeline timeline = new Timeline(new KeyFrame(Duration.millis(1000), event1 -> {
                    for (int i = 0; i < cell.length; i++)
                        for (int j = 0; j < cell[i].length; j++) {
                            if(cell[i][j] != null){
                                cell[i][j].setWatered(true);
                                cellG[j + x][i + y].setFill(imagePatterns[i][j]);
                            }
                        }
                }));
                timeline.play();
                for (Map.Entry<String, Double> entry : wateringCan.getFeatureChangeRate().entrySet())
                    for (int i = 0; i < player.getFeature().size(); i++)
                        if (entry.getKey().equals(player.getFeature().get(i).getName()))
                            player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent() + entry.getValue());
                wateringCan.setWaterLevel(wateringCan.getWaterLevel() - count);
                Double random = Math.random();
                if (wateringCan.getInvalid().getProbability() > random) {
                    wateringCan.setValid(false);
                    String[] string = {" broke!", " cassé!", "rompió"};
                    return wateringCan.getName() + string[UI.language];
                } else {
                    String[] string = {"Chosen cells watered", "Cellules choisies arrosées",  "Las células elegidas se regaron"};
                    return string[UI.language];
                }

            } else {
                String[] string = {" doesn't have enough water!", " N'a pas assez d'eau!", "No tiene suficiente agua"};
                return wateringCan.getName() +  string[UI.language];
            }

        } else {
            String[] string = {" \" is broken\"!", " est cassé!", "está roto"};
            return wateringCan.getName() + string[UI.language];
        }
    }

    public String run(WateringCan wateringCan, Rectangle cell, Player player, GardenTree gardenTree) {
        if (wateringCan.isValid()) {
            int count = 0;
            Image treePlacePic = new Image("pics/treePlace.png");
            Image watered = new Image("pics/wateredTreePlace.png");
            if (wateringCan.getWaterLevel() >= 1) {
                cell.setFill(new ImagePattern(watered));
                Timeline timeline = new Timeline(new KeyFrame(Duration.millis(1000), event1 -> {
                    cell.setFill(new ImagePattern(treePlacePic));
                }));
                timeline.play();
                for (Map.Entry<String, Double> entry : wateringCan.getFeatureChangeRate().entrySet())
                    for (int i = 0; i < player.getFeature().size(); i++)
                        if (entry.getKey().equals(player.getFeature().get(i).getName()))
                            player.getFeature().get(i).setCurrent(player.getFeature().get(i).getCurrent() + entry.getValue());
                wateringCan.setWaterLevel(wateringCan.getWaterLevel() - count);
                Double random = Math.random();
                try {
                    gardenTree.setWatered(true);
                }
                catch (Exception e){
                }
                if (wateringCan.getInvalid().getProbability() > random) {
                    wateringCan.setValid(false);
                    String[] string = {" broke!", " cassé!", "rompió"};
                    return wateringCan.getName() + string[UI.language];
                } else{
                    String[] string = {"Chosen cells watered", "Cellules choisies arrosées", "Las células elegidas se regaron"};
                    return string[UI.language];
                }
            } else {
                String[] string = {" doesn't have enough water!", " N'a pas assez d'eau!", "No tiene suficiente agua"};
                return wateringCan.getName() +  string[UI.language];
            }

        } else {
            String[] string = {" \" is broken\"!", " est cassé!", "está roto"};
            return wateringCan.getName() + string[UI.language];
        }
    }
}
