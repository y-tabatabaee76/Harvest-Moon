import com.google.common.collect.HashMultiset;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class WateringCan extends Tool{

    private Range range;
    private int waterLevel;
    private int maxWaterLevel;
    private int fieldNum;

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public int getMaxWaterLevel() {
        return maxWaterLevel;
    }

    public void setMaxWaterLevel(int maxWaterLevel) {
        this.maxWaterLevel = maxWaterLevel;
    }

    public int getFieldNum() {
        return fieldNum;
    }

    public void setFieldNum(int fieldNum) {
        this.fieldNum = fieldNum;
    }

    public int getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(int waterLevel) {
        this.waterLevel = waterLevel;
    }

    public WateringCan(String type, HashMap<String, Double> featureChangeRate, double capacity, String name,
                       double price, ArrayList<Task> task, int level, Dissassemblity dissassemblity, double buildingPrice,
                       double repairPrice, HashMultiset<Item> repairItems, Range range, int maxWaterLevel, int fieldNum, Image picture) {
        super(type, featureChangeRate, capacity, name, new Probability(){{setProbability((double)(5 - level) / 20);}}, price, task, 4, level,
                dissassemblity, buildingPrice, repairPrice, repairItems, picture);
        this.range = range;
        this.maxWaterLevel = maxWaterLevel;
        this.fieldNum = fieldNum;
    }

    public WateringCan() {
    }
}
