import java.util.ArrayList;

public class Weather {

    private double humidity;
    private double temperature;
    private String type;
    private ArrayList<Task> task;

    public Weather() {

    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Task> getTask() {
        return task;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = task;
    }

    public Weather(double humidity, double temperature, String type, ArrayList<Task> task) {
        this.humidity = humidity;
        this.temperature = temperature;
        this.type = type;
        this.task = task;
    }
}