import com.google.common.collect.HashMultiset;

public class Workshop extends Building {


    private HashMultiset<Item> tool;

    public HashMultiset<Item> getTool() {
        return tool;
    }

    public void setTool(HashMultiset<Item> tool) {
        this.tool = tool;
    }

    public Workshop(boolean isRuin, Probability ruin, HashMultiset repairObjects, double repairPrice, HashMultiset<Item> tool) {
        super(isRuin, ruin, repairObjects, repairPrice);
        this.tool = tool;
    }

    public Workshop(HashMultiset<Item> tool){
        super();
        this.tool = tool;
    }
}