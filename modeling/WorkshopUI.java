import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class WorkshopUI implements Mover {
    Group root = new Group();
    private static Workshop workshop;
    static double tileSize = 50;
    private static Rectangle tooleMaker;
    private static Rectangle repairTable;
    private static Rectangle repairBoard;
    private static Rectangle bookcase;
    private static Rectangle dissassembler;
    private static Rectangle checkTable;
    private static Rectangle makeBoard;
    private static Rectangle checkBoard;
    private static Rectangle dissassmebleBoard;

    private Rectangle rectangleBuilder(double width, double height, Paint fill, double setX, double setY) {
        Rectangle rectangle = new Rectangle(width, height, fill);
        rectangle.setX(setX);
        rectangle.setY(setY);
        return rectangle;
    }

    public static Text buttonDesigner(String name, double x, double y, double fontSize, Color color) {
        Text text = new Text(x, y, name);
        text.setFont(Font.font("Comic Sans MS", fontSize));
        text.setFill(Color.BLACK);
        text.setStroke(color);
        text.setOnMouseEntered(event -> text.setFill(Color.DARKGREY));
        text.setOnMouseExited(event -> text.setFill(Color.BLACK));
        return text;
    }

    public WorkshopUI(Group root, Workshop workshop) {
        this.root = root;
        this.workshop = workshop;
    }

    public Scene sceneBuilder(Stage primaryStage) {
        Scene workshopScene = new Scene(root, 1000, 700, Color.BLACK);
        Image workshopPic = new Image("pics/maps/workshopMap.png");
        Image doorPic = new Image("pics/doors/workshopDoor.png");
        Image bookcasePic = new Image("pics/bookcase.png");
        Image toolMakerPic = new Image("pics/toolmaker.png");
        Image dissassemblerPic = new Image("pics/dissassembler.png");
        Image checkTablePic = new Image("pics/checkWorkshop.png");
        Image repairTablePic = new Image("pics/repairTable.png");
        Image boardPic = new Image("pics/workshopBoard.png");

        Rectangle rectangle = new Rectangle(1000, 700, new ImagePattern(workshopPic));
        root.getChildren().add(rectangle);

        Rectangle door = rectangleBuilder(tileSize, tileSize * 3, new ImagePattern(doorPic), 1000 - tileSize * 3, 0);
        bookcase = rectangleBuilder(tileSize * 5, tileSize * 2, new ImagePattern(bookcasePic), tileSize * 2, tileSize * 2);
        tooleMaker = rectangleBuilder(tileSize * 2, tileSize * 4, new ImagePattern(toolMakerPic), tileSize * 2, tileSize * 5);
        dissassembler = rectangleBuilder(tileSize * 2, tileSize * 3, new ImagePattern(dissassemblerPic), tileSize * 12, 700 - tileSize * 5);
        checkTable = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(checkTablePic), tileSize * 11, tileSize * 6);
        repairTable = rectangleBuilder(tileSize * 3, tileSize * 2, new ImagePattern(repairTablePic), tileSize * 2, tileSize * 10);
        root.getChildren().addAll(door, bookcase, tooleMaker, dissassembler, checkTable, repairTable);

        makeBoard = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(boardPic), tileSize * 5, tileSize * 5);
        repairBoard = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(boardPic), tileSize * 5, tileSize * 9);
        checkBoard = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(boardPic), tileSize * 15, tileSize * 5);
        dissassmebleBoard = rectangleBuilder(tileSize * 2, tileSize * 2, new ImagePattern(boardPic), tileSize * 15, tileSize * 9);
        root.getChildren().addAll(makeBoard, repairBoard, checkBoard, dissassmebleBoard);

        return workshopScene;
    }

    public boolean moveUp(Rectangle player) {
        if (player.getY() + player.getHeight() >= tileSize * 3.4 &&
                !(player.getX() + player.getWidth() >= bookcase.getX() + tileSize * 0.4 &&
                        player.getX() <= bookcase.getX() + bookcase.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= bookcase.getY() + bookcase.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= bookcase.getY() + bookcase.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= checkBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= checkBoard.getX() + checkBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= checkBoard.getY() + checkBoard.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= checkBoard.getY() + checkBoard.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= dissassmebleBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= dissassmebleBoard.getX() + dissassmebleBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dissassmebleBoard.getY() + dissassmebleBoard.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= dissassmebleBoard.getY() + dissassmebleBoard.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= makeBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= makeBoard.getX() + makeBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= makeBoard.getY() + makeBoard.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= makeBoard.getY() + makeBoard.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= repairBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= repairBoard.getX() + repairBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= repairBoard.getY() + repairBoard.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= repairBoard.getY() + repairBoard.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= checkTable.getX() + tileSize * 0.4 &&
                        player.getX() <= checkTable.getX() + checkTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= checkTable.getY() + checkTable.getHeight() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= checkTable.getY() + checkTable.getHeight()) &&
                !(player.getX() + player.getWidth() >= dissassembler.getX() + tileSize * 0.4 &&
                        player.getX() <= dissassembler.getX() + dissassembler.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= dissassembler.getY() + dissassembler.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= dissassembler.getY() + dissassembler.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tooleMaker.getX() + tileSize * 0.4 &&
                        player.getX() <= tooleMaker.getX() + tooleMaker.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= tooleMaker.getY() + tooleMaker.getHeight() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= tooleMaker.getY() + tooleMaker.getHeight() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= repairTable.getX() + tileSize * 0.4 &&
                        player.getX() <= repairTable.getX() + repairTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= repairTable.getY() + repairTable.getHeight() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= repairTable.getY() + repairTable.getHeight())) {
            return true;
        }
        return false;
    }

    public boolean moveDown(Rectangle player) {
        if (player.getY() + player.getHeight() <= tileSize * 14 &&
                !(player.getX() + player.getWidth() >= bookcase.getX() + tileSize * 0.4 &&
                        player.getX() <= bookcase.getX() + bookcase.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= bookcase.getY() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= bookcase.getY() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= checkBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= checkBoard.getX() + checkBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= checkBoard.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= checkBoard.getY()) &&
                !(player.getX() + player.getWidth() >= dissassmebleBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= dissassmebleBoard.getX() + dissassmebleBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dissassmebleBoard.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= dissassmebleBoard.getY()) &&
                !(player.getX() + player.getWidth() >= makeBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= makeBoard.getX() + makeBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= makeBoard.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= makeBoard.getY()) &&
                !(player.getX() + player.getWidth() >= repairBoard.getX() + tileSize * 0.6 &&
                        player.getX() <= repairBoard.getX() + repairBoard.getWidth() - tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= repairBoard.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= repairBoard.getY()) &&
                !(player.getX() + player.getWidth() >= checkTable.getX() + tileSize * 0.4 &&
                        player.getX() <= checkTable.getX() + checkTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= checkTable.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= checkTable.getY()) &&
                !(player.getX() + player.getWidth() >= dissassembler.getX() + tileSize * 0.4 &&
                        player.getX() <= dissassembler.getX() + dissassembler.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= dissassembler.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= dissassembler.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= tooleMaker.getX() + tileSize * 0.4 &&
                        player.getX() <= tooleMaker.getX() + tooleMaker.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= tooleMaker.getY() + tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= tooleMaker.getY() + tileSize * 0.4) &&
                !(player.getX() + player.getWidth() >= repairTable.getX() + tileSize * 0.4 &&
                        player.getX() <= repairTable.getX() + repairTable.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= repairTable.getY() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() <= repairTable.getY())) {
            return true;
        }
        return false;
    }

    public boolean moveRight(Rectangle player) {
        if (player.getX() + player.getWidth() <= tileSize * 20.2 &&
                !(player.getX() + player.getWidth() >= bookcase.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= bookcase.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= bookcase.getY() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= bookcase.getY() + bookcase.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= checkBoard.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= checkBoard.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= checkBoard.getY() &&
                        player.getY() + player.getHeight() <= checkBoard.getY() + checkBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= dissassmebleBoard.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= dissassmebleBoard.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= dissassmebleBoard.getY() &&
                        player.getY() + player.getHeight() <= dissassmebleBoard.getY() + dissassmebleBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= makeBoard.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= makeBoard.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= makeBoard.getY() &&
                        player.getY() + player.getHeight() <= makeBoard.getY() + makeBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= repairBoard.getX() + tileSize * 0.4 &&
                        player.getX() + player.getWidth() <= repairBoard.getX() + tileSize * 0.6 &&
                        player.getY() + player.getHeight() >= repairBoard.getY() &&
                        player.getY() + player.getHeight() <= repairBoard.getY() + repairBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= checkTable.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= checkTable.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= checkTable.getY() &&
                        player.getY() + player.getHeight() <= checkTable.getY() + checkTable.getHeight() - tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= dissassembler.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= dissassembler.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= dissassembler.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= dissassembler.getY() + dissassembler.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= tooleMaker.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= tooleMaker.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= tooleMaker.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= tooleMaker.getY() + tooleMaker.getHeight() + tileSize * 0.2) &&
                !(player.getX() + player.getWidth() >= repairTable.getX() + tileSize * 0.2 &&
                        player.getX() + player.getWidth() <= repairTable.getX() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= repairTable.getY() &&
                        player.getY() + player.getHeight() <= repairTable.getY() + repairTable.getHeight() - tileSize * 0.2)) {
            return true;
        }
        return false;
    }

    public boolean moveLeft(Rectangle player) {
        if (player.getX() >= -tileSize * 0.2 &&
                !(player.getX() >= bookcase.getX() + bookcase.getWidth() - tileSize * 0.4 &&
                        player.getX() <= bookcase.getX() + bookcase.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= bookcase.getY() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= bookcase.getY() + bookcase.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= checkBoard.getX() + checkBoard.getWidth() - tileSize * 0.6 &&
                        player.getX() <= checkBoard.getX() + checkBoard.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= checkBoard.getY() &&
                        player.getY() + player.getHeight() <= checkBoard.getY() + checkBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= dissassmebleBoard.getX() + dissassmebleBoard.getWidth() - tileSize * 0.6 &&
                        player.getX() <= dissassmebleBoard.getX() + dissassmebleBoard.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= dissassmebleBoard.getY() &&
                        player.getY() + player.getHeight() <= dissassmebleBoard.getY() + dissassmebleBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= makeBoard.getX() + makeBoard.getWidth() - tileSize * 0.6 &&
                        player.getX() <= makeBoard.getX() + makeBoard.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= makeBoard.getY() &&
                        player.getY() + player.getHeight() <= makeBoard.getY() + makeBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= repairBoard.getX() + repairBoard.getWidth() - tileSize * 0.6 &&
                        player.getX() <= repairBoard.getX() + repairBoard.getWidth() - tileSize * 0.4 &&
                        player.getY() + player.getHeight() >= repairBoard.getY() &&
                        player.getY() + player.getHeight() <= repairBoard.getY() + repairBoard.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= checkTable.getX() + checkTable.getWidth() - tileSize * 0.4 &&
                        player.getX() <= checkTable.getX() + checkTable.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= checkTable.getY() &&
                        player.getY() + player.getHeight() <= checkTable.getY() + checkTable.getHeight() - tileSize * 0.2) &&
                !(player.getX() >= dissassembler.getX() + dissassembler.getWidth() - tileSize * 0.4 &&
                        player.getX() <= dissassembler.getX() + dissassembler.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= dissassembler.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= dissassembler.getY() + dissassembler.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= tooleMaker.getX() + tooleMaker.getWidth() - tileSize * 0.4 &&
                        player.getX() <= tooleMaker.getX() + tooleMaker.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= tooleMaker.getY() + tileSize * 0.4 &&
                        player.getY() + player.getHeight() <= tooleMaker.getY() + tooleMaker.getHeight() + tileSize * 0.2) &&
                !(player.getX() >= repairTable.getX() + repairTable.getWidth() - tileSize * 0.4 &&
                        player.getX() <= repairTable.getX() + repairTable.getWidth() - tileSize * 0.2 &&
                        player.getY() + player.getHeight() >= repairTable.getY() &&
                        player.getY() + player.getHeight() <= repairTable.getY() + repairTable.getHeight() - tileSize * 0.2)) {
            return true;
        }
        return false;
    }

    public int rightPlace(Rectangle player) {
        if (player.getX() + player.getWidth() / 2 <= checkTable.getX() + checkTable.getWidth() + 30 &&
                player.getX() + player.getWidth() / 2 >= checkTable.getX() - 30 &&
                player.getY() + player.getHeight() <= checkTable.getY() + checkTable.getHeight() + 30 &&
                player.getY() + player.getHeight() >= checkTable.getY() - 30)
            return 1;
        else if (player.getX() + player.getWidth() / 2 <= tooleMaker.getX() + tooleMaker.getWidth() + 30 &&
                player.getX() + player.getWidth() / 2 >= tooleMaker.getX() - 30 &&
                player.getY() + player.getHeight() <= tooleMaker.getY() + tooleMaker.getHeight() + 30 &&
                player.getY() + player.getHeight() >= tooleMaker.getY() - 30)
            return 2;
        else if (player.getX() + player.getWidth() / 2 <= dissassembler.getX() + dissassembler.getWidth() + 30 &&
                player.getX() + player.getWidth() / 2 >= dissassembler.getX() - 30 &&
                player.getY() + player.getHeight() <= dissassembler.getY() + dissassembler.getHeight() + 30 &&
                player.getY() + player.getHeight() >= dissassembler.getY() - 30)
            return 3;
        else if (player.getX() + player.getWidth() / 2 <= repairTable.getX() + repairTable.getWidth() + 30 &&
                player.getX() + player.getWidth() / 2 >= repairTable.getX() - 30 &&
                player.getY() + player.getHeight() <= repairTable.getY() + repairTable.getHeight() + 30 &&
                player.getY() + player.getHeight() >= repairTable.getY() - 30)
            return 4;
        return -1;
    }

    public void toolMaker() {

    }

    public void make(Item choosedItem, Player player, Barn barn, boolean[] flags, BarnUI barnUI) {
        Make make = new Make();
        UI.showPopup(make.run((Tool)choosedItem, player, workshop, barn, barnUI), root);
        flags[3] = true;
    }

    public void repair(Item choosedItem, Player player, boolean[] flags) {
        Repair repair = new Repair();
        UI.showPopup(repair.run((Tool)choosedItem, player), root);
        flags[3] = true;
    }

    public void dissassemble(Item choosedItem, Player player, boolean[] flags) {
        Disassemble disassemble = new Disassemble();
        UI.showPopup(disassemble.run((Tool)choosedItem, player), root);
        flags[3] = true;
    }
}
